<?php
use think\Env;

$a=THINK_PATH;//just for source read debug

return [
   
    //应用设置 ↓
    'app_namespace'          => 'app',// 应用命名空间,惯例配置
    //'app_debug'              => Env::get('app.debug', false), // 应用调试模式,
    'app_debug'              => true, // 应用调试模式,
    //'app_trace'              => Env::get('app.trace', true),// 应用Trace（右下角的图标）
    'app_trace'              => true,// 应用Trace（右下角的图标）
    'app_status'             => '',// 应用模式状态 
    'app_multi_module'       => true, // 是否支持多模块
    'auto_bind_module'       => false, // 入口自动绑定模块
    'root_namespace'         => [],// 注册的根命名空间
    'extra_file_list'        => [THINK_PATH . 'helper' . EXT],// 扩展函数文件 (thinkphp助手文件)
//你可以随意修改助手函数的名称或者添加自己的助手函数
//    'extra_file_list' => [
//THINK_PATH . 'helper' . EXT,
//APP_PATH . 'helper' . EXT
//],
    'default_return_type'    => 'html',// 默认输出类型
    'default_ajax_return'    => 'json', // 默认AJAX 数据返回格式,可选json xml ...
    'default_jsonp_handler'  => 'jsonpReturn',// 默认JSONP格式返回的处理方法
    'var_jsonp_handler'      => 'callback',// 默认JSONP处理方法
    'default_timezone'       => 'Japan',  // 默认时区 &PRC' 参见http://php.net/manual/zh/timezones.others.php
    'lang_switch_on'         => true,// 是否开启多语言    
    //'default_filter'         => '',// 默认全局过滤方法 用逗号分隔多个
    'default_filter'         => 'htmlspecialchars',// 20190115 试一下这个
    
    'default_lang'           => 'ja-jp',//zh-cn,en-us // 默认语言
    //'default_lang'           => 'zh-cn',//ja-jp,en-us
    'class_suffix'           => false,// 应用类库后缀
    'controller_suffix'      => false, // 控制器类后缀
    //应用设置 ↑

   // 模块设置↓
    'default_module'         => 'index', // 默认模块名    
    'deny_module_list'       => ['common'],// 禁止访问模块
    'default_controller'     => 'Index', // 默认控制器名
    'default_action'         => 'index',// 默认操作名
    'default_validate'       => '', // 默认验证器
    'empty_controller'       => 'Error',// 默认的空控制器名
    'action_suffix'          => '', // 操作方法后缀
    'controller_auto_search' => true,// 自动搜索控制器　　true？？
    // 模块设置↑

   
 //URL设置↓
    'var_pathinfo'           => 's', // PATHINFO变量名 用于兼容模式
    'pathinfo_fetch'         => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],// 兼容PATH_INFO获取
    'pathinfo_depr'          => '/',// pathinfo分隔符
    'url_html_suffix'        => 'html',// URL伪静态后缀
    'url_common_param'       => false,// URL普通方式参数 用于自动生成
    'url_param_type'         => 0,// URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_route_on'           => true,// 是否开启路由
    'route_complete_match'   => false, // 路由使用完整匹配
    'route_config_file'      => ['route'],// 路由配置文件（支持配置多个）
    'url_route_must'         => false,// 是否强制使用路由
    'url_domain_deploy'      => false, // 域名部署
    'url_domain_root'        => '',// 域名根，如thinkphp.cn
    'url_convert'            => true, // 是否自动转换URL中的控制器和操作名
    'url_controller_layer'   => 'controller',// 默认的访问控制器层
    'var_method'             => '_method',// 表单请求类型伪装变量
    'var_ajax'               => '_ajax',// 表单ajax伪装变量
    'var_pjax'               => '_pjax',// 表单pjax伪装变量
    'request_cache'          => false,// 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache_expire'   => null, // 请求缓存有效期
    'request_cache_except'   => [],// 全局请求缓存排除规则
  //URL设置↑

 //模板设置　↓
    'template'               => [
        'type'         => 'Think',  // 模板引擎类型 支持 php think 支持扩展（think有点性能损失，但是可用模板标签）
        //'type'         => 'php',  // 模板引擎类型 支持 php think 支持扩展（php 原生不会在temp输出，但是没法用模板标签）
        'view_path'    => '',// 模板路径
        //'view_suffix'  => 'html',// 模板后缀,（不好用，写原生php不方便）
        'view_suffix'  => 'php',// 模板后缀
        'view_depr'    => DS, // 模板文件名分隔符
        'tpl_begin'    => '{',  // 模板引擎普通标签开始标记
        'tpl_end'      => '}', // 模板引擎普通标签结束标记
        'taglib_begin' => '{', // 标签库标签开始标记
        'taglib_end'   => '}', // 标签库标签结束标记
        'tpl_cache'    => true,
    ],
    // 视图输出字符串内容替换
    //全局配置view_replace_str 参数 P250 
    'view_replace_str'       => [
        '__PUBLIC__' => '',
        '__ROOT__'   => '',
        '__CDN__'    => '',
    ],
    'dispatch_success_tmpl'  => APP_PATH . 'common' . DS . 'view' . DS . 'tpl' . DS . 'dispatch_jump.tpl',// 默认跳转页面对应的模板文件
    'dispatch_error_tmpl'    => APP_PATH . 'common' . DS . 'view' . DS . 'tpl' . DS . 'dispatch_jump.tpl',// 默认跳转页面对应的模板文件（error）
//    'TMPL_ENGINE_TYPE'      =>  'php',     // Think模板引擎使用原生PHP
//    'TMPL_TEMPLATE_SUFFIX'  =>  '.php',     // 默认模板文件后缀
     //模板设置　↑

    // +----------------------------------------------------------------------
    // | 
    // +----------------------------------------------------------------------

    
    //异常 及错误设置　↓
    //'exception_tmpl'         => THINK_PATH . 'tpl' . DS . 'think_exception.tpl',// // 异常页面的模板文件　thinkphp 官方
    'exception_tmpl'         => APP_PATH . 'common' . DS . 'view' . DS . 'tpl' . DS . 'think_exception.tpl',// // 异常页面的模板文件　非调试模式有效
    'error_message'          => '入力されたURLのページは存在しません',// 错误显示信息,非调试模式有效
    'show_error_msg'         => false,// 显示错误信息
    'exception_handle'       => '', // 异常处理handle类 留空使用 \think\exception\Handle
    //异常 及错误设置　↑

   
//日志设置↓
    'log'                    => [
        'type'  => 'File',  // 日志记录方式，内置 file socket 支持扩展 
        'path'  => LOG_PATH,// 日志保存目录
        'level' => [],  // 日志记录级别
    ],
     //日志设置↑

   // Trace设置 开启 app_trace 后 有效　↓
    'trace'                  => [
        'type' => 'Html',// 内置Html Console 支持扩展
    ],
     // Trace设置 开启 app_trace 后 有效　↑


//缓存设置　↓
    'cache'                  => [
        'type'   => 'File', // 驱动方式
        'path'   => CACHE_PATH, // 缓存保存目录
        'prefix' => '',// 缓存前缀
        'expire' => 0, // 缓存有效期 0表示永久缓存
    ],
    //缓存设置　↓

//会话设置↓
    'session'                => [
        'id'             => '',
        'var_session_id' => '', // SESSION_ID的提交变量,解决flash上传跨域
        'prefix'         => 'think',  // SESSION 前缀
        'type'           => '',// 驱动方式 支持redis memcache memcached
        'auto_start'     => true,// 是否自动开启 SESSION
    ],
    //会话设置↑


    //Cookie设置↓
    'cookie'                 => [
        'prefix'    => '',// cookie 名称前缀
        'expire'    => 0,   // cookie 保存时间
        'path'      => '/', // cookie 保存路径
        'domain'    => '',  // cookie 有效域名
        'secure'    => false,  //  cookie 启用安全传输
        'httponly'  => '', // httponly设置
        'setcookie' => true,  // 是否使用 setcookie
    ],
    //Cookie设置↑

    //分页配置↓
    'paginate'               => [
        'type'      => 'bootstrap',
        'var_page'  => 'page',
        'list_rows' => 10,
    ],
    //分页配置↑
    
    //验证码配置　↓
//    'captcha'                => [
//        'codeSet'  => '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY', // 验证码字符集合
//        'fontSize' => 16, // 验证码字体大小(px)
//        'useCurve' => false,// 是否画混淆曲线
//        'useZh'    => false, //使用中文验证码
//        'imageH'   => 30,  // 验证码图片高度
//        'imageW'   => 100,// 验证码图片宽度
//        'length'   => 4,  // 验证码位数
//        'reset'    => true  // 验证成功后是否重置
//    ],
    //验证码配置　↑
    
//    //LiuAdmin配置 ↓
//    'liu_admin'              => [
//        'version' => '1.0.0.20171211_beta',
//        //'api_url' => 'http://api.liuadmin.com',
//    ],
//    //LiuAdmin配置 ↑
//    
//
//    
//         
 
    'db_config_user_mana' => [//// ユーザ管理DB
// 数据库类型
        'type' => 'mysql',
// 服务器地址
        'hostname' => '153.126.159.250',
// 数据库名
        'database' => 'liuadmin_user_mana',
// 数据库用户名
        'username' => 'root',
// 数据库密码
        'password' => 'Iiyama2016',
// 数据库连接端口
        'hostport' => '3306',
// 数据库连接参数
        'params' => [],
// 数据库编码默认采用utf8
        'charset' => 'utf8',
// 数据库表前缀
        'prefix' => '',
    ],
    
    
];
