<?php

namespace app\admin\validate;

use think\Validate;
use app\admin\model\SSystemconfig;
class HoteldateUpdateValidate extends Validate
{
    protected $rule = [
        //'hoteldate'  => 'dateFormat:Y/m/d|check_date:1',
       ['hoteldate' , 'dateFormat:Y/m/d','ホテル日フォーマット：YYYY/MM/DD'],
       ['new_password'  , 'length:6,16', '新パスワードの長さ:6,16'],
       [ 'cur_password'  , 'require|length:6,16|check_cur_hoteldate_update_password:1','現在のパスワード必須|現在のパスワードの長さ:6,16'],
       ['__token__' , 'token']
    ];
    

    
    protected function check_cur_hoteldate_update_password($value) // 自定义验证规则
    {
        $cur_db_hoteldate_password = SSystemconfig::get_db_hoteldate_password();
        return $cur_db_hoteldate_password === $value?true:"現在パスワードを確認してください";
    }
    
   
  

}
