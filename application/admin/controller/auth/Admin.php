<?php

namespace app\admin\controller\auth;

use app\admin\model\AuthGroup;
use app\admin\model\Admin as AdminModel;
use app\admin\model\AuthGroupAccess;
use app\common\controller\Backend;
use fast\Random;
use fast\Tree;

/**
 * 管理员管理
 *
 * @icon fa fa-users
 * @remark 一个管理员可以有多个角色组,左侧的菜单根据管理员所拥有的权限进行生成
 */
class Admin extends Backend
{

    protected $model = null;
    protected $childrenGroupIds = [];
    protected $childrenAdminIds = [];

    public function _initialize() {
        parent::_initialize();
        $this->model = model('Admin');//用helper的model函数装载Admin模块并存入成员

        $this->childrenAdminIds = $this->auth->getChildrenAdminIds(true);//取出本组所有成员ID ,(取出当前管理员所拥有权限的管理员)
        $this->childrenGroupIds = $this->auth->getChildrenGroupIds($this->auth->isSuperAdmin() ? true : false);//取出当前管理员所拥有权限的分组---取出当前用户所在组号`(数组，但是不搞那么复杂)

        $groupList = collection(AuthGroup::where('id', 'in', $this->childrenGroupIds)->select())->toArray();
        $groupIds = $this->auth->getGroupIds();
        Tree::instance()->init($groupList);
        $result = [];
        if ($this->auth->isSuperAdmin()) {
            $result = Tree::instance()->getTreeList(Tree::instance()->getTreeArray(0));
        } else {
            foreach ($groupIds as $m => $n) {
                $result = array_merge($result, Tree::instance()->getTreeList(Tree::instance()->getTreeArray($n)));
            }
        }
        $groupName = [];
        foreach ($result as $k => $v) {
            $groupName[$v['id']] = $v['name'];
        }
        $this->view->assign('groupdata', $groupName);
        
        $this->assignconfig("admin", ['id' => $this->auth->id]);
    }

    /**
     * 查看
     */
    public function index()
    {
        if ($this->request->isAjax())
        {
            $childrenGroupIds = $this->auth->getChildrenAdminIds(true);
            
            $groupName = AuthGroup::where('id', 'in', $childrenGroupIds)
                    ->column('id,name');
            $authGroupList = AuthGroupAccess::where('group_id', 'in', $childrenGroupIds)
                    ->field('uid,group_id')
                    ->select();

            $adminGroupName = [];
            foreach ($authGroupList as $k => $v)
            {
                if (isset($groupName[$v['group_id']]))
                    $adminGroupName[$v['uid']][$v['group_id']] = $groupName[$v['group_id']];
            }

            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->where($where)
                    ->where('id', 'in', $this->childrenAdminIds)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->where($where)
                    ->where('id', 'in', $this->childrenAdminIds)
                    ->field(['password', 'salt', 'token'], true)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            
            foreach ($list as $k => &$v)
            {
                $groups = isset($adminGroupName[$v['id']]) ? $adminGroupName[$v['id']] : [];
                $v['groups'] = implode(',', array_keys($groups));
                $v['groups_text'] = implode(',', array_values($groups));
            }
            unset($v);
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        $sdf=$this->request->isAjax();
        
        if ($this->request->isPost())//新增 确定后， ajax
        {
            $params = $this->request->post("row/a");
            if ($params) {
                
//                $params['salt'] = Random::alnum();
//                $params['password'] = md5(md5($params['password']) . $params['salt']);
                
                $params['avatar'] = '/assets/img/avatar.png'; //设置新管理员默认头像。
                $username_new = $params['username'];
                $admin_exist = AdminModel::get(['username' => $username_new]);
                if ($admin_exist) {
                    $this->error('担当者名重複');
                }
                $admin = $this->model->create($params);
                $group = $this->request->post("group/a"); //  /a是 数组的意思

                $group = array_intersect($this->childrenGroupIds, $group); //过滤不允许的组别,避免越权
                $dataset = [];
                foreach ($group as $value) {
                    $dataset[] = ['uid' => $admin->id, 'group_id' => $value];
                }
                model('AuthGroupAccess')->saveAll($dataset);
                $this->success();
            }
            $this->error();
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        $row = $this->model->get(['id' => $ids]);
        if (!$row)
        {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost())//提交的时候
        {
            $params = $this->request->post("row/a");
            if ($params)
            {
                if ((isset($password))) {
                    $admin = AdminModel::get(['id' => $ids]);
                    if (!$admin) {
                        $this->error();
                    }
                    if ($admin->password != $password) {
                        $this->error('パスワードを確認してください');
                    }
                }

                $row->save($params);
                model('AuthGroupAccess')->where('uid', $row->id)->delete(); // 先移除所有权限
                $group = $this->request->post("group/a");
                $group = array_intersect($this->childrenGroupIds, $group);// 过滤不允许的组别,避免越权
                $dataset = [];
                foreach ($group as $value)
                {
                    $dataset[] = ['uid' => $row->id, 'group_id' => $value];
                }
                model('AuthGroupAccess')->saveAll($dataset);
                $this->success();
            }
            $this->error();
        }
        //↓不是提交的时候，抽出记录给view
        $grouplist = $this->auth->getGroups($row['id']);
        $groupids = [];
        foreach ($grouplist as $k => $v)
        {
            $groupids[] = $v['id'];
        }
        $this->view->assign("row", $row);
        $this->view->assign("groupids", $groupids);
        return $this->view->fetch();
    }

    public function del($ids = "",$password=null)//删除
    {
        if ($ids)
        {
            if((isset($password))) {
                $admin = AdminModel::get(['id' => $ids]);
                if (!$admin) {
                    $this->error();
                }
                //$password = $this->request->get('password', '');
                //if ($admin->password != md5(md5($password) . $admin->salt)) {
                if ($admin->password != $password ) {
                    $this->error('パスワードを確認してください');
                }
            }

            $childrenGroupIds = $this->childrenGroupIds;// 避免越权删除管理员
            $adminList = $this->model->where('id', 'in', $ids)->where('id', 'in', function($query) use($childrenGroupIds) {
                        $query->name('auth_group_access')->where('group_id', 'in', $childrenGroupIds)->field('uid');
                    })->select();
            if ($adminList)
            {
                $deleteIds = [];
                foreach ($adminList as $k => $v)
                {
                    $deleteIds[] = $v->id;
                }
                $deleteIds = array_diff($deleteIds, [$this->auth->id]);
                if ($deleteIds)
                {
                    $this->model->destroy($deleteIds);//删除执行
                    model('AuthGroupAccess')->where('uid', 'in', $deleteIds)->delete();//当然也要删除对应的
                    $this->success();
                }
            }
        }
        $this->error();
    }
    
    public function check_edit_pass() {
        $pass_for_check = $this->request->post("password");
        $id = $this->request->post("id");
        $result_code = 0;
        if ((isset($pass_for_check))) {
            $admin = AdminModel::get(['id' => $id]);
            if (!$admin) {
                $this->error();
            }
            if ($admin->password == $pass_for_check) {
                $result_code = 1;
            }
        }
        $result['code'] =  $result_code;
        $result['data']['no_msg']=$result_code;//パスワード合わない場合、msg表示、合う場合,msg非表示
        $result['msg'] = $result_code ==0?'パスワードを確認してください':'';
        //$result = json($result); //json是助手函数,自动打包json输出到客户端 要不要都可以，客户端用 ret = Fast.events.onAjaxResponse(response);去接就可以收到了
        return $result;
    }


}
