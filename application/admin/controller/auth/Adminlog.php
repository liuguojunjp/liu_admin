<?php

namespace app\admin\controller\auth;

use app\admin\model\AuthGroup;
use app\common\controller\Backend;

/**
 * 管理员日志
 *
 * @icon fa fa-users
 * @remark 管理员可以查看自己所拥有的权限的管理员日志
 */
class Adminlog extends Backend
{
    protected $model = null;
    protected $childrenGroupIds = [];
    protected $childrenAdminIds = [];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('AdminLog');//用helper的model函数装载AdminLog模块并存入成员
        $this->childrenAdminIds = $this->auth->getChildrenAdminIds(true);//取出本组所有成员ID ,(取出当前管理员所拥有权限的管理员)
        $this->childrenGroupIds = $this->auth->getChildrenGroupIds($this->auth->isSuperAdmin() ? true : false);//取出当前管理员所拥有权限的分组---取出当前用户所在组号`(数组，但是不搞那么复杂)
        $groupName = AuthGroup::where('id', 'in', $this->childrenGroupIds)->column('id,name');//获得 id索引，值为name的数组 （条件 id in 前用户所在组号 ）
        $this->view->assign('groupdata', $groupName);//把组名传入view
    }

    /**
     * 查看
     */
    public function index()
    {
        if ($this->request->isAjax())//ajax 返回json
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();//根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了

            $uid = $this->auth->__get('id');//取得当前登录者id

            $total = $this->model
                    ->where($where)
                    // ->where('admin_id', 'in', $this->childrenAdminIds)
                    ->where('admin_id', 'in', $uid)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->where($where)
                    //->where('admin_id', 'in', $this->childrenAdminIds)
                    ->where('admin_id', 'in', $uid)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            $result = array("total" => $total, "rows" => $list);
            $result_json=   json($result);//json是助手函数,自动打包json输出到客户端
   
//          $json_encode= json_encode($result_json);          
//          $json_encode_list= json_encode($list);
//          $arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
//          $json_encode_list= json_encode($arr);
//          $a= sizeof($json_encode_list);
          
             return $result_json;
        }
        return $this->view->fetch();
    }

    /**
     * 详情
     */
    public function detail($ids) {
        $row = $this->model->get(['id' => $ids]);//查找单条记录
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        
        //$row_array=$row->toArray();//转换当前模型对象为数组
       
        $item = [];
        $item['id'] = $row->id;
        $item['担当者ID'] = $row->admin_id;
        $item['担当者名'] = $row->username;
        $item['URL'] = $row->url;
        $item['タイトル'] = $row->title;
        $item['内容'] = $row->content;
        $item['IP'] = $row->ip;
        $item['エージェント'] = $row->useragent;
        $item['ログ時間'] = date('Y/m/d H:i:s', $row->createtime);

        $this->view->assign("row",$item );
        return $this->view->fetch();
    }

    /*
    public function add()
    {
        $this->error();
    }

    public function edit($ids = NULL)
    {
        $this->error();
    }

   
    public function del($ids = "")
    {
        if ($ids)
        {
            $childrenGroupIds = $this->childrenGroupIds;
            $adminList = $this->model->where('id', 'in', $ids)->where('admin_id', 'in', function($query) use($childrenGroupIds) {
                        $query->name('auth_group_access')->field('uid');
                    })->select();
            if ($adminList)
            {
                $deleteIds = [];
                foreach ($adminList as $k => $v)
                {
                    $deleteIds[] = $v->id;
                }
                if ($deleteIds)
                {
                    $this->model->destroy($deleteIds);
                    $this->success();
                }
            }
        }
        $this->error();
    }

    
    public function multi($ids = "")
    {
        // 管理员禁止批量操作
        $this->error();
    }
    
    */
    
//    public function selectpage()
//    {
//        return parent::selectpage();//backend 的selectpage() 是一个比较通用的selectpage方法，这里照搬，可以
//    }

}
