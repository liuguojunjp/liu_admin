<?php
namespace app\admin\controller\test;

use app\common\controller\Backend;
use think\Db;
use app\admin\model\MSalessection;

class Ajax extends Backend
{

    protected $model = null;

    public function _initialize() {
        parent::_initialize();
        //$this->model = model('MRoomtype'); //用helper的model函数装载AdminLog模块并存入成员
    }

    public function index() {
        if ($this->request->isAjax()) {//ajax 返回json
            $row = $this->request->post("row/a");
        }
        return $this->view->fetch();
    }
    
    
     public function part_submit() {
        if ($this->request->isAjax()) {//ajax 返回json
            $row = $this->request->post("row/a");
            
            $this->success(__('part_submit successful'), null, [ 'name1' => $row['name']['name1'],'array1'=>[ 'slip' => 'sliptest', 'payment' => 'paymenttest'] ,'extend' => $row['extend']]);
        }
       
    }
    
    public function add() {
        if ($this->request->isAjax()) {//ajax 返回json
           
           $row = $this->request->post("row/a");
           
           
           $sql = "update m_roomtype set RoomTypeShortName='".$row['title']."'  limit 1";
           $sql=$row['extend'];
           $sfd=  Db::execute($sql);
            
           $sql = 'select a.*,b.RoomTypeShortName from m_room as a 
                     inner join m_roomtype as b on a.RoomTypeNo=b.RoomTypeNo where  1=1 limit 1';
           $list= Db::query($sql);
            
            
          //  $this->success(__('add successful'), null, [ 'name1' => $row['name']['name1'],'array1'=>[ 'slip' => 'sliptest', 'payment' => 'paymenttest'] ,'extend1' => $row['extend1']]);
            
            $this->success(__('add successful'), null,$list);
        }
       
    }
    
    public function change_url() {
        if ($this->request->isAjax()) {//ajax 返回json
            $row = $this->request->post("row/a");
            
            $this->success(__('change_url successful'), null, [ 'name1' => "'{#s",'array1'=>[ 'slip' => 'sliptest', 'payment' => 'paymenttest'] ,'extend1' => $row['extend1']]);
        }
       
    }
    
    
    
    public function direct_ajax() {
        if ($this->request->isAjax()) {//ajax 返回json
            $row = $this->request->post("row/a");
            $this->success(__('direct_ajax successful'), null, [ 'name1' => $row['name']['name1']]);// success,error ,json是think端生成响应的函数，本质都是Response::create($result, 'json')
            
        }
    }
    
    public function direct_ajax_json() {
        if ($this->request->isAjax()) {//ajax 返回json
            $row = $this->request->post("row/a");
            // return json( [ 'name1' => $row['name']['name1'],'array1'=>[ 'slip' => 'sliptest', 'payment' => 'paymenttest'] ] );
            return json(MSalessection::get_id_name_list());
            //  return json( MSalessection::get_value_text_data_1());
            // return json( json_encode(MSalessection::get_value_text_data_1())); //如果在服务器用 json_encode编码（变成了json字符串）的话，客户端如果要再转换成对象的话要用
//             if(typeof data !== 'object')
//                                            {
//                                              ret=  JSON.parse(data);
//                                            }
            //之类的代码 变成 客户端对象来应用
        }
    }

    public function direct_ajax_para() {
        if ($this->request->isAjax()) {//ajax 返回json
            $row = $this->request->post("row/a");
            $this->success(__('direct_ajax_para successful'), null);
        }
       
    }
    
    
    
    
    
    
   
}
