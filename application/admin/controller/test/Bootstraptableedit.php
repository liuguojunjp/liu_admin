<?php
namespace app\admin\controller\test;

use app\common\controller\Backend;
use app\admin\model\MItem;
use app\admin\model\MSalessection;

class Bootstraptableedit extends Backend
{
    protected $model = null;
    public function _initialize() {
        parent::_initialize();
     $this->model = model('MItem'); //用helper的model函数装载AdminLog模块并存入成员
    }
    public function index() {
        
   if ($this->request->isAjax()) {//ajax 返回json
            list($where, $sort, $order, $offset, $limit, $where_arr) = $this->buildparams(); //根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了
            $total = $this->model
                    ->where($where)
                    ->where('PackFlg', '=', '0')
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->alias('MI')
                    ->join('m_salessection MS' , 'MS.SalesSectionCode = MI.SalesSectionCode' )
                    ->join('m_summarysection MSS' , 'MSS.SummarySecCode = MI.SummarySecCode' )
                    ->where($where)
                    ->where('MI.PackFlg', '=', '0')
                    ->field("MI.SortNo ,"
                           // . "CASE WHEN MI.DispFlg = 0 THEN '非表示' WHEN MI.DispFlg = 1 THEN '表示' END as DispFlg ,"
                            . "MI.DispFlg ,"
                            . "'2018/12/10' as DateEditTest ,"
                            . "MI.ItemNo ,"//as 科目ID
                            . "MI.ItemName  ,"//as 科目名
                            . "MI.BasePrice ,"//as 基本料金
                            . "MI.ServiceRate ,"//as サービス料率
                            . "CASE WHEN MI.ServiceKbn = 0 THEN 'なし' WHEN MI.ServiceKbn = 1 THEN '別' WHEN MI.ServiceKbn = 2 THEN '込' END as サービス料区分,"//
                            . "CASE WHEN MI.TaxKbn = 0 THEN 'なし' WHEN MI.TaxKbn = 1 THEN '外税' WHEN MI.TaxKbn = 2 THEN '内税' END as 消費税区分,"//
                            . "CASE WHEN MI.SpaTaxKbn = 0 THEN 'なし' WHEN MI.SpaTaxKbn = 1 THEN '外税' WHEN MI.SpaTaxKbn = 2 THEN '内税' END as 入湯税区分,"//
                            . " func_get_item_name(MI.ItemKindCode)  as 科目種別, "//
                            . "CASE WHEN MI.ReceiptPrtKbn = 0 THEN '非印字' WHEN MI.ReceiptPrtKbn = 1 THEN '印字' END  as 印字区分,"//
                            . "CASE WHEN MI.PackFlg = 0 THEN '無' WHEN MI.PackFlg = 1 THEN 'パック' END  as パック,"//
                            . "CASE WHEN MI.PeopleKbn = 1 THEN '大人' WHEN MI.PeopleKbn = 2 THEN '子A' WHEN MI.PeopleKbn = 3 THEN '子B' WHEN MI.PeopleKbn = 4 THEN '子C' WHEN MI.PeopleKbn = 5 THEN '幼児' ELSE ' ' END as 入区分,"//
                            . "CASE WHEN MI.FoodKbn = 1 THEN '朝食' WHEN MI.FoodKbn = 2 THEN '朝食その他' WHEN MI.FoodKbn = 3 THEN '夕食' WHEN MI.FoodKbn = 4 THEN '夕食その他' WHEN MI.FoodKbn = 5 THEN '昼食' WHEN MI.FoodKbn = 6 THEN '昼食その他' ELSE ' 　 ' END as 料理区分, "//
                            . "CASE WHEN MI.ServiceContactReportDispKbn = 0 THEN '非表示' WHEN MI.ServiceContactReportDispKbn = 1 THEN '表示'  ELSE ' 　 ' END as 業務連絡表区分,"//
                            . "CASE WHEN MI.StayTaxKbn = 0 THEN 'なし' WHEN MI.StayTaxKbn = 1 THEN '外税' WHEN MI.StayTaxKbn = 2 THEN '内税' END as 宿泊税区分,"//
                            . "CASE WHEN MI.StayTaxAutoKbn = 0 THEN 'しない' WHEN MI.StayTaxAutoKbn = 1 THEN 'する' END as 宿泊税自動算出区分,"//
                            . "MI.StayTaxMstSummary as 宿泊税額, "//
                            . "MS.SalesSectionCode ,"
                            . "MSS.SummarySecName as 集計部門"
                            . "")
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            
            $result = array("total" => $total, "rows" => $list);
            $result_json = json($result); //json是助手函数,自动打包json输出到客户端
            return $result_json;
        }
        
        return $this->view->fetch();
    }
    
    
    

    public function edit1() {
        
        $i=9;
        
        $ItemCode = input('ItemNo');
        $ItemName = input('ItemName');
        $DispFlg = input('DispFlg');
        $SortNo = input('SortNo');
        $SalesSectionCode = input('SalesSectionCode');

 
        if (!MItem::update_one_field($ItemCode, 'SortNo', $SortNo,$this->auth->__get('id'))) {
            $this->error("表示順番 更新失敗");
        }
        if (!MItem::update_one_field($ItemCode, 'ItemName', $ItemName,$this->auth->__get('id'))) {
            $this->error("科目名 更新失敗");
        }
        if (!MItem::update_one_field($ItemCode, 'DispFlg', $DispFlg,$this->auth->__get('id'))) {
            $this->error("表示フラグ 更新失敗");
        }
        if (!MItem::update_one_field($ItemCode, 'SalesSectionCode', $SalesSectionCode,$this->auth->__get('id'))) {
            $this->error("売上部門 更新失敗");
        }
        
        $this->success();
    }
    public function get_sale_dep_list() {
        if (!$this->request->isAjax()) {
            return;
        }
        return MSalessection::get_id_name_list();
        
    }

}
