<?php
namespace app\admin\controller\test;

use app\common\controller\Backend;

class Fullcalendar extends Backend
{
    protected $model = null;
    public function _initialize() {
        parent::_initialize();
        $this->model = model('Canlendar'); //用helper的model函数装载AdminLog模块并存入成员
    }
    public function index() {
        $lists = $this->model->field("id,title,start,end,code")->select();
        foreach ($lists as $k => $v) {
            $lists[$k]['title'] = htmlspecialchars($v['title']);
            $lists[$k]['className'] = $v['code'];
            $lists[$k]['allDay'] = getCanlendarAllday($v['start'], $v['end']);
        }
//        $events = json_encode($lists);
////      print_r($lists);
//        $this->assign("lists", $lists);
//        $this->assign("title", "历史跟踪");
        $this->assign("events", $lists);
        
        return $this->view->fetch();
    }
    
      public function postEvent() {
        $id = input('post.id', '', 'int');
        $data['title'] = input('post.title');
        $data['start'] = input('post.start');
        $data['end'] = input('post.end');
        $data['code'] = input('post.code');
        $data['uid'] = $this->auth->id;
//        print_r($data);exit;
        if ($id > 0) {
          //  $this->model->where("id = " . $id . "")->save($data);
            $row = $this->model->get(['id' => $id]);
        $row->save($data);
            echo json_encode(array("id" => $id));
        } else {
            $lastid = $this->model->create($data);
            echo json_encode(array("id" => $lastid));
        }
    }

    public function daysEvent() {
        $id = input('post.id', '', 'int');
        $code = input('post.code');
        $detail = $this->model->field("end,start")->where("id = " . $id . "")->find();
        $days_strtoitime = input("post.days") * 3600 * 24;
        $end = strtotime($detail['end']) + $days_strtoitime;
        $data['end'] = date("Y-m-d H:i:s", $end);

        if ($code == 'drop') {
            $start = strtotime($detail['start']) + $days_strtoitime;
            $data['start'] = date("Y-m-d H:i:s", $start);
        }
        $row = $this->model->get(['id' => $id]);
        $row->save($data);
        echo 1;
    }
    public function delEvent() {
        $id = input('post.id', '', 'int');
        $this->model->where("id = " . $id . "")->delete();
    }
}
