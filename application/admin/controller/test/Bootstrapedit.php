<?php
namespace app\admin\controller\test;

use app\common\controller\Backend;

class Bootstrapedit extends Backend
{
    protected $model = null;
    public function _initialize() {
        parent::_initialize();
        $this->model = model('MRoomtype'); //用helper的model函数装载AdminLog模块并存入成员
    }
    public function index() {
        
    if ($this->request->isAjax()) {//ajax 返回json
            list($where, $sort, $order, $offset, $limit, $where_arr) = $this->buildparams(); //根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了
            $total = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->where($where)
                    ->field('*')
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            
//            
//          $total=  3;
//          $list =  MRoomtype::get_db_roomtype(); //这样也可以

            $result = array("total" => $total, "rows" => $list);
            $result_json = json($result); //json是助手函数,自动打包json输出到客户端
            return $result_json;
        }
        
        return $this->view->fetch();
    }
    
    
    public function edit1($ids) {
        
        $i=9;
        
        $roomtypename_input=input('RoomTypeShortName');
        
        $this->success();
        
    }
    
      
}
