<?php
namespace app\admin\controller\othermaintenance;
use app\common\controller\Backend;
use app\admin\model\HUsehis;
class Usehissearch extends Backend
{

  protected $model = null;
  
  public function _initialize() {
        parent::_initialize();
        $this->model = model('HUsehis'); //用helper的model函数装载AdminLog模块并存入成员
    }
    public function index()
    {
           if ($this->request->isAjax()) {//ajax 返回json
               
            $isHaveDel=false;
            $filter = $this->request->get("filter", '');
            if (strpos($filter, 'IsDel') !==FALSE) {
                $isHaveDel=true;
                
            }
            list($where, $sort, $order, $offset, $limit, $where_arr) = $this->buildparams(); //根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了
            $field_str=" CASE  WHEN `Comment` LIKE 'Del%' THEN 'Del' ELSE ''  END  as 'IsDel'"//削除
                            . ", date_format(`UpdateDate`, '%Y-%m-%d %H:%i') as 'UpdateDate'"
                            . ",`UpdatePersonName` as 'UpdatePersonName'"
                            . ",`ResvNo` "//as '予約番号'
                            . ",CASE `UseStatus` WHEN 11 THEN '予約'  WHEN 21 THEN '在室' WHEN 31 THEN 'アウト' ELSE ''  END  as 'UseStatus'"//as '状態'
                            . ",date_format(`Cindate`, '%Y-%m-%d') as 'Cindate' "//as 'ﾁｪｯｸｲﾝ日'
                            . ",date_format(`Coutdate`, '%Y-%m-%d') as 'Coutdate' "//as 'ﾁｪｯｸｱｳﾄ日'
                            . ",`Stay` "//as '泊数'
                             . ",`ClientName` "//as '顧客名'
                             . ",`ClientKana`  "//as '顧客カナ'
                             . ",`BaseItemName` "//as '基本科目'
                             . ",`BasePrice` "//as '基本単価'
                             . ",`TotalSummary` "//as '合計金額'
                             . ",`PaySummary` "//as '入金額'
                             . ",`RoomCount` "//as '部屋数'
                             . ",`PeopleCount` "//as '人数'
                             . ",`Memo`  "//as 'メモ'
                            . ",`MachineName` "//as '端末名'
                             . ",`ViaView` "//as '画面'
                            . ",`ApplicationUser` "//as '申込者'
                             . ",`UseHisUpdateMemo` "//as '利用変更メモ'
                            . ",`ID`  ";
            
            if ($isHaveDel) {
                unset($where['IsDel']);
                $where['Comment'] = array("LIKE", "Del%");
            }
            $total = $this->model->where($where)->order($sort, $order)->count();
            $list = $this->model->where($where) ->field($field_str)->order($sort, $order)->limit($offset, $limit) ->select();
 
            $result = array("total" => $total, "rows" => $list);
            $result_json = json($result); //json是助手函数,自动打包json输出到客户端, Response::create( $data ,'json', 200,...
            return $result_json;
        }
        

        return $this->view->fetch();
        
        
        
    }

   

}
