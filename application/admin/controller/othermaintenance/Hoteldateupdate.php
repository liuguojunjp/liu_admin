<?php

namespace app\admin\controller\othermaintenance;

use think\Validate;
use app\common\controller\Backend;
use app\admin\model\SSystemconfig;
use app\admin\validate\HoteldateUpdateValidate;

class Hoteldateupdate extends Backend {

    public function index() {
        $cur_hoteldate = SSystemconfig::get_cur_hoteldate();
        $this->view->assign('cur_hoteldate', $cur_hoteldate);
        return $this->view->fetch();
    }

    public function update() {//更新
        if ($this->request->isPost() && IS_AJAX) {
            $token = $this->request->post('__token__');
            $params = $this->request->post("row/a");
            //$params = array_filter(array_intersect_key($params, array_flip(array('cur_hoteldate', 'new_hoteldate', 'cur_password', 'password')))); //初始值处理?空的不要？

//            $cur_db_hoteldate_password = SSystemconfig::get_db_hoteldate_password(); // validate に移動した
//            if ($cur_db_hoteldate_password !== $params['cur_password']) {
//                $this->error('現在パスワードを確認してください');
//            }
            
            if (strlen($params['cur_hoteldate']) == 0 || !Validate::dateFormat($params['cur_hoteldate'], 'Y/m/d')) {// just for code demo
                $this->error('現在のホテル日フォーマットエラー');
            }

            $data = [
                'hoteldate' => $params['new_hoteldate'],
                'cur_password' => $params['cur_password'],
                'new_password' => $params['new_password'],
                '__token__' => $token,
            ];
            
            $validate = new HoteldateUpdateValidate();
            // or  $validate = Loader::validate('HoteldateUpdateValidate');
            // or  $validate = validate('HoteldateUpdateValidate');

            $result = $validate->check($data);
            //如果需要批量验证，可以使用： $result   = $validate->batch()->check($data); //批量验证如果验证不通过，返回的是一个错误信息的数组。
            if (!$result) {
                $this->error($validate->getError(), null, ['token' => $token]);
            }

            //检查date格式,password 格式
            if ($params) {
                $success_msg = '';
                //model('admin')->where('id', $this->auth->id)->update($params);
                if ($params['new_password'] != $params['cur_password'] && \strlen($params['new_password']) > 0) {//パスワード更新
                    $is_update_success = SSystemconfig::update_hoteldate_password($params['new_password'], $params['cur_password']); //更新
                    $success_msg .= ($is_update_success ? '　パスワード更新成功' : '　パスワード更新失敗');
                }
                if ($params['new_hoteldate'] != $params['cur_hoteldate']) {
                    $is_update_success=SSystemconfig::update_hoteldate($params['cur_hoteldate'], $params['new_hoteldate']); //更新
                    $success_msg .= ($is_update_success ? '　ホテル日更新成功' : ' ホテル日更新失敗');
                }
                $this->success($success_msg);
            }
            $this->error('登録データがありません');
        }
        return;
    }

}
