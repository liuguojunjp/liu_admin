<?php

namespace app\admin\controller\mastersetting;

use app\common\controller\Backend;
use app\admin\model\SOptiondata;

class Controlmaster extends Backend {
    //protected $model = null;

    public function _initialize() {
        parent::_initialize();
        //$this->model  = new SOptiondata();
    }

    public function index() {//相当于formload，非ajax
       
        $option_list=array();
        
        $option_list['SysOption_FractionKbn']=array('tab_id'=>'tab_basic','title'=>'端数処理','value'=>'0','view_type'=>'select','data_rule'=>'','data_tip'=>'','extend'=>'','select_list'=>array('0'=>'切り捨て','1'=>'切り上げ'));
        $option_list['ResvForm_DefCinTime']=array('tab_id'=>'tab_basic','title'=>'到着時刻','value'=>'10:00','view_type'=>'textbox_timepicker','data_rule' => 'required', 'data_tip' => 'フォーマット:(半角)HH:mm,　例：15:00 ', 'extend' => '');
        $option_list['ResvForm_DefCoutTime']=array('tab_id'=>'tab_basic','title'=>'出発時刻','value'=>'15:00','view_type'=>'textbox_timepicker','data_rule' => 'required', 'data_tip' => 'フォーマット:(半角)HH:mm,　例：10:00 ', 'extend' => '');
        $option_list['SysOption_RealizationBasis']=array('tab_id'=>'tab_basic','title'=>'集計区分','value'=>'0','view_type'=>'select','data_rule'=>'','data_tip'=>'','extend'=>'','select_list'=>array('0'=>'発生主義','1'=>'実現主義'));
        $option_list['SysOption_OutdateBasis']=array('tab_id'=>'tab_basic','title'=>'アウト日計上区分','value'=>'0','view_type'=>'select','data_rule'=>'','data_tip'=>'','extend'=>'','select_list'=>array('0'=>'しない','1'=>'する'));
        $option_list['SysOption_StDateYerMon']=array('tab_id'=>'tab_basic','title'=>'期首月','value'=>'01','view_type'=>'select','data_rule'=>'','data_tip'=>'','extend'=>'',
            'select_list'=>array('01'=>'1月','02'=>'2月','03'=>'3月','04'=>'4月','05'=>'5月','06'=>'6月','07'=>'7月','08'=>'8月','09'=>'9月','10'=>'10月','11'=>'11月','12'=>'12月' ));
        
        $shime_select_arr=array();
        for($i = -1; $i <= 31; $i++) {
           $shime_select_arr[$i]=''.$i.'日';
        }
        $option_list['SysOption_ShimeKbn']=array('tab_id'=>'tab_basic','title'=>'締め日','value'=>'0','view_type'=>'select','data_rule'=>'','data_tip'=>'','extend'=>'',
            'select_list'=>$shime_select_arr);

        $option_list['SysOption_StayOpeRoomCount']=array('tab_id'=>'tab_basic','title'=>'宿泊稼働部屋数','value'=>0,'view_type'=>'number','data_rule'=>'required','data_tip'=>'','extend'=>'');
        $option_list['SysOption_RestOpeRoomCount']=array('tab_id'=>'tab_basic','title'=>'休憩稼働部屋数','value'=>0,'view_type'=>'number','data_rule'=>'required','data_tip'=>'','extend'=>'');
        
        $option_list['ReceiptOption_isReceiptTypeA4']=array('tab_id'=>'tab_receipt','title'=>'用紙タイプ','value'=>0,'view_type'=>'radio','data_rule'=>'','data_tip'=>'','extend'=>'',
            'radio_content'=> array('0'=>'B5','1'=>'A4正副')
            );
        
         $option_list['ReceiptOption_isReceiptSumSub']=array('tab_id'=>'tab_receipt','title'=>'領収額から差し引く','value'=>1,'view_type'=>'checkbox_group','data_tip'=>'','extend'=>'',
            'checkbox_group_content'=> SOptiondata::get_checkbox_group_content_isReceiptSumSub()
            );
         
         $option_list['ReceiptOption_isRequestSumSub']=array('tab_id'=>'tab_receipt','title'=>'請求額から差し引く','value'=>1,'view_type'=>'checkbox_group','data_tip'=>'','extend'=>'',
            'checkbox_group_content'=> SOptiondata::get_checkbox_group_content_isRequestSumSub()
            );
        
       
        $option_list['ResvForm_FreeSummary1Name'] = array('tab_id' => 'tab_freeitem', 'title' => '自由集計種別1名称', 'value' => '自由集計種別1', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['ResvForm_FreeSummary2Name'] = array('tab_id' => 'tab_freeitem', 'title' => '自由集計種別2名称', 'value' => '自由集計種別2', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['ResvForm_FreeSummary3Name'] = array('tab_id' => 'tab_freeitem', 'title' => '自由集計種別3名称', 'value' => '自由集計種別3', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['ResvForm_FreeSummary4Name'] = array('tab_id' => 'tab_freeitem', 'title' => '自由集計種別4名称', 'value' => '自由集計種別4', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['ResvForm_ClientFreeSummary1Name'] = array('tab_id' => 'tab_freeitem', 'title' => '顧客自由項目1名称', 'value' => '顧客自由項目1', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['ResvForm_ClientFreeSummary2Name'] = array('tab_id' => 'tab_freeitem', 'title' => '顧客自由項目2名称', 'value' => '顧客自由項目2', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['ResvForm_ClientFreeSummary3Name'] = array('tab_id' => 'tab_freeitem', 'title' => '顧客自由項目3名称', 'value' => '顧客自由項目3', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['ResvForm_CropFreeSummary1Name'] = array('tab_id' => 'tab_freeitem', 'title' => ' 会社自由項目1名称', 'value' => ' 会社自由項目1', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['ResvForm_CropFreeSummary2Name'] = array('tab_id' => 'tab_freeitem', 'title' => ' 会社自由項目2名称', 'value' => ' 会社自由項目2', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['ResvForm_CropFreeSummary3Name'] = array('tab_id' => 'tab_freeitem', 'title' => ' 会社自由項目3名称', 'value' => ' 会社自由項目3', 'view_type' => 'textbox', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');


        $option_list['SysOption_SpaTaxSummary'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(宿泊/大人)', 'value' => 150, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['SysOption_SpaAmountStayChildA'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(宿泊/子供A)', 'value' => 0, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['SysOption_SpaAmountStayChildB'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(宿泊/子供B)', 'value' => 0, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['SysOption_SpaAmountStayChildC'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(宿泊/子供C)', 'value' => 0, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['SysOption_SpaAmountStayBaby'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(宿泊/幼児)', 'value' => 0, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['SysOption_SpaAmountShortStayAdult'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(休憩/大人)', 'value' => 0, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['SysOption_SpaAmountShortStayChildA'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(休憩/子供A)', 'value' => 0, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['SysOption_SpaAmountShortStayChildB'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(休憩/子供B', 'value' => 0, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['SysOption_SpaAmountShortStayChildC'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(休憩/子供C)', 'value' => 0, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');
        $option_list['SysOption_SpaAmountShortStayBaby'] = array('tab_id' => 'tab_tax', 'title' => '入湯税(休憩/幼児)', 'value' => 0, 'view_type' => 'number', 'data_rule' => 'required', 'data_tip' => '', 'extend' => '');

        $db_all_optionlst= SOptiondata::get_db_all_optionlst();//静态方式最好用
        foreach ($db_all_optionlst as $key => $db_option_value) {
            if (array_key_exists($key, $option_list)) {
                $option_list[$key]['value'] = $db_option_value;
            }
        }
        
        foreach (SOptiondata::get_checkbox_group_content_isReceiptSumSub() as $key => $checkbox_detail) {
            if (array_key_exists($key, $db_all_optionlst)) {
                $option_list['ReceiptOption_isReceiptSumSub']['checkbox_group_content'][$key]['checkbox_value']=$db_all_optionlst[$key];
            }
        }
        
        foreach (SOptiondata::get_checkbox_group_content_isRequestSumSub() as $key => $checkbox_detail) {
            if (array_key_exists($key, $db_all_optionlst)) {
                $option_list['ReceiptOption_isRequestSumSub']['checkbox_group_content'][$key]['checkbox_value']=$db_all_optionlst[$key];
            }
        }
        
       
        $this->view->assign('option_list', $option_list);
       
        $tab_list=array();
        $tab_list['tab_basic']=array('title'=>'システム','is_active'=>true,'is_show'=>true);
        $tab_list['tab_freeitem']=array('title'=>'自由項目名称','is_active'=>false,'is_show'=>true);
        $tab_list['tab_receipt']=array('title'=>'領 収 書','is_active'=>false,'is_show'=>true);
        $tab_list['tab_tax']=array('title'=>'税 計 算','is_active'=>false,'is_show'=>true);
        //$tab_list['tab_zaimu']=array('title'=>'財務変換','is_active'=>false,'is_show'=>false);

        $this->view->assign('tab_list', $tab_list); 
        
        return $this->view->fetch();
    }

    public function update() {//各个动作，ajax
        if (!$this->request->isAjax()) {//ajax 返回json
            return;
        }
        $ctl_mst_setting = $this->request->post("ctl_mst_setting/a");
        foreach (SOptiondata::get_checkbox_group_content_isReceiptSumSub() as $key => $checkbox_detail) {//0 不提交上来的？
                $ctl_mst_setting[$key]= (isset($ctl_mst_setting[$key])&&$ctl_mst_setting[$key]==1)?1:0;
        }
        foreach (SOptiondata::get_checkbox_group_content_isRequestSumSub() as $key => $checkbox_detail) {//0 不提交上来的？
                $ctl_mst_setting[$key]= (isset($ctl_mst_setting[$key])&&$ctl_mst_setting[$key]==1)?1:0;
        }
        $curLoginUseID = $this->auth->__get('id');
        if (SOptiondata::update_option_lst($curLoginUseID, $ctl_mst_setting)) {
            $this->success(__('変更成功しました'), null, null);
        } else {
            $this->error("変更失敗しました", null, null);
        }
    }

}
