<?php
namespace app\admin\controller\mastersetting;

use app\common\controller\Backend;
use app\admin\model\MAgent;
use app\admin\model\MFreeitem;

class Freeitemmaster extends Backend
{

    protected $model = null;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('MFreeitem');//用helper的model函数装载AdminLog模块并存入成员
    }
    
    
    public function index()
    {
        if ($this->request->isAjax())//ajax 返回json
        {
            list($where, $sort, $order, $offset, $limit,$where_arr) = $this->buildparams();//根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了
            $total = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

          $list =  $this->model
                    ->alias('a')
                    ->join('m_freetitle b' , 'b.FreeID = a.FreeID' )
                    ->where($where)
                    ->field('a.*,b.FreeTitle')
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();// update dateなら、曖昧問題？            
            $result = array("total" => $total, "rows" => $list);
            $result_json=   json($result);//json是助手函数,自动打包json输出到客户端
            return $result_json;
        }
        return $this->view->fetch();
    }
    
    
    public function add()
    {
        if ($this->request->isAjax()&&$this->request->isPost()) {//新增 确定后， ajax
            $row = $this->request->post("row/a"); // /a是 数组的意思
            
            if ($row) {
                $pk_exist = $this->model->get(['FreeID' => $row['FreeID'],'FreeItemNo' => $row['FreeItemNo']]);
                if ($pk_exist) {
                    $this->error('自由集計ID+自由集計項目ID重複');
                }
                $row['UpdateDate']=date("Y/m/d H:i:s");
                $row['UpdatePerson']= $this->auth->__get('id');
                $this->model->create($row);
                $this->success();
            }
            $this->error();
        }
        

        $this->view->assign('FreeitemSelectData', \app\admin\model\MFreetitle::getFreeitemSelectData());
        return $this->view->fetch();
    }

   public function check_mul_pk() {
        if (!$this->request->isAjax()) {
            return;
        }
        $row = $this->request->post("row/a");
        $pk_exist = $this->model->get(['FreeID' => $row['FreeID'],'FreeItemNo' => $row['FreeItemNo']]);
        $result =$pk_exist?array("error" => "自由集計ID+自由集計項目ID重複"): array("OK" => "OK");
        $result_json = json($result); //json是助手函数,自动打包json输出到客户端
        return $result_json;
    }
    
    public function del($ids = "") {
        if ($ids) {
            $pk_del = explode(',', $ids);
            $pk = $this->model->getPk();
            $del_row = $this->model->where($pk[0], 'in', $pk_del[0])->where($pk[1], 'in', $pk_del[1])->select();
            if (count($del_row) == 1) {
                $this->model->where($pk[0], 'in', $pk_del[0])->where($pk[1], 'in', $pk_del[1])->delete();
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function edit($ids = NULL) {
        if (!$ids) {
            $this->error();
        }
        $pk_edit = explode(',', $ids);
        $pk = $this->model->getPk();
        $row = $this->model->where($pk[0], 'in', $pk_edit[0])->where($pk[1], 'in', $pk_edit[1])->select();
        if ($this->request->isPost()) {//提交的时候
            if (count($row) == 1) {
                $params = $this->request->post("row/a");
                if ($params) {
                    $params['UpdateDate'] = date("Y/m/d H:i:s");
                    $params['UpdatePerson'] = $this->auth->__get('id');
                    $row[0]->save($params);
                    $this->success();
                }
                $this->error();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        //↓不是提交的时候，抽出记录给view
        $this->view->assign("row", $row[0]);
      
        $this->view->assign('FreeitemSelectData', \app\admin\model\MFreetitle::getFreeitemSelectData());
        return $this->view->fetch();
    }

    
    public function fastedit($field_name) {
        if (!$this->request->isAjax()) {
            return;
        }
        $ItemCode = $this->request->post('FreeID');
        $ItemCode1 = $this->request->post('FreeItemNo');
        if (!MFreeitem::update_one_field_TwoPk($ItemCode,$ItemCode1 ,$field_name, $this->request->post($field_name),$this->auth->__get('id'))) {
            $this->error("$field_name 更新失敗。", null, ['field_name' => $field_name, 'no_msg' => 1]); //本质也是 Response::create( $data ,'json', ,...
        }
        $this->success(" 更新成功!", null, ['field_name' => $field_name, 'no_msg' => 0]); //本质也是 Response::create( $data ,'json',...
    }
}
