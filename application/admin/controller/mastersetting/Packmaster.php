<?php

namespace app\admin\controller\mastersetting;

use app\common\controller\Backend;
use app\admin\model\MItem;
use app\admin\model\MPack;
use app\admin\model\MSalessection;
use app\admin\model\MSummarysection;

class Packmaster extends Backend {

    protected $model = null;

    public function _initialize() {
        parent::_initialize();
        $this->model = model('MItem'); //用helper的model函数装载AdminLog模块并存入成员
    }

    public function index() {
        if ($this->request->isAjax()) {//ajax 返回json
            list($where, $sort, $order, $offset, $limit, $where_arr) = $this->buildparams(); //根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了
            $total = $this->model
                    ->where($where)
                    ->where('PackFlg', '=', '1')
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->alias('a')
                    ->join('m_pack b' , 'a.ItemNo = b.ItemNoPack' )
                    ->where($where)
                    ->where('a.PackFlg', '=', '1')
                    ->field("a.ItemNo  ,"//as パックID
                            . "a.ItemName ,"//as パック名
                            . "count(b.ItemNoChild) as child_count "//as 子科目数
                            . "")
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->group('a.ItemNo')
                    ->select();
            
            $result = array("total" => $total, "rows" => $list);
            $result_json = json($result); //json是助手函数,自动打包json输出到客户端
            return $result_json;
        }
        return $this->view->fetch();
    }

    
    public function add() {
        if ($this->request->isAjax()&&$this->request->isPost()) {//新增 确定后， ajax
            $row = $this->request->post("row/a"); // /a是 数组的意思
            $PackBaseitem = $this->request->post("PackBaseitem");
            $PackBaseitemAmount = $this->request->post("PackBaseitemAmount");
            $PackAdditems = $this->request->post("PackAdditems/a"); // /a是 数组的意思
            
            if ($row) {
                $pk_exist = $this->model->get(['ItemNo' => $row['ItemNo']]);
                if ($pk_exist) {
                    $this->error('科目ID重複');
                }
                $row['PackFlg']=1;
                $row['UpdateDate']=date("Y/m/d H:i:s");
                $row['UpdatePerson']= $this->auth->__get('id');
                $created_model= $this->model->create($row);
                if(!$created_model)
                {
                     $this->error('パック詳細追加失敗しました。');
                }
                
                
                $insert_pack_mst=array();
                $insert_pack_mst[0]=array('ItemNoPack'=>$row['ItemNo'],'Seq'=>0,'ItemNoChild'=>$PackBaseitem,'Amount'=>(int)$PackBaseitemAmount,'UpdateDate'=>date("Y/m/d H:i:s"),'UpdatePerson'=>$this->auth->__get('id'));
                
                $add_item_seq=1;
                foreach ($PackAdditems['additem_id'] as $id_key => $PackAdditem_id) {
                    $amount = 0;
                    foreach ($PackAdditems['additem_amount'] as $amount_key => $PackAdditem_amount) {
                        if (((int) $id_key) === ((int) $amount_key)) {
                            $amount = (int) $PackAdditem_amount;
                            break;
                        }
                    }
                    $insert_pack_mst[$add_item_seq] = array('ItemNoPack' => $row['ItemNo'], 'Seq' => $add_item_seq, 'ItemNoChild' => $PackAdditem_id, 'Amount' => $amount, 'UpdateDate' => date("Y/m/d H:i:s"), 'UpdatePerson' => $this->auth->__get('id'));
                    $add_item_seq++;
                }
                
                foreach ($insert_pack_mst as $insert_pack_mst_item) {  
                   $insert_pack_mst_item_inserted= model('MPack')->create($insert_pack_mst_item);
                   if(!$insert_pack_mst_item_inserted)
                   {
                       $this->error('パック内容追加失敗しました');
                   }
                }

                $this->success();
            }
            $this->error();
        }
        
        $this->view->assign('SalessectionData', MSalessection::get_id_name_list());
        $this->view->assign('SummarysectionData', MSummarysection::get_id_name_list() );
        $this->view->assign('ItemKindData',\app\admin\model\envdata\EnvData:: get_ItemKindData());
        $this->view->assign('ReceiptPrtKbnData',  \app\admin\model\envdata\EnvData:: get_ReceiptPrtKbnData());
        $this->view->assign('ServiceKbnData', \app\admin\model\envdata\EnvData:: get_ServiceKbnData());
        $this->view->assign('TaxKbnData', \app\admin\model\envdata\EnvData:: get_TaxKbnData());
        $this->view->assign('DispFlgData', \app\admin\model\envdata\EnvData:: get_DispFlgData());
        $this->view->assign('PeopleKbnData', \app\admin\model\envdata\EnvData:: get_PeopleKbnData());
        
        $PackBaseitemIni = null;
        $PackBaseitemLst = MItem::get_base_items();
        if (count($PackBaseitemLst) > 0) {
            reset($PackBaseitemLst); 
            //end($PackBaseitemLst); 
            $PackBaseitemIni = key($PackBaseitemLst);
        }
        $this->view->assign('PackBaseitemLst', $PackBaseitemLst);
        $this->view->assign('PackBaseitemIni', $PackBaseitemIni);
        
        
        $PackAdditemIni = null;
        $PackAdditemLst = MItem::get_add_items();
        if (count($PackAdditemLst) > 0) {
            reset($PackAdditemLst); 
            //end($PackBaseitemLst); 
            $PackAdditemIni = key($PackAdditemLst);
        }
        $this->view->assign('PackAdditemLst', $PackAdditemLst);
        $this->view->assign('PackAdditemIni', $PackAdditemIni);

        return $this->view->fetch();
    }
    
      public function check_mul_pk() {
        if (!$this->request->isAjax()) {
            return;
        }
        $row = $this->request->post("row/a");
        $pk_exist = $this->model->get(['ItemNo' => $row['ItemNo']]);
        $result =$pk_exist?array("error" => "科目ID重複"): array("OK" => "OK");
        $result_json = json($result); //json是助手函数,自动打包json输出到客户端
        return $result_json;
    }
   
    
    public function edit($ids = NULL)
    {
        $row = $this->model->get(['ItemNo' => $ids]);
        if (!$row) {
            $this->error(__('結果が見つかりません '));
        }
        $pack_row = model('MPack')->where('ItemNoPack', '=', $ids )->select();
        if (!$pack_row) {
            $this->error(__('結果が見つかりません '));
        }
        
        if ($this->request->isAjax()&&$this->request->isPost()) {//提交的时候
            
            $para = $this->request->post("row/a"); // /a是 数组的意思
            $PackBaseitem = $this->request->post("PackBaseitem");
            $PackBaseitemAmount = $this->request->post("PackBaseitemAmount");
            $PackAdditems = $this->request->post("PackAdditems/a"); // /a是 数组的意思
            
            if ($para) {
                
                $para['PackFlg']=1;
                $para['UpdateDate']=date("Y/m/d H:i:s");
                $para['UpdatePerson']= $this->auth->__get('id');
                $saved_model= $row->save($para);
                if(!$saved_model)
                {
                     $this->error('パック詳細変更失敗しました。');
                }
                
                $model_pack = model('MPack');
                $pk_pack = $model_pack->getPk();
                $model_pack->where($pk_pack[0], 'in', $ids)->delete();
                
                $insert_pack_mst=array();
                $insert_pack_mst[0]=array('ItemNoPack'=>$ids,'Seq'=>0,'ItemNoChild'=>$PackBaseitem,'Amount'=>(int)$PackBaseitemAmount,'UpdateDate'=>date("Y/m/d H:i:s"),'UpdatePerson'=>$this->auth->__get('id'));
                
                $add_item_seq=1;
                foreach ($PackAdditems['additem_id'] as $id_key => $PackAdditem_id) {
                    $amount = 0;
                    foreach ($PackAdditems['additem_amount'] as $amount_key => $PackAdditem_amount) {
                        if (((int) $id_key) === ((int) $amount_key)) {
                            $amount = (int) $PackAdditem_amount;
                            break;
                        }
                    }
                    $insert_pack_mst[$add_item_seq] = array('ItemNoPack' => $ids, 'Seq' => $add_item_seq, 'ItemNoChild' => $PackAdditem_id, 'Amount' => $amount, 'UpdateDate' => date("Y/m/d H:i:s"), 'UpdatePerson' => $this->auth->__get('id'));
                    $add_item_seq++;
                }
                foreach ($insert_pack_mst as $insert_pack_mst_item) {  
                   $insert_pack_mst_item_inserted= $model_pack->create($insert_pack_mst_item);
                   if(!$insert_pack_mst_item_inserted)
                   {
                       $this->error('パック内容更新失敗しました');
                   }
                }
                $this->success();
            }
            $this->error();
        }
        
        $this->view->assign("row", $row);//↓不是提交的时候，抽出记录给view
        
        $this->view->assign('SalessectionData', MSalessection::get_id_name_list());
        $this->view->assign('SummarysectionData', MSummarysection::get_id_name_list() );
        $this->view->assign('ItemKindData', \app\admin\model\envdata\EnvData::get_ItemKindData());
        $this->view->assign('ReceiptPrtKbnData',  \app\admin\model\envdata\EnvData:: get_ReceiptPrtKbnData());
        $this->view->assign('ServiceKbnData', \app\admin\model\envdata\EnvData:: get_ServiceKbnData());
        $this->view->assign('TaxKbnData',  \app\admin\model\envdata\EnvData::get_TaxKbnData());
        $this->view->assign('DispFlgData',  \app\admin\model\envdata\EnvData::get_DispFlgData());
        $this->view->assign('PeopleKbnData',  \app\admin\model\envdata\EnvData::get_PeopleKbnData());
        
        $PackBaseitemLst = MItem::get_base_items();
        $this->view->assign('PackBaseitemLst', $PackBaseitemLst);
        
        $PackBaseitemIni = null;
        $PackBaseitemAmountIni=1;
        $PackBaseitemSeq = null;
        $prv_seq=PHP_INT_MAX;
        
        foreach ($pack_row as $pack_item) {
            $seq = (int) $pack_item->Seq;
            if ($seq < $prv_seq) {
                $PackBaseitemIni = $pack_item->ItemNoChild;
                $PackBaseitemSeq= $pack_item->Seq;
                $PackBaseitemAmountIni=$pack_item->Amount;
            }
            $prv_seq = $seq;
        }
        $this->view->assign('PackBaseitemIni', $PackBaseitemIni);
        $this->view->assign('PackBaseitemAmountIni', $PackBaseitemAmountIni);
        
        $PackAdditemLst = MItem::get_add_items();
        $this->view->assign('PackAdditemLst', $PackAdditemLst);

        $PackAdditemIni = null;
        $PackAdditemAmountIni=1;
        
        $prv_seq_addItem = PHP_INT_MAX;
        $add_item_lst_other = array();
        $idx=1;
        foreach ($pack_row as $pack_item) {
            $seq = (int) $pack_item->Seq;
            if ($seq == $PackBaseitemSeq) {
                continue;
            }
            if ($seq < $prv_seq_addItem) {
                $PackAdditemIni = $pack_item->ItemNoChild;
                $PackAdditemAmountIni = $pack_item->Amount;
            } else {
                $add_item_lst_other[$idx]['ID'] = $pack_item->ItemNoChild;
                $add_item_lst_other[$idx]['Amount'] = $pack_item->Amount;
            }
            $prv_seq_addItem = $seq;
            $idx++;
        }
        $this->view->assign('PackAdditemIni', $PackAdditemIni);// 第一个 追加科目 id
        $this->view->assign('PackAdditemAmountIni', $PackAdditemAmountIni);// 第一个 追加科目 amount
        
        $this->view->assign('add_item_lst_other', $add_item_lst_other);
        $this->view->assign('add_item_count', count($add_item_lst_other)+1);
        
        return $this->view->fetch();//
    }
    
   

      public function del($ids = "") {
        if ($ids) {
            $pk_del = $ids;
            $pk = $this->model->getPk();
            $del_row = $this->model->where($pk, 'in', $pk_del)->select();
            if (count($del_row) == 1) {
                $this->model->where($pk, 'in', $pk_del)->delete();
                $model_pack = model('MPack');
                $pk_pack = $model_pack->getPk();
                $model_pack->where($pk_pack[0], 'in', $pk_del)->delete();
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
    
    public function fastedit($field_name) {
        if (!$this->request->isAjax()) {
            return;
        }
        $ItemCode = $this->request->post('ItemNo');
        if (!MItem::update_one_field($ItemCode, $field_name, $this->request->post($field_name),$this->auth->__get('id'))) {
            $this->error("$field_name 更新失敗。",null,['field_name' => $field_name,'no_msg'=> 1]);//本质也是 Response::create( $data ,'json', ,...
        }
        $this->success(" 更新成功!",null,['field_name' => $field_name,'no_msg'=> 0]);//本质也是 Response::create( $data ,'json',...
    }
    
}
