<?php

namespace app\admin\controller\mastersetting;

use app\common\controller\Backend;
use app\admin\model\MSalessection;
use app\admin\model\MSummarysection;
use app\admin\model\MItem;


class Itemmaster extends Backend {

    protected $model = null;

    public function _initialize() {
        parent::_initialize();
        $this->model = model('MItem'); //用helper的model函数装载AdminLog模块并存入成员
    }

    public function index() {
        
        if ($this->request->isAjax()) {//ajax 返回json
            list($where, $sort, $order, $offset, $limit, $where_arr) = $this->buildparams(); //根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了
            $total = $this->model
                    ->where($where)
                    ->where('PackFlg', '=', '0')
                    ->order($sort, $order)
                    ->count();

//            trace('limit---debug:' . $limit, 'error'); //20181031 add
//            $limit_getdata=$limit;
//            if ($limit == 'NaN') {
//                $limit_getdata = $total;
//            }


            $list = $this->model
                    ->alias('MI')
                    ->join('m_salessection MS' , 'MS.SalesSectionCode = MI.SalesSectionCode' )
                    ->join('m_summarysection MSS' , 'MSS.SummarySecCode = MI.SummarySecCode' )
                    ->where($where)
                    ->where('MI.PackFlg', '=', '0')
                    ->field("MI.SortNo ,"
                            //. "CASE WHEN MI.DispFlg = 0 THEN '非表示' WHEN MI.DispFlg = 1 THEN '表示' END as 表示フラグ ,"
                            . "MI.DispFlg ,"
                            . "MI.ItemNo ,"//as 科目ID
                            . "MI.ItemName  ,"//as 科目名
                            . "MI.BasePrice ,"//as 基本料金
                            . "MI.ServiceRate ,"//as サービス料率
                           // . "CASE WHEN MI.ServiceKbn = 0 THEN 'なし' WHEN MI.ServiceKbn = 1 THEN '別' WHEN MI.ServiceKbn = 2 THEN '込' END as サービス料区分,"//
//                            . "CASE WHEN MI.TaxKbn = 0 THEN 'なし' WHEN MI.TaxKbn = 1 THEN '外税' WHEN MI.TaxKbn = 2 THEN '内税' END as 消費税区分,"//
//                            . "CASE WHEN MI.SpaTaxKbn = 0 THEN 'なし' WHEN MI.SpaTaxKbn = 1 THEN '外税' WHEN MI.SpaTaxKbn = 2 THEN '内税' END as 入湯税区分,"//
//                            . " func_get_item_name(MI.ItemKindCode)  as 科目種別, "//
//                            . "CASE WHEN MI.ReceiptPrtKbn = 0 THEN '非印字' WHEN MI.ReceiptPrtKbn = 1 THEN '印字' END  as 印字区分,"//
//                            . "CASE WHEN MI.PackFlg = 0 THEN '無' WHEN MI.PackFlg = 1 THEN 'パック' END  as パック,"//
//                            . "CASE WHEN MI.PeopleKbn = 1 THEN '大人' WHEN MI.PeopleKbn = 2 THEN '子A' WHEN MI.PeopleKbn = 3 THEN '子B' WHEN MI.PeopleKbn = 4 THEN '子C' WHEN MI.PeopleKbn = 5 THEN '幼児' ELSE ' ' END as 入区分,"//
//                            . "CASE WHEN MI.FoodKbn = 1 THEN '朝食' WHEN MI.FoodKbn = 2 THEN '朝食その他' WHEN MI.FoodKbn = 3 THEN '夕食' WHEN MI.FoodKbn = 4 THEN '夕食その他' WHEN MI.FoodKbn = 5 THEN '昼食' WHEN MI.FoodKbn = 6 THEN '昼食その他' ELSE ' 　 ' END as 料理区分, "//
//                            . "CASE WHEN MI.ServiceContactReportDispKbn = 0 THEN '非表示' WHEN MI.ServiceContactReportDispKbn = 1 THEN '表示'  ELSE ' 　 ' END as 業務連絡表区分,"//
//                            . "CASE WHEN MI.StayTaxKbn = 0 THEN 'なし' WHEN MI.StayTaxKbn = 1 THEN '外税' WHEN MI.StayTaxKbn = 2 THEN '内税' END as 宿泊税区分,"//
//                            . "CASE WHEN MI.StayTaxAutoKbn = 0 THEN 'しない' WHEN MI.StayTaxAutoKbn = 1 THEN 'する' END as 宿泊税自動算出区分,"//
                             . " MI.ServiceKbn,"
                             . " MI.TaxKbn,"
                             . " MI.SpaTaxKbn,"
                             . " MI.ItemKindCode,"
                             . " MI.ReceiptPrtKbn,"
                             . " MI.PackFlg,"
                             . " MI.FoodKbn,"
                             . " MI.PeopleKbn,"
                             . " MI.ServiceContactReportDispKbn,"
                             . " MI.StayTaxKbn,"
                             . " MI.StayTaxAutoKbn,"
                            . "MI.StayTaxMstSummary as 宿泊税額, "//
                            . "MS.SalesSectionCode,"
                            . "MSS.SummarySecCode "
                            . "")
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();//返回结果，模型数组
            
            $result = array("total" => $total, "rows" => $list);
            $result_json = json($result); //json是助手函数,自动打包json输出到客户端, Response::create( $data ,'json', 200,...
            return $result_json;
        }
        
       // array ('a'=>1,'b'=>2,'c'=>3,'d'=>4,'e'=>5);⇛[{"a":1,"b":2,"c":3,"d":4,"e":5}]
       // MSalessection::get_id_name_list()  ⇛[{"1":"宿泊","2":"休憩","3":"四季亭","4":"四季蕎麦","5":"その他売上"}]
        
        $SalessectionData = json_encode(MSalessection::get_value_text_data_1());//返回JSON 编码 例 [{"value":"1","text":"宿泊"},{"value":"2","text":"休憩"},{"value":"3","text":"四季亭"},{"value":"4","text":"四季蕎麦"},{"value":"5","text":"その他売上"}]
        $SummarysectionData = json_encode(MSummarysection::get_value_text_data());
        $this->view->assign('SalessectionData', $SalessectionData);
        $this->view->assign('SummarysectionData', $SummarysectionData);
        return $this->view->fetch();
    }

    
    public function add() {
        if ($this->request->isAjax()&&$this->request->isPost()) {//新增 确定后， ajax
            $row = $this->request->post("row/a"); // /a是 数组的意思
            if ($row) {
                $pk_exist = $this->model->get(['ItemNo' => $row['ItemNo']]);
                if ($pk_exist) {
                    $this->error('科目ID重複');
                }
                $row['PackFlg']=0;
                $row['UpdateDate']=date("Y/m/d H:i:s");
                $row['UpdatePerson']= $this->auth->__get('id');
                $this->model->create($row);
                $this->success();
            }
            $this->error();
        }
       
        $this->view->assign('SalessectionData', MSalessection::get_id_name_list());
        $this->view->assign('SummarysectionData', MSummarysection::get_id_name_list() );
        $this->view->assign('ItemKindData', \app\admin\model\envdata\EnvData:: get_ItemKindData());
        $this->view->assign('ReceiptPrtKbnData', \app\admin\model\envdata\EnvData:: get_ReceiptPrtKbnData());
        $this->view->assign('ServiceKbnData',  \app\admin\model\envdata\EnvData::get_ServiceKbnData());
        $this->view->assign('TaxKbnData', \app\admin\model\envdata\EnvData:: get_TaxKbnData());
        $this->view->assign('DispFlgData',  \app\admin\model\envdata\EnvData::get_DispFlgData());
        $this->view->assign('PeopleKbnData', \app\admin\model\envdata\EnvData:: get_PeopleKbnData());
        
        return $this->view->fetch();
    }
    
      public function check_mul_pk() {
        if (!$this->request->isAjax()) {
            return;
        }
        $row = $this->request->post("row/a");
        $pk_exist = $this->model->get(['ItemNo' => $row['ItemNo']]);
        $result =$pk_exist?array("error" => "科目ID重複"): array("OK" => "OK");
        $result_json = json($result); //json是助手函数,自动打包json输出到客户端
        return $result_json;
    }
    
    // del 函数写都不用写，BackendTraits 的 del已经搞定
    
    
    public function edit($ids = NULL)
    {
        $row = $this->model->get(['ItemNo' => $ids]);
        if (!$row) {
            $this->error(__('結果が見つかりません '));
        }
        if ($this->request->isPost()) {//提交的时候
            $params = $this->request->post("row/a");
            if ($params) {
                $params['PackFlg']=0;
                $params['UpdateDate'] = date("Y/m/d H:i:s");
                $params['UpdatePerson'] = $this->auth->__get('id');
                $row->save($params);
                $this->success();
            }
            $this->error();
        }
        //↓不是提交的时候，抽出记录给view
        $this->view->assign("row", $row);
        
        $this->view->assign('SalessectionData', MSalessection::get_id_name_list());
        $this->view->assign('SummarysectionData', MSummarysection::get_id_name_list() );
        $this->view->assign('ItemKindData', \app\admin\model\envdata\EnvData::get_ItemKindData());
        $this->view->assign('ReceiptPrtKbnData',  \app\admin\model\envdata\EnvData:: get_ReceiptPrtKbnData());
        $this->view->assign('ServiceKbnData',  \app\admin\model\envdata\EnvData::get_ServiceKbnData());
        $this->view->assign('TaxKbnData', \app\admin\model\envdata\EnvData::get_TaxKbnData());
        $this->view->assign('DispFlgData',  \app\admin\model\envdata\EnvData::get_DispFlgData());
        $this->view->assign('PeopleKbnData', \app\admin\model\envdata\EnvData:: get_PeopleKbnData());
        
        return $this->view->fetch();
    }
    
    
    public function fastedit($field_name) {
        if (!$this->request->isAjax()) {
            return;
        }
        $ItemCode = $this->request->post('ItemNo');
        if (!MItem::update_one_field($ItemCode, $field_name, $this->request->post($field_name),$this->auth->__get('id'))) {
            $this->error("$field_name 更新失敗。",null,['field_name' => $field_name,'no_msg'=> 1]);//本质也是 Response::create( $data ,'json', ,...
        }
        $this->success(" 更新成功!",null,['field_name' => $field_name,'no_msg'=> 0]);//本质也是 Response::create( $data ,'json',...
    }

}
