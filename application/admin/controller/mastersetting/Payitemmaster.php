<?php
namespace app\admin\controller\mastersetting;

use app\common\controller\Backend;
use app\admin\model\MSalessection;
use app\admin\model\MSummarysection;
use app\admin\model\MPayitem;

class Payitemmaster extends Backend {

    protected $model = null;

    public function _initialize() {
        parent::_initialize();
        $this->model = model('MPayitem'); //用helper的model函数装载AdminLog模块并存入成员
    }

    public function index() {
        if ($this->request->isAjax()) {//ajax 返回json
            list($where, $sort, $order, $offset, $limit, $where_arr) = $this->buildparams(); //根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了
            $total = $this->model
                    //->fetchSql(true) 加这个可以查看原生查询sql
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

//            $list = $this->model
//                    ->alias('a')
//                    ->join('m_salessection b' , 'b.SalesSectionCode = a.SalesSectionCode' )
//                    ->join('m_summarysection c' , 'c.SummarySecCode = a.SummarySecCode' )
//                    ->where($where)
//                    ->field("a.*,b.SalesSectionName,c.SummarySecName,func_get_payitem_name(a.ItemKindCode) as 'ItemKindName',(CASE WHEN a.ReceiptPrtKbn = 0 THEN '非印字' WHEN a.ReceiptPrtKbn = 1 THEN '印字' END ) as ReceiptPrtKbnName ")
//                    ->order($sort, $order)
//                    ->limit($offset, $limit)
//                    ->select();
            
            $list = $this->model->where($where)->order($sort, $order)->limit($offset, $limit) ->select();
            $result = array("total" => $total, "rows" => $list);
            return json($result);//json是助手函数,自动打包json输出到客户端
        }
        $SalessectionData = json_encode(MSalessection::get_value_text_data_1());//返回JSON 编码 例 [{"value":"1","text":"宿泊"},{"value":"2","text":"休憩"},{"value":"3","text":"四季亭"},{"value":"4","text":"四季蕎麦"},{"value":"5","text":"その他売上"}]
        $SummarysectionData = json_encode(MSummarysection::get_value_text_data());
        $this->view->assign('SalessectionData', $SalessectionData);
        $this->view->assign('SummarysectionData', $SummarysectionData);
        return $this->view->fetch();
    }

    public function add() {
        if ($this->request->isAjax()&&$this->request->isPost()) {//新增 确定后， ajax
            $row = $this->request->post("row/a"); // /a是 数组的意思
            if ($row) {
                $pk_exist = $this->model->get(['ItemNo' => $row['ItemNo']]);
                if ($pk_exist) {
                    $this->error('入金科目ID重複');
                }
                $row['UpdateDate']=date("Y/m/d H:i:s");
                $row['UpdatePerson']= $this->auth->__get('id');
                $this->model->create($row);
                $this->success();
            }
            $this->error();
        }
        
        $this->view->assign('SalessectionData', MSalessection::get_id_name_list());
        $this->view->assign('SummarysectionData', MSummarysection::get_id_name_list() );
        $this->view->assign('ItemKindData', \app\admin\model\envdata\EnvData:: get_PayItemKindData());
        $this->view->assign('ReceiptPrtKbnData',  \app\admin\model\envdata\EnvData:: get_ReceiptPrtKbnData());
        return $this->view->fetch();
    }
    
      public function check_mul_pk() {
        if (!$this->request->isAjax()) {
            return;
        }
        $row = $this->request->post("row/a");
        $RoomTypeNo_exist = $this->model->get(['ItemNo' => $row['ItemNo']]);
        $result =$RoomTypeNo_exist?array("error" => "入金科目ID重複"): array("OK" => "OK");
        $result_json = json($result); //json是助手函数,自动打包json输出到客户端
        return $result_json;
    }    
    // del 函数写都不用写，BackendTraits 的 del已经搞定
    
    public function edit($ids = NULL)
    {
        $row = $this->model->get(['ItemNo' => $ids]);
        if (!$row)
        {
            $this->error(__('結果が見つかりません '));
        }
        if ($this->request->isPost())//提交的时候
        {
            $params = $this->request->post("row/a");
            if ($params)
            {            
                $params['UpdateDate']=date("Y/m/d H:i:s");
                $params['UpdatePerson']= $this->auth->__get('id');
                $row->save($params);
                $this->success();
            }
            $this->error();
        }
        //↓不是提交的时候，抽出记录给view
        $this->view->assign("row", $row);
        $this->view->assign('SalessectionData', MSalessection::get_id_name_list());
        $this->view->assign('SummarysectionData', MSummarysection::get_id_name_list() );
        $this->view->assign('ItemKindData',  \app\admin\model\envdata\EnvData::get_PayItemKindData());
        $this->view->assign('ReceiptPrtKbnData',  \app\admin\model\envdata\EnvData:: get_ReceiptPrtKbnData());
        
        return $this->view->fetch();
    }
    
    public function fastedit($field_name) {
        if (!$this->request->isAjax()) {
            return;
        }
        $ItemCode = $this->request->post('ItemNo');
        if (! MPayitem::update_one_field($ItemCode, $field_name, $this->request->post($field_name),$this->auth->__get('id'))) {
            $this->error("$field_name 更新失敗。",null,['field_name' => $field_name,'no_msg'=> 1]);//本质也是 Response::create( $data ,'json', ,...
        }
        $this->success(" 更新成功!",null,['field_name' => $field_name,'no_msg'=> 0]);//本质也是 Response::create( $data ,'json',...
    }

}
