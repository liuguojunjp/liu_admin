<?php

namespace app\admin\controller\mastersetting;

use app\common\controller\Backend;
use app\admin\model\STax;

class Taxmaster extends Backend {

    protected $model = null;

    public function _initialize() {
        parent::_initialize();
        $this->model = model('STax'); //用helper的model函数装载AdminLog模块并存入成员
    }

    public function index() {
        if ($this->request->isAjax()) {//ajax 返回json
            list($where, $sort, $order, $offset, $limit, $where_arr) = $this->buildparams(); //根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了
            $total = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                     ->alias('a')
                    ->where($where)
                    //->field("Date_Format(a.StartDate,'%Y/%m/%d') as StartDate,CONCAT(a.TaxRate,'%') as TaxRate ")
                    ->field("Date_Format(a.StartDate,'%Y/%m/%d') as StartDate,a.TaxRate ")
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            
            $result = array("total" => $total, "rows" => $list);
            $result_json = json($result); //json是助手函数,自动打包json输出到客户端
            return $result_json;
        }
        return $this->view->fetch();
    }

    
    public function add() {
        if ($this->request->isAjax()&&$this->request->isPost()) {//新增 确定后， ajax
            $row = $this->request->post("row/a"); // /a是 数组的意思
            $row['StartDate']=date("Y-m-d",strtotime($row['StartDate']));
            if ($row) {
                $pk_exist = $this->model->get(['StartDate' => $row['StartDate']]);
                if ($pk_exist) {
                    $this->error('開始日重複');
                }
                $row['UpdateDate']=date("Y/m/d H:i:s");
                $row['UpdatePerson']= $this->auth->__get('id');
                $this->model->create($row);
                $this->success();
            }
            $this->error();
        }
        return $this->view->fetch();
    }
    
      public function check_mul_pk() {
        if (!$this->request->isAjax()) {
            return;
        }
        $row = $this->request->post("row/a");
        $row['StartDate']=date("Y-m-d",strtotime($row['StartDate']));
        $pk_exist = $this->model->get(['StartDate' => $row['StartDate']]);
        $result =$pk_exist?array("error" => "開始日重複"): array("OK" => "OK");
        $result_json = json($result); //json是助手函数,自动打包json输出到客户端
        return $result_json;
    }
    
    // del 函数写都不用写，BackendTraits 的 del已经搞定
    public function edit($ids = NULL)
    {
        $row = $this->model->get(['StartDate' => $ids]);
        if (!$row) {
            $this->error(__('結果が見つかりません '));
        }
        if (IS_AJAX&&$this->request->isPost()) {//提交的时候
            $params = $this->request->post("row/a");
            if ($params) {
                $params['UpdateDate'] = date("Y/m/d H:i:s");
                $params['UpdatePerson'] = $this->auth->__get('id');
                $row->save($params);
                $this->success();
            }
            $this->error();
        }
        //↓不是提交的时候，抽出记录给view
        $row['StartDate']=date("Y/m/d",strtotime($row['StartDate']));
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
    //del 见BackendTraits
    // $ids=$this->request->post("ids");//这个包含在json数据里头的ids字段数据才准（上面哪个参数的）
    
    
      public function fastedit($field_name) {
        if (!$this->request->isAjax()) {
            return;
        }
        $ItemCode = $this->request->post('StartDate');
        if (!STax::update_one_field($ItemCode, $field_name, $this->request->post($field_name),$this->auth->__get('id'))) {
            $this->error("$field_name 更新失敗。",null,['field_name' => $field_name,'no_msg'=> 1]);//本质也是 Response::create( $data ,'json', ,...
        }
        $this->success(" 更新成功!",null,['field_name' => $field_name,'no_msg'=> 0]);//本质也是 Response::create( $data ,'json',...
    }
}
