<?php
namespace app\admin\controller\mastersetting;

use app\common\controller\Backend;
use app\admin\model\MRoomtype;
use app\admin\model\MRoom;

class Roommaster extends Backend
{

    protected $model = null;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('MRoom');//用helper的model函数装载AdminLog模块并存入成员
    }
    
    
    public function index()
    {
        if ($this->request->isAjax())//ajax 返回json
        {
            list($where, $sort, $order, $offset, $limit,$where_arr) = $this->buildparams();//根据提交过来的东西，生成查询所需要的条件,排序方式 ,这个人家写好了，不深究了
            $total = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

          $list =  $this->model
                    ->alias('a')
                    ->join('m_roomtype b' , 'b.RoomTypeNo = a.RoomTypeNo' )
                    ->where($where)
                    ->field('a.*,b.RoomTypeShortName')
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();// update dateなら、曖昧問題？
            

//           $sql_org= $this->model->where($where)->fetchSql(true)->find();
//           $where_sql=strrpos($sql_org,"(")>=0? preg_replace("#^.*?\((.*?)\).*?$#us", "$1", $sql_org):'';//提取第一个小括号()中的内容
//            
////$where_sql= substr($sql_org,strrpos($sql_org,"("),strrpos($sql_org,")")-strrpos($sql_org,"("));//提取第一个小括号()中的内容
//
//            $sql = 'select a.*,b.RoomTypeShortName from m_room as a 
//                     inner join m_roomtype as b on a.RoomTypeNo=b.RoomTypeNo where  '.$where_sql.' order by a.'.$sort.' '.$order.' limit '.$offset.','.$limit;
//            $list =$this->model ->query($sql);
            
            $result = array("total" => $total, "rows" => $list);
            $result_json=   json($result);//json是助手函数,自动打包json输出到客户端
            return $result_json;
        }
        return $this->view->fetch();
    }
    
    
     public function add()
    {
        if ($this->request->isAjax()&&$this->request->isPost()) {//新增 确定后， ajax
            $row = $this->request->post("row/a"); // /a是 数组的意思
            
            if ($row) {
                $RoomNo_exist = $this->model->get(['RoomNo' => $row['RoomNo']]);
                if ($RoomNo_exist) {
                    $this->error('部屋ID重複');
                }
                $row['UpdateDate']=date("Y/m/d H:i:s");
                $row['UpdatePerson']= $this->auth->__get('id');
                $this->model->create($row);
                $this->success();
            }
            $this->error();
        }
        $RoomTypeData = [];
        $roomtype_data = model('MRoomtype')->all();
        foreach ($roomtype_data as $roomtype_item) {
            $RoomTypeData[$roomtype_item->RoomTypeNo] = $roomtype_item->RoomTypeShortName;
        }
        $this->view->assign('RoomTypeData', $RoomTypeData);
        return $this->view->fetch();
    }

   public function check_mul_pk() {
        if (!$this->request->isAjax()) {
            return;
        }
        $row = $this->request->post("row/a");
        $RoomTypeNo_exist = $this->model->get(['RoomNo' => $row['RoomNo']]);
        $result =$RoomTypeNo_exist?array("error" => "部屋ID重複"): array("OK" => "OK");
        $result_json = json($result); //json是助手函数,自动打包json输出到客户端
        return $result_json;
    }

    
     public function edit($ids = NULL) {
        $row = $this->model->get(['RoomNo' => $ids]);
        if (!$row) {
            $this->error(__('結果が見つかりません '));
        }
        if ($this->request->isPost()) {//提交的时候
            $params = $this->request->post("row/a");
            if ($params) {
                $params['UpdateDate'] = date("Y/m/d H:i:s");
                $params['UpdatePerson'] = $this->auth->__get('id');
                $row->save($params);
                $this->success();
            }
            $this->error();
        }
        //↓不是提交的时候，抽出记录给view
        $this->view->assign("row", $row);
        $RoomTypeData = [];
        $roomtype_data = model('MRoomtype')->all();
        foreach ($roomtype_data as $roomtype_item) {
            $RoomTypeData[$roomtype_item->RoomTypeNo] = $roomtype_item->RoomTypeShortName;
        }
        $this->view->assign('RoomTypeData', $RoomTypeData);
        return $this->view->fetch();
    }
    
      public function fastedit($field_name) {
        if (!$this->request->isAjax()) {
            return;
        }
        $ItemCode = $this->request->post('RoomNo');
        if (!MRoom::update_one_field($ItemCode, $field_name, $this->request->post($field_name),$this->auth->__get('id'))) {
            $this->error("$field_name 更新失敗。",null,['field_name' => $field_name,'no_msg'=> 1]);//本质也是 Response::create( $data ,'json', ,...
        }
        $this->success(" 更新成功!",null,['field_name' => $field_name,'no_msg'=> 0]);//本质也是 Response::create( $data ,'json',...
    }

}
