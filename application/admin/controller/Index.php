<?php
namespace app\admin\controller;

use app\common\controller\Backend;
use think\Validate;
use think\Session;


class Index extends Backend//后台首页
{
    protected $noNeedLogin = ['login','conn_clear'];// 无需登录的方法,同时也就不需要鉴权了
    protected $noNeedRight = ['index', 'logout'];//无需鉴权的方法,但需要登录
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();//Backend的_initialize
    }

    public function login() {//管理员登录
        $index_url = $this->request->get('url', 'index/index'); //如果有url的get参数，就转到那个url，否则转到， 后台首页的url（'index/index'）
        if ($this->auth->isLogin()) {//session已经有[admin]值
            $this->success(__("既にログイン済みです。"), $index_url);//不用显示登录页面直接转入后台 //$this->redirect($url);//不提示直接转到后台首页
        }
        if ($this->request->isPost()) {//用户在登录页面上按下登录按钮,但是注意是用的ajax！
            $username = $this->request->post('username');
            $password = $this->request->post('password');
            //$keeplogin = $this->request->post('keeplogin');//20180622 从登录画面取消，null,不搞cookie
            $token = $this->request->post('__token__');
            $rule = [
                'username' => 'require|length:3,30',
                'password' => 'require|length:3,30',
                '__token__' => 'token',
            ];
            $data = [
                'username' => $username,
                'password' => $password,
                '__token__' => $token,
            ];
            $validate = new Validate($rule);
            $result = $validate->check($data);
            if (!$result) {
                $this->error($validate->getError(), '', ['token' => $this->request->token()]);
            }
            \app\admin\model\AdminLog::setTitle(__('Login'));
            //$result = $this->auth->login($username, $password, $token,$keeplogin ? 86400 : 0);           
            if ($this->auth->login($username, $password, $token, 0) === true) {
                $this->success(//ajax 返回
                        __('ログイン成功!'),//提示信息
                        $index_url, //要转向的地址，（给JS由JS的回调函数放入地址栏来转）
                        ['url' => $index_url, 'id' => $this->auth->id, 'username' => $username, 'avatar' => $this->auth->avatar]//给客户端的数据，用数组，
                        ); //由于是ajax，并不是在这里转到index界面的，而是客户端的location.href = Backend.api.fixurl(data.url);
                // $this->redirect($index_url);//直接跳转，
            } else {
                $this->error(__('担当者またはパスワードが違います'), '', ['token' => $this->request->token()]); //由于这里是ajax，只是返回给客户端显示错误信息
            }
        }
//        if ($this->auth->autologin())// 根据客户端的cookie,判断是否可以自动登录
//        {
//            $this->redirect($url);
//        }
//       \think\Hook::listen("login_init", $this->request); //liu del 20180104 应该没用
        //show view ↓
        $hotel_name = "";
        if ($this->user_id > 0) {
            $hotel_name .= 'ホテルユーザ' . $this->user_id; //とりあえず
        }
        return view('', ['user_id' => $this->user_id, 'hotel_name' => $hotel_name]);
    }

    public function conn_clear() {// [＜＜接続クリアして戻る] 的服务器动作
        if ($this->request->isPost()) {
            $user_id = $this->request->post('user_id');
            $conn_tocken = Session::get('conn_tocken');
            $reflect = new \ReflectionClass('app\admin\model\user_conn\User' . $user_id);
            $reflect->newInstance()->clear_row_by_conn_tocken($conn_tocken);
//            $conn_obj = $reflect->newInstance()->get_obj_by_conn_tocken($conn_tocken); //另外一种写法
//            if ($conn_obj) {
//                $conn_obj->clear_row_by_conn_tocken_obj_inner();
//            }
            //Session::delete("user_id");
            Session::delete("conn_tocken");
            $this->redirect('/user/' . $user_id);
        }
    }
    
    public function index()//后台首页
    {       
        $menulist = $this->auth->getSidebar([], $this->view->site['fixedpage']);//
        //加标识 fixedpage is dashboard  //$menulist = $this->auth->getSidebar(['othermaintenance' => ['new', 'red', 'badge'],], $this->view->site['fixedpage']);
        return view('', ['menulist' => $menulist, 'title' => __('Home')]);//这个title好像并不重要
        
    }

    public function logout()//注销登录
    {
        $this->auth->logout();
        $this->success(__('ログアウト完了'), 'index/login');
    }
    
  
}
