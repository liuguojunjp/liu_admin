<?php

namespace app\admin\model\traits;

trait MasterModelTraits {
    
//    protected static $instance;
//    public static function getInstance() {  //在traits里头是可以用self的，虽然这里 注释掉了（用MyModelBase里头的代码）
//        if (!(self::$instance instanceof self)) {
//            self::$instance = new static();
//        }
//        return self::$instance;
//    }
    
    public static function update_one_field($key_val, $field, $field_value, $person_id) {
        try {
            self::where(self::getInstance()->getPk(), (int) $key_val)->setField([$field => $field_value, 'UpdateDate' => date("Y/m/d H:i:s"), 'UpdatePerson' => $person_id]);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public static function update_one_field_TwoPk($key_val1,$key_val2, $field, $field_value, $person_id) {
        try {
            $pkarr=self::getInstance()->getPk();
            self::where($pkarr[0],  $key_val1)->where($pkarr[1],  $key_val2)->setField([$field => $field_value, 'UpdateDate' => date("Y/m/d H:i:s"), 'UpdatePerson' => $person_id]);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    
    
}
