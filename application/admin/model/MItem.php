<?php

namespace app\admin\model;

class MItem extends MyModelBase {

    public static function get_base_items() {
        $result = [];
        $db_Data = self::
                where('ItemKindCode', 'in', [10, 20])
                ->where('PackFlg', '=', 0)
                ->order('ItemNo')
                ->select();
        foreach ($db_Data as $item) {
            $result[$item->ItemNo] = $item->ItemNo . '-----' . $item->ItemName;
        }
        return $result;
    }

    public static function get_add_items() {
        $result = [];
        $db_Data = self::where('ItemKindCode', 'not in', [10, 20])
                ->where('PackFlg', '=', 0)
                ->order('ItemNo')
                ->select();
        foreach ($db_Data as $item) {
            $result[$item->ItemNo] = $item->ItemNo . '-----' . $item->ItemName;
        }
        return $result;
    }

    use traits\MasterModelTraits;
}
