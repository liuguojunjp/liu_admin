<?php

namespace app\admin\model;

class MAgent extends MyModelBase
{
    use traits\MasterModelTraits; 
    
     public static function get_agent_select_data() {
        $AgentSelectData = [];
        $agent_data = self::getInstance()->all();
        foreach ($agent_data as $agent_item) {
            $AgentSelectData[$agent_item->AgentCode] = $agent_item->AgentName;
        }
        return $AgentSelectData;
    }
    
     public static function get_value_text_data() {
        $value_text_data = self::getInstance()->field("concat(AgentCode,'') as value,AgentName as text ")->select();
        return $value_text_data;
    }
    
    
}
