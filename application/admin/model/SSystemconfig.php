<?php

namespace app\admin\model;

use think\DB;

class SSystemconfig extends MyModelBase
{
    public static function get_cur_hoteldate() {
        $sql = "select date_format(HotelDate, '%Y/%m/%d') as HotelDate from s_systemconfig  limit 1";
        $DB_RES = Db::query($sql);

        $cur_hoteldate = '';
        if (count($DB_RES) > 0) {
            $cur_hoteldate = $DB_RES[0]['HotelDate'];
        }
        return $cur_hoteldate;
    }

    public static function get_db_hoteldate_password() {
        $sql = 'select HotelDateUpdatePassword from s_systemconfig  limit 1';
        $DB_RES = Db::query($sql);
        $result = '';
        if (count($DB_RES) > 0) {
            $result = $DB_RES[0]['HotelDateUpdatePassword'];
        }
        return $result;
    }

    public static function update_hoteldate($DateStr10From, $ToDateStr10) {
        try {
            $sql = "update  s_systemconfig SET  HotelDate = date_format('" . $ToDateStr10 . "', '%Y/%m/%d') ";
            $sql .= " WHERE  date_format(HotelDate, '%Y/%m/%d')='" . $DateStr10From . "' ";
            $sfd = Db::execute($sql);
        } catch (\Exception $e) {
            Log::error('★★★★★★ホテルデート更新失敗しました★★★★★★' . $e->getMessage());
            return false;
        }
        return true;
    }

    public static function update_hoteldate_password($newPassword, $oldPassword) {
        try {
            $sql = "update  s_systemconfig SET  HotelDateUpdatePassword = '" . $newPassword . "' ";
            $sql .= " WHERE  HotelDateUpdatePassword='" . $oldPassword . "' ";
            $sfd = Db::execute($sql);
        } catch (\Exception $e) {
            Log::error('★★★★★★ホテルデート更新パスワード更新失敗しました★★★★★★' . $e->getMessage());
            return false;
        }
        return true;
    }

}
