<?php

namespace app\admin\model;

class MFreetitle extends MyModelBase {

    public static function getFreeitemSelectData() {
        $FreeitemSelectData = [];
        for ($i = 1; $i <= 4; $i++) {
            $FreeSummaryName = 'ResvForm_FreeSummary' . $i . 'Name';
            $FreeitemSelectData[$i] = envdata\EnvData::$$FreeSummaryName;
        }
        return $FreeitemSelectData;
    }

}
