<?php

namespace app\admin\model;


use think\Db;
use think\Log;

class SOptiondata extends MyModelBase {
    public static function update_option_lst($curLoginUseID, $ctlmst_basic) {
        Db::startTrans();
        try {
            foreach ($ctlmst_basic as $key => $value) {
                $group_key = explode("_", $key);
                $group = $group_key[0];
                $sql = "INSERT INTO s_optiondata(`Key`,`Value`,`GroupName`,UpdateDate,UpdatePerson)";
                $sql .= "  VALUES('$key','$value','$group',now()," . $curLoginUseID . ")";
                $sql .= "  ON DUPLICATE KEY UPDATE `Value` = '$value',`GroupName` = '$group' ,`UpdateDate` = now(),`UpdatePerson` =  $curLoginUseID ;";
                $sfd = Db::execute($sql);
            }
            Db::commit();    // 提交事务  
        } catch (\Exception $e) {
            Db::rollback(); // 回滚事务
            Log::error('★★★★★★コントロールマスタ設定失敗しました★★★★★★' . $e->getMessage());
            return false;
        }
        return true;
    }

    public static function get_db_all_optionlst() {
        $sql = 'select * from s_optiondata  ';
        $DB_RES = Db::query($sql);
        $option_list = array();
        foreach ($DB_RES as $idx => $row) {
            $option_list[$row['Key']] = $row['Value'];
        }
        return $option_list;
    }

    public static function get_checkbox_group_content_isReceiptSumSub() {

        return array(
            'ReceiptOption_isReceiptSumSubCash' => array('checkbox_title' => '現金', 'checkbox_value' => 1),
            'ReceiptOption_isReceiptSumSubDeposit' => array('checkbox_title' => '前受金', 'checkbox_value' => 1),
            'ReceiptOption_isReceiptSumSubCupon' => array('checkbox_title' => 'クーポン', 'checkbox_value' => 1),
            'ReceiptOption_isReceiptSumSubCredit' => array('checkbox_title' => 'クレジット', 'checkbox_value' => 1),
            'ReceiptOption_isReceiptSumSubAccountsReceivable' => array('checkbox_title' => '売掛金', 'checkbox_value' => 1),
            'ReceiptOption_isReceiptSumSubagentAccountsReceivable' => array('checkbox_title' => '業者売掛金', 'checkbox_value' => 1)
        );
    }

    public static function get_checkbox_group_content_isRequestSumSub() {

        return array(
            'ReceiptOption_isRequestSumSubCash' => array('checkbox_title' => '現金', 'checkbox_value' => 1),
            'ReceiptOption_isRequestSumSubDeposit' => array('checkbox_title' => '前受金', 'checkbox_value' => 1),
            'ReceiptOption_isRequestSumSubCupon' => array('checkbox_title' => 'クーポン', 'checkbox_value' => 1),
            'ReceiptOption_isRequestSumSubCredit' => array('checkbox_title' => 'クレジット', 'checkbox_value' => 1),
            'ReceiptOption_isRequestSumSubAccountsReceivable' => array('checkbox_title' => '売掛金', 'checkbox_value' => 1),
            'ReceiptOption_isRequestSumSubagentAccountsReceivable' => array('checkbox_title' => '業者売掛金', 'checkbox_value' => 1)
        );
    }

}
