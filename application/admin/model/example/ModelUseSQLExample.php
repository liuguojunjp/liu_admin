<?php
namespace app\admin\model\example;
use think\Model;
//use think\DB;


class ModelUseSQLExample extends Model //这是一个用model不用DB，用原生sql的例子，但是能不这么用就不这么用。
{
    protected $autoWriteTimestamp = 'int';// 开启自动写入时间戳字段
    protected $createTime = 'createtime';// 定义时间戳字段名
    protected $updateTime = 'updatetime';
    
    //    protected $autoWriteTimestamp = false;
//    
//    protected $createTime = '';// 定义时间戳字段名 ，（$createTime固定要这么写）
//    protected $updateTime = '';// 没有updateTime 字段
    
    private static $instance;
   
    public static function getInstance()
    {
        if(!(self::$instance instanceof self)){
            self::$instance = new static();
        }
        return self::$instance;
    }

    public static function insertOne($uid,$uname,$uage,$usex)
    {
        $inserttheone = self::getInstance()->execute("insert into users(u_id,u_name,u_age,u_sex) value(".$uid.",'".$uname."',".$uage.",".$usex.")");
        if($inserttheone)
        {
            return true;
        }
        else{
            return false;
        }
    }
    /**
    * 删除一条数据
    */
    public static function deleteOne($uid)
    {
        $delone = self::getInstance()->execute("delete from users where u_id = ".$uid."");
        if($delone)
        {
            return true;
        }
        else{
            return false;
        }
    }
    /**
    *  修改一条数据
    */
    public static function updateOne($uid,$age)
    {
        $updateone = self::getInstance()->execute("update users set u_age = ".$age." where u_uid = ".$uid."");
        if($updateone)
        {
            return true;
        }
        else{
            return false;
        }
    }
    /**
    * 查询
    */
    public static function query($defaultField,$uid)
    {   
        if($defaultField == '' || empty($defaultField) || is_null($defaultField)){
            $defaultField = '*';   
        }
        if($uid == '' || empty($uid) || is_null($uid)){
            $uid = '';   
        }
        else{
            $uid = "where u_id = $uid";
        }
        return self::getInstance()->query("select $defaultField from users $uid");
 
    }
    /**
    * 批量修改
    */
    public static function batchUpdate($arr)
    {   
        foreach ($arr as $key => $value) {
            $updatearr = self::getInstance()->execute("update users set u_name = '".$value['u_name']."',u_age = ".$value['u_age'].",u_sex = ".$value['u_sex']." where u_uid = ".$uid."");
            if($updatearr)
            {
                return true;
            }
            else{
                return false;
            }
        }

}


}