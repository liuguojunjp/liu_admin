<?php

namespace app\admin\model;

use think\Db;

class MSalessection extends MyModelBase
{
     public static function get_id_name_list() {
        $SalessectionData = [];
        $db_SalessectionData = self::getInstance()->all();
        foreach ($db_SalessectionData as $Salessection_item) {
            $SalessectionData[$Salessection_item->SalesSectionCode] = $Salessection_item->SalesSectionName;
        }
        return $SalessectionData;
    }
    
    public static function get_data() {        
        return self::getInstance()->all();
    }
    
    public static function get_value_text_data() {
        return self::getInstance()->field("concat(SalesSectionCode,'') as value,SalesSectionName as text ")->select();
    }

    public static function get_value_text_data_1() {//20181218 这里只是一个原生sql的例子，这个函数和上面get_value_text_data 的结果是一样的。可见上面比较方便
        $sql = " select * from m_salessection ;";
        $DB_RES = Db::query($sql);
        $result=array();
        foreach ($DB_RES as $key => $value) {
            $db_SalessectionData = new \stdClass;//注意命名空间,或者用 $db_SalessectionData = new MSalessection();也是可以的但debug并不方便
            $db_SalessectionData->value=(string)$value['SalesSectionCode'];
            $db_SalessectionData->text=(string)$value['SalesSectionName'];
            $result[]=$db_SalessectionData;
        }
        return $result;
    }
    
    public static function get_value_text_data_2() {//20181218 这里只是一个用模型抽数据的例子，这个函数和上面get_value_text_data 的结果是一样的。可见上面比较方便
       
        $all_data = self::getInstance()->all();
        $result=array();
        foreach ($all_data as $key => $SalesSectionObj) {
            $db_SalessectionData = new \stdClass;//注意命名空间,或者用 $db_SalessectionData = new MSalessection();也是可以的但debug并不方便
            $db_SalessectionData->value=(string)$SalesSectionObj->SalesSectionCode;
            $db_SalessectionData->text=$SalesSectionObj->SalesSectionName;
            $result[]=$db_SalessectionData;
        }
        return $result;
    }

     use traits\MasterModelTraits;   
    
}
