<?php

namespace app\admin\model;

use think\Db;
class DClientinfo extends MyModelBase
{
    const ClientNoPix = "D";
    const NumKbn=2;
    
    use traits\MasterModelTraits; 
    
    public static function getNewClientNo()
    {
        $newClientNo="";
        try {
            $sql = " select Number from d_number where NumKbn=" . self::NumKbn . " limit 1 ;";
            $DB_RES = Db::query($sql);
            $cur_Number = (int) $DB_RES[0]['Number'];
            if (!isset($cur_Number)) {
                $sql = " INSERT INTO `d_number` (`NumKbn`, `NumDate`, `Number`) VALUES (" . self::NumKbn . ", '" . self::ClientNoPix . "', 1) ;";
                $new_res = Db::execute($sql);
                $cur_Number = 1;
            }
            $sqlArray = array();
            $sqlArray[] = " UPDATE `semaphore` SET `Seq` = `Seq`+1,`OtherInfo` = '" . date("Y-m-d H:i:s") . "' WHERE `ID` =2;";
            $sqlArray[] = "update d_number set Number=Number where NumDate= '".self::ClientNoPix."' and NumKbn=" . self::NumKbn . ";";
            $sqlArray[] = "update d_number set Number=Number+1 where NumDate= '".self::ClientNoPix."' and NumKbn=" . self::NumKbn . ";";
            Db::startTrans();
            foreach ($sqlArray as  $sql) {
                $sfd = Db::execute($sql);
            }
            Db::commit();    // 提交事务  
        } catch (\Exception $e) {
            Db::rollback(); // 回滚事务
            Log::error('★★★★★★顧客番号採番失敗しました★★★★★★' . $e->getMessage());
            return "";
        }
        $newClientNo = self::ClientNoPix . str_pad($cur_Number, (13 - strlen(self::ClientNoPix)), "0", STR_PAD_LEFT);
        return $newClientNo;
    }
     
}
