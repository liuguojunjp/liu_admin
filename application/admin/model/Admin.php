<?php

namespace app\admin\model;

class Admin extends MyModelBase
{
    protected $autoWriteTimestamp = 'int';// 开启自动写入时间戳字段
    protected $createTime = 'createtime';// 定义时间戳字段名
    protected $updateTime = 'updatetime';
  
    public function resetPassword($uid, $NewPassword)//重置用户密码
    {
        $passwd = $this->encryptPassword($NewPassword);
        $ret = $this->where(['id' => $uid])->update(['password' => $passwd]);
        return $ret;
    }
    
    protected function encryptPassword($password, $salt = '', $encrypt = 'md5') // 密码加密
    {
        return $encrypt($password . $salt);
    }

}
