<?php

namespace app\admin\model\db_class;

use think\Db;

class DB_Select 
{
    private static $instance;
    public static function getInstance()
    {
        if(!(self::$instance instanceof self)){
            self::$instance = new static();
        }
        return self::$instance;
    }

      public static function get_db_roomtype() {
        $sql = 'select * from m_roomtype  order by RoomTypeNo desc limit 3';
        $DB_RES = Db::query($sql);
        return $DB_RES;
    }
  
    
}
