<?php

namespace app\admin\model\user_conn;
use think\Model;

class UserConnBase extends Model
{
    protected $connection = 'db_config_user_mana';//注意，db_config_user_mana可不是数据库名而是一个设定   
    
//    private static $instance=null;
//    public static function getInstance()
//    {
//        if(!(self::$instance instanceof self)){
//            self::$instance = new static();
//        }
//        return self::$instance;
//    }
    
    public function get_mana_obj() {// $data = Db::connect('db_config_user_mana')->query("select * from $user_table  where RowFlg='pass' limit 1 ");//连接管理数据库，检查pass
        return $this->where('RowFlg', '=', 'pass')->find();//取出管理行(obj)
        
    }
    
     public function get_last_conn() {// //按照时间获取一个连接（ID），(冲掉最旧的时间) $data_id = Db::connect('db_config_user_mana')->query("select ID from $user_table  where RowFlg='conn' order by StartTime Asc limit 1 ");
        return $this->where('RowFlg', '=', 'conn')->order('StartTime', 'asc')->find();
    }
    
    
    public function get_obj_by_conn_tocken($conn_tocken) {
       return $this->where('RowFlg', '=', 'conn')
                ->where('ConnectToken', '=', $conn_tocken)
                ->order('StartTime', 'asc')
                ->find();
    }
    
    
    //$sql = " update $user_table set StartTime='1970-01-01' ,ConnectToken='', IP='', UserAgent='' where RowFlg='conn' And ConnectToken='$conn_tocken'  limit 1 ";
    public function clear_row_by_conn_tocken($conn_tocken) {
        $obj = $this->get_obj_by_conn_tocken($conn_tocken);
        if ($obj) {
            $obj->StartTime = '1970-01-01';
            $obj->ConnectToken = '';
            $obj->UserAgent = '';
            $obj->IP = '';
            $obj->save();
        }
    }
    
    public function clear_row_by_conn_tocken_obj_inner() {
        $this->setAttr('StartTime', '1970-01-01');//注意不可 $this->StartTime = '1970-01-01'; 出错
        $this->setAttr('ConnectToken', '');//
        $this->setAttr('UserAgent', '');
        $this->setAttr('IP', '');
        $this->save();
    }
    
    public function save_conn_detail($token='',$UserAgent='',$IP='') {
        $this->setAttr('StartTime', date("Y/m/d H:i:s"));//注意不可 $this->StartTime = '1970-01-01'; 出错
        $this->setAttr('ConnectToken', $token);//
        $this->setAttr('UserAgent', $UserAgent);
        $this->setAttr('IP', $IP);
        $this->save();
    }

}
