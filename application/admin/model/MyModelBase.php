<?php

namespace app\admin\model;

use think\Model;

class MyModelBase extends Model
{
    protected static $instance;
    public static function getInstance() {//在这里可不能用self！！！，要用self那放到 traits中去
        if (!(static::$instance instanceof static)) {
            static::$instance = new static();
        }
        return static::$instance;
    }
}
