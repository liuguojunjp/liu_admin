<?php

namespace app\admin\model;


class MSummarysection extends MyModelBase
{
     use traits\MasterModelTraits;   
     
     public static function get_id_name_list() {
        $SummarysectionData = [];
        $db_SummarysectionData = self::getInstance()->all();
        foreach ($db_SummarysectionData as $Summarysection_item) {
            $SummarysectionData[$Summarysection_item->SummarySecCode] = $Summarysection_item->SummarySecName;
        }
        return $SummarysectionData;
    }
    
    public static function get_value_text_data() {
        $db_SalessectionData = self::getInstance()->field("concat(SummarySecCode,'') as value,SummarySecName as text ")->select();
        return $db_SalessectionData;
    }   
}
