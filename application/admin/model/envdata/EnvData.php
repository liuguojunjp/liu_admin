<?php

namespace app\admin\model\envdata;

use app\admin\model\SOptiondata;

class EnvData 
{
    static public $ResvForm_FreeSummary1Name = '';
    static public $ResvForm_FreeSummary2Name = '';
    static public $ResvForm_FreeSummary3Name = '';
    static public $ResvForm_FreeSummary4Name = '';
  
    public static function ini()
    {
      $all_optionlst= SOptiondata::get_db_all_optionlst();
      for ($i = 1; $i <= 4; $i++) {
            $FreeSummaryName = 'ResvForm_FreeSummary' . $i . 'Name';
            if (array_key_exists($FreeSummaryName, $all_optionlst)) {
                self::$$FreeSummaryName = $all_optionlst[$FreeSummaryName];
            }
        }
        
        
        
    }
    
    

   public static  function get_PayItemKindData() {
    $ItemKindData = [];
        $ItemKindData[90] = '現金';
        $ItemKindData[92] = '前受金';
        $ItemKindData[93] = 'クーポン';
        $ItemKindData[94] = 'クレジット';
        $ItemKindData[95] = '売掛金';
        $ItemKindData[96] = '業者売掛金';
    return $ItemKindData;
}

public static function get_ItemKindData() {
    $ItemKindData = [];
    $ItemKindData[10] = '宿泊基本';
    $ItemKindData[20] = '休憩基本';
    $ItemKindData[50] = '追加料理';
    $ItemKindData[51] = '追加飲料';
    $ItemKindData[59] = '追加その他';
    $ItemKindData[60] = '立替料理';
    $ItemKindData[61] = '立替飲料';
    $ItemKindData[69] = '立替その他';
    return $ItemKindData;
}

public static function get_ReceiptPrtKbnData() {
    $ReceiptPrtKbnData = [];
    $ReceiptPrtKbnData[0] = '非印字';
    $ReceiptPrtKbnData[1] = '印字';
    return $ReceiptPrtKbnData;
}

public static function get_ServiceKbnData() {
    $ServiceKbnData = [];
    $ServiceKbnData[0] = 'なし';
    $ServiceKbnData[1] = '別';
    $ServiceKbnData[2] = '込';
    return $ServiceKbnData;
}

public static function get_TaxKbnData() {
    $TaxKbnData = [];
    $TaxKbnData[0] = 'なし';
    $TaxKbnData[1] = '外税';
    $TaxKbnData[2] = '内税';
    return $TaxKbnData;
}

public static function get_DispFlgData() {
    $DispFlgData = [];
        $DispFlgData[0] = '非表示';
        $DispFlgData[1] = '表示';
    return $DispFlgData;
}


public static function get_PeopleKbnData() {
 $PeopleKbnData = [];
        $PeopleKbnData[1] = '大人';
        $PeopleKbnData[2] = '子A';
        $PeopleKbnData[3] = '子B';
        $PeopleKbnData[4] = '子C';
        $PeopleKbnData[5] = '幼児';
        $PeopleKbnData[0] = 'その他';
    return $PeopleKbnData;
}

    
   

}

