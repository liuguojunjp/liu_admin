<?php
namespace app\admin\behavior;
class AdminLog
{
    public function run(&$params)//被执行的时机，app end ，具体看 admin 下的tags.php ,      在那里被定义的
    {
        if (request()->isPost())
        {
            \app\admin\model\AdminLog::record();//写入操作员访问日志表（）
        }
    }

}
