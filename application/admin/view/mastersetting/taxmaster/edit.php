<form id="tax_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="StartDate" class="control-label col-xs-12 col-sm-2">{:__('開始日')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" style="width: 90%;" class="form-control" id="StartDate" name="row[StartDate]" value="{$row.StartDate}" disabled />
        </div>
    </div>
    <div class="form-group">
        <label for="TaxRate" class="control-label col-xs-12 col-sm-2">{:__('消費税率')}:</label> 
        <div class="col-xs-12 col-sm-8">
            <input type="text" style="width: 90%;display: inline" class="form-control" id="TaxRate" name="row[TaxRate]" value="{$row.TaxRate}" data-rule="required;float"/>%
        </div>
    </div>
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>