<form id="tax_mst_add-form" role="form" class="form-horizontal form-ajax"  data-toggle="validator" method="POST" action=""><!-- action="" 自提交 原来的url，这里是 'mastersetting/roommaster/add' role="form" 不能少，不然对应的js找不到这个form，无法绑定各种方法-->
    <div class="form-group">
        <label for="StartDate" class="control-label col-xs-12 col-sm-2"><i class="fa fa-calendar"></i>{:__('開始日')}:</label> <!-- for="xxxx"  一定要有，用来显示客户端验证的结果 -->
        <div class="col-xs-12 col-sm-8">
            <fieldset>
                <input type="datetime" style="width: 90%" class="form-control datetimepicker" id="StartDate" name="row[StartDate]" value="" 
                       data-rule="required;date;remote[mastersetting/taxmaster/check_mul_pk]" autocomplete="off"/>  <!-- 最后的[username]   是对应validator里头的一个rule -->
            </fieldset>
        </div>
    </div>
    <div class="form-group">
        <label for="TaxRate" class="control-label col-xs-12 col-sm-2">{:__('消費税率')}:</label> 
        <div class="col-xs-12 col-sm-8">
           <input type="text" class="form-control" style="width: 90%;display: inline" id="TaxRate" name="row[TaxRate]" value="" data-rule="required;float"/>%
        </div>
    </div>
    <div class="form-group hidden layer-footer"><!-- layer-footer  配合着fast.api.open  -->
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>