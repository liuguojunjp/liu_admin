<form id="item_mst_add-form" role="form" class="form-horizontal form-ajax"  data-toggle="validator" method="POST" action=""><!-- action="" 自提交 原来的url，这里是 'mastersetting/roommaster/add' role="form" 不能少，不然对应的js找不到这个form，无法绑定各种方法-->
    <div class="form-group">
        <label for="ItemNo" class="control-label col-xs-12 col-sm-2">{:__('科目ID')}:</label> <!-- for="RoomTypeNo"  一定要有，用来显示客户端验证的结果 -->
        <div class="col-xs-12 col-sm-8">
            <fieldset>
             <input type="text" class="form-control" id="ItemNo" name="row[ItemNo]" value="" data-rule="required;digits;remote[mastersetting/itemmaster/check_mul_pk]" autocomplete="off"/>  <!-- 最后的[username]   是对应validator里头的一个rule -->
            </fieldset>
        </div>
    </div>
    
      <div class="form-group">
        <label for="SortNo" class="control-label col-xs-12 col-sm-2">{:__('表示順番')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="SortNo" name="row[SortNo]" value="" data-rule="required;digits;"/> 
        </div>
    </div>
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('表示フラグ')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[DispFlg]', $DispFlgData, 1, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
    
    
    <div class="form-group">
        <label for="ItemName" class="control-label col-xs-12 col-sm-2">{:__('科目名')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="nickname" name="row[ItemName]" value="" data-rule="required" />
        </div>
    </div>
    
    <div class="form-group">
        <label for="BasePrice" class="control-label col-xs-12 col-sm-2">{:__('基本料金')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="BasePrice" name="row[BasePrice]" value="" data-rule="required;digits;"/> 
        </div>
    </div>
    
     <div class="form-group">
        <label for="ServiceRate" class="control-label col-xs-12 col-sm-2">{:__('サービス料率')}:</label>
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="ServiceRate" name="row[ServiceRate]" value="" data-rule="required;float;"/> 
        </div>
    </div>
    
    
    <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('サービス料区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[ServiceKbn]', $ServiceKbnData, null, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
    <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('消費税区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[TaxKbn]', $TaxKbnData, null, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('入湯税区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[SpaTaxKbn]', $TaxKbnData, null, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('売上部門')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[SalesSectionCode]', $SalessectionData, null, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('集計部門')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[SummarySecCode]', $SummarysectionData, null, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('科目種別')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[ItemKindCode]', $ItemKindData, null, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
    <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('入区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[PeopleKbn]', $PeopleKbnData, 1, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('印字区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[ReceiptPrtKbn]', $ReceiptPrtKbnData, 1, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
    <div class="form-group hidden layer-footer"><!-- layer-footer  配合着fast.api.open  -->
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>