<form id="item_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="ItemNo" class="control-label col-xs-12 col-sm-2">{:__('科目ID')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="RoomTypeNo" name="row[ItemNo]" value="{$row.ItemNo}" disabled />
        </div>
    </div>
    
    <div class="form-group">
        <label for="ItemName" class="control-label col-xs-12 col-sm-2">{:__('科目名')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="nickname" name="row[ItemName]" value="{$row.ItemName}" data-rule="required" />
        </div>
    </div>
    <div class="form-group">
        <label for="BasePrice" class="control-label col-xs-12 col-sm-2">{:__('基本料金')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="BasePrice" name="row[BasePrice]" value="{$row.BasePrice}" data-rule="required;digits;"/> 
        </div>
    </div>
    
     <div class="form-group">
        <label for="ServiceRate" class="control-label col-xs-12 col-sm-2">{:__('サービス料率')}:</label>
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="ServiceRate" name="row[ServiceRate]" value="{$row.ServiceRate}" data-rule="required;float;"/> 
        </div>
    </div>
    
    
    <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('サービス料区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[ServiceKbn]', $ServiceKbnData, $row.ServiceKbn, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
    <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('消費税区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[TaxKbn]', $TaxKbnData, $row.TaxKbn, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('入湯税区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[SpaTaxKbn]', $TaxKbnData, $row.SpaTaxKbn, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('売上部門')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[SalesSectionCode]', $SalessectionData, $row.SalesSectionCode, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('集計部門')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[SummarySecCode]', $SummarysectionData,  $row.SummarySecCode, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('科目種別')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[ItemKindCode]', $ItemKindData,  $row.ItemKindCode, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('入区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[PeopleKbn]', $PeopleKbnData, $row.PeopleKbn, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('印字区分')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[ReceiptPrtKbn]', $ReceiptPrtKbnData,  $row.ReceiptPrtKbn, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>