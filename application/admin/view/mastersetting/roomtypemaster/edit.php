<form id="roomtype_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="RoomTypeNo" class="control-label col-xs-12 col-sm-2">{:__('部屋タイプID')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="RoomTypeNo" name="row[RoomTypeNo]" value="{$row.RoomTypeNo}" disabled />
        </div>
    </div>
    <div class="form-group">
        <label for="RoomTypeShortName" class="control-label col-xs-12 col-sm-2">{:__('部屋タイプ名')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="RoomTypeShortName" name="row[RoomTypeShortName]" value="{$row.RoomTypeShortName}" data-rule="required" />
        </div>
    </div>
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>