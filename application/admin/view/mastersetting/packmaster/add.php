
<div class="panel panel-default panel-intro">
    <div class="panel-heading">   
        <ul class="nav nav-tabs">

            <li class="active"><a href="#tab_pack_detail" data-toggle="tab">{:__('パック詳細')}</a></li>
            <li><a href="#tab_pack_baseitem" data-toggle="tab">{:__('パック内容:基本科目')}</a></li>
            <li><a href="#tab_pack_additem" data-toggle="tab">{:__('パック内容:追加科目')}</a></li>
           

        </ul>
    </div>

    <form id="form_pack_add_form" role="form" class="form-horizontal form-ajax"  data-toggle="validator" method="POST" action=""><!-- action="" 自提交 原来的url，这里是 'mastersetting/roommaster/add' role="form" 不能少，不然对应的js找不到这个form，无法绑定各种方法-->

        <div class="panel-body">
            <div id="myTabContent" class="tab-content">

                
                <div class="tab-pane fade active in"  id="tab_pack_detail">  
                
                <div class="form-group">
                    <label for="ItemNo" class="control-label col-xs-12 col-sm-2">{:__('科目ID')}:</label> <!-- for="RoomTypeNo"  一定要有，用来显示客户端验证的结果 -->
                    <div class="col-xs-12 col-sm-8">
                        <fieldset>
                            <input type="text" form="form_pack_add_form" class="form-control" id="ItemNo" name="row[ItemNo]" value="" data-rule="required;digits;remote[mastersetting/itemmaster/check_mul_pk]" autocomplete="off"/>  <!-- 最后的[username]   是对应validator里头的一个rule -->
                        </fieldset>
                    </div>
                </div>

                <div class="form-group">
                    <label for="SortNo" class="control-label col-xs-12 col-sm-2">{:__('表示順番')}:</label> 
                    <div class="col-xs-12 col-sm-8">
                        <input type="text" form="form_pack_add_form" class="form-control" id="SortNo" name="row[SortNo]" value="" data-rule="required;digits;"/> 
                    </div>
                </div>
                <div class="form-group">
                    <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('表示フラグ')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        {:build_select('row[DispFlg]', $DispFlgData, 1, ['form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                    </div>
                </div>



                <div class="form-group">
                    <label for="ItemName" class="control-label col-xs-12 col-sm-2">{:__('科目名')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        <input type="text" form="form_pack_add_form" class="form-control" id="nickname" name="row[ItemName]" value="" data-rule="required" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="BasePrice" class="control-label col-xs-12 col-sm-2">{:__('基本料金')}:</label> 
                    <div class="col-xs-12 col-sm-8">
                        <input type="text" form="form_pack_add_form" class="form-control" id="BasePrice" name="row[BasePrice]" value="" data-rule="required;digits;"/> 
                    </div>
                </div>

                <div class="form-group">
                    <label for="ServiceRate" class="control-label col-xs-12 col-sm-2">{:__('サービス料率')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        <input type="text"  form="form_pack_add_form" class="form-control" id="ServiceRate" name="row[ServiceRate]" value="" data-rule="required;float;"/> 
                    </div>
                </div>


                <div class="form-group">
                    <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('サービス料区分')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        {:build_select('row[ServiceKbn]', $ServiceKbnData, null, ['form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                    </div>
                </div>

                <div class="form-group">
                    <label for="role_id"  form="form_pack_add_form" class="control-label col-xs-12 col-sm-2">{:__('消費税区分')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        {:build_select('row[TaxKbn]', $TaxKbnData, null, ['form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                    </div>
                </div>

                <div class="form-group">
                    <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('入湯税区分')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        {:build_select('row[SpaTaxKbn]', $TaxKbnData, null, ['form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                    </div>
                </div>
                <div class="form-group">
                    <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('売上部門')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        {:build_select('row[SalesSectionCode]', $SalessectionData, null, ['form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                    </div>
                </div>

                <div class="form-group">
                    <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('集計部門')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        {:build_select('row[SummarySecCode]', $SummarysectionData, null, ['form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                    </div>
                </div>

                <div class="form-group">
                    <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('科目種別')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        {:build_select('row[ItemKindCode]', $ItemKindData, null, ['form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                    </div>
                </div>

                <div class="form-group">
                    <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('入区分')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        {:build_select('row[PeopleKbn]', $PeopleKbnData, 1, ['form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                    </div>
                </div>

                <div class="form-group">
                    <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('印字区分')}:</label>
                    <div class="col-xs-12 col-sm-8">
                        {:build_select('row[ReceiptPrtKbn]', $ReceiptPrtKbnData, 1, ['form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                    </div>
                </div>
 </div>
                
                <div class="tab-pane fade"  id="tab_pack_baseitem">  
                    <div class="form-group">
                        <label for="PackBaseitem" class="control-label col-xs-12 col-sm-2">{:__('基本科目')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('PackBaseitem', $PackBaseitemLst, $PackBaseitemIni, ['id'=>'PackBaseitem' , 'data-live-search'=>'true' ,'form'=>'form_pack_add_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="PackBaseitemAmount" class="control-label col-xs-12 col-sm-2">{:__('基本科目数量')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            <input type="number" name="PackBaseitemAmount" id='PackBaseitemAmount' class="form-control" value="1" size="10" />
                        </div>
                    </div>
                    
                </div>
                
                 <div class="tab-pane fade"  id="tab_pack_additem">
                     
                        <!-- 数量対応できない
                    <div class="form-group">
                        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('追加科目(複数可)')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('row[PackAdditems][]', $PackAdditemLst, null, ['data-live-search'=>'true' ,'form'=>'form_pack_add_form','multiple'=>'true','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>
          
          -->
                     
             <dl class="fieldlist" rel="1" data-name="PackAdditems">
              <dd>
                  <ins>{:__('追加科目IDと数量')}</ins>
                  <ins>{:__('')}</ins>
              </dd>
              <dd class="form-inline">
                  <span>
                  {:build_select('PackAdditems[additem_id][0]', $PackAdditemLst, $PackAdditemIni, ['id'=>'PackAdditems_0' ,  'data-live-search'=>'true' ,'form'=>'form_pack_add_form','class'=>'selectpicker', 'data-rule'=>'required'])}
                  </span>
                  <input type="number" name="PackAdditems[additem_amount][0]" id='PackAdditems_amount_0' class="form-control" value="1" size="10" />
                  <span class="btn btn-sm btn-primary btn-dragsort"><i class="fa fa-arrows"></i></span>
              </dd>
              <dd><a href="javascript:;" class="append btn btn-sm btn-success"><i class="fa fa-plus"></i> {:__('追加')}</a></dd>
          </dl>
                     
                 </div>
                
                <div class="form-group" ><!--style="background:#00FF00 "-->
                    <label class="control-label col-xs-12 col-sm-2"></label>
                    <div class="col-xs-12 col-sm-4">
                        <button id="btn_pack_add_sumit" type="submit" class="btn btn-success btn-embossed" disabled="">{:__('登録(全タブ)')}</button>
                        <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
                    </div>
                </div>

               
            </div>
        </div>
    </form>
</div>