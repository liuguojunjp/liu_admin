
<div class="panel panel-default panel-intro">
    <div class="panel-heading">   
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_pack_detail" data-toggle="tab">{:__('パック詳細')}</a></li>
            <li><a href="#tab_pack_baseitem" data-toggle="tab">{:__('パック内容:基本科目')}</a></li>
            <li><a href="#tab_pack_additem" data-toggle="tab">{:__('パック内容:追加科目')}</a></li>
        </ul>
    </div>

    <form id="form_pack_edit_form" role="form" class="form-horizontal form-ajax"  data-toggle="validator" method="POST" action=""><!-- action="" 自提交 原来的url，这里是 'mastersetting/roommaster/add' role="form" 不能少，不然对应的js找不到这个form，无法绑定各种方法-->

        <div class="panel-body">
            <div id="myTabContent" class="tab-content">


                <div class="tab-pane fade active in"  id="tab_pack_detail">  

                    <div class="form-group">
                        <label for="ItemNo" class="control-label col-xs-12 col-sm-2">{:__('科目ID')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" form="form_pack_edit_form" id="RoomTypeNo" name="row[ItemNo]" value="{$row.ItemNo}" disabled />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ItemName" class="control-label col-xs-12 col-sm-2">{:__('科目名')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" form="form_pack_edit_form" id="nickname" name="row[ItemName]" value="{$row.ItemName}" data-rule="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="BasePrice" class="control-label col-xs-12 col-sm-2">{:__('基本料金')}:</label> 
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" form="form_pack_edit_form" id="BasePrice" name="row[BasePrice]" value="{$row.BasePrice}" data-rule="required;digits;"/> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ServiceRate" class="control-label col-xs-12 col-sm-2">{:__('サービス料率')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" form="form_pack_edit_form" id="ServiceRate" name="row[ServiceRate]" value="{$row.ServiceRate}" data-rule="required;float;"/> 
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('サービス料区分')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('row[ServiceKbn]', $ServiceKbnData, $row.ServiceKbn, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('消費税区分')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('row[TaxKbn]', $TaxKbnData, $row.TaxKbn, ['form'=>'form_pack_edit_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('入湯税区分')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('row[SpaTaxKbn]', $TaxKbnData, $row.SpaTaxKbn, ['form'=>'form_pack_edit_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('売上部門')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('row[SalesSectionCode]', $SalessectionData, $row.SalesSectionCode, ['form'=>'form_pack_edit_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('集計部門')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('row[SummarySecCode]', $SummarysectionData,  $row.SummarySecCode, ['form'=>'form_pack_edit_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('科目種別')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('row[ItemKindCode]', $ItemKindData,  $row.ItemKindCode, ['form'=>'form_pack_edit_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('入区分')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('row[PeopleKbn]', $PeopleKbnData, $row.PeopleKbn, ['form'=>'form_pack_edit_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('印字区分')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('row[ReceiptPrtKbn]', $ReceiptPrtKbnData,  $row.ReceiptPrtKbn, ['form'=>'form_pack_edit_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade"  id="tab_pack_baseitem">  
                    <div class="form-group">
                        <label for="PackBaseitem" class="control-label col-xs-12 col-sm-2">{:__('基本科目')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            {:build_select('PackBaseitem', $PackBaseitemLst, $PackBaseitemIni, ['id'=>'PackBaseitem' , 'data-live-search'=>'true' ,'form'=>'form_pack_edit_form','class'=>'form-control selectpicker', 'data-rule'=>'required'])}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="PackBaseitemAmount" class="control-label col-xs-12 col-sm-2">{:__('基本科目数量')}:</label>
                        <div class="col-xs-12 col-sm-8">
                            <input type="number" name="PackBaseitemAmount" id='PackBaseitemAmount' class="form-control" value="{$PackBaseitemAmountIni}" size="10" />
                        </div>
                    </div>

                </div>

                <div class="tab-pane fade"  id="tab_pack_additem">
                    <dl class="fieldlist" rel="{$add_item_count}" data-name="PackAdditems">
                        <dd>
                            <ins>{:__('追加科目IDと数量')}</ins>
                            <ins>{:__('')}</ins>
                        </dd>
                        <dd class="form-inline">
                            <span>
                                {:build_select('PackAdditems[additem_id][0]', $PackAdditemLst, $PackAdditemIni, ['id'=>'PackAdditems_0' ,  'data-live-search'=>'true' ,'form'=>'form_pack_edit_form','class'=>'selectpicker', 'data-rule'=>'required'])}
                            </span>
                            <input type="number" name="PackAdditems[additem_amount][0]" id='PackAdditems_amount_0' class="form-control" value="{$PackAdditemAmountIni}" size="10" />
                            <span class="btn btn-sm btn-primary btn-dragsort"><i class="fa fa-arrows"></i></span>
                        </dd>
                        <?php foreach ($add_item_lst_other as $idx => $add_item) {
                            ?>
                            <dd class="form-inline">
                                <span>
                                    <select id="PackAdditems_{$idx}" class="selectpicker" data-live-search="true"  form="form_pack_edit_form"  name="PackAdditems[additem_id][{$idx}]">
                                        <?php foreach ($PackAdditemLst as $additem_id => $add_item_name) { ?>
                                            <option value="{$additem_id}" <?php if ((int)$additem_id == (int)$add_item['ID']) {echo ' selected="selected" ';} ?>>{$add_item_name}</option>
                                        <?php } ?>
                                    </select>
                                </span>
                                <input type="number" name="PackAdditems[additem_amount][{$idx}]" id="PackAdditems_amount_{$idx}" class="form-control" value="{$add_item.Amount}" size="10" />
                                <span class="btn btn-sm btn-danger btn-remove"><i class="fa fa-times"></i></span>
                                <span class="btn btn-sm btn-primary btn-dragsort"><i class="fa fa-arrows"></i></span>
                            <?php } ?>
                        <dd><a href="javascript:;" class="append btn btn-sm btn-success"><i class="fa fa-plus"></i> {:__('追加')}</a></dd>
                    </dl>
                </div>
                <div class="form-group" ><!--style="background:#00FF00 "-->
                    <label class="control-label col-xs-12 col-sm-2"></label>
                    <div class="col-xs-12 col-sm-4">
                        <button id="btn_pack_add_sumit" type="submit" class="btn btn-success btn-embossed" disabled="">{:__('登録(全タブ)')}</button>
                        <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>