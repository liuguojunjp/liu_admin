<div class="panel panel-default panel-intro">
    <div class="panel-heading">
        {:build_heading(null, false)}
        <ul class="nav nav-tabs">
            {foreach $tab_list as $tab_name=>$tab_detail}
            <?php
            if ($tab_detail['is_show']) {
                ?>
                <li class="{$tab_detail.is_active?'active':''}"><a href="#{$tab_name}" data-toggle="tab">{:__($tab_detail.title)}</a></li>
                <?php
            }
            ?>
            {/foreach}
        </ul>
    </div>
    <form id="form_ctlmst_edit" name="form_ctlmst_edit" class="edit-form form-horizontal" role="form" data-toggle="validator" method="POST" action="{:url('update')}">
        <!-- 或写绝对{:url('/admin/mastersetting/controlmaster/update')} -->
        <!-- 会生成  action="/admin/mastersetting.controlmaster/update" -->
   
        <div class="panel-body">
            <div id="myTabContent" class="tab-content">
                {foreach $tab_list as $tab_name=>$tab_detail}
                  <?php if ($tab_detail['is_show']) { ?>                
                    <div class="{$tab_detail.is_active?'tab-pane fade active in':'tab-pane fade'}"  id="{$tab_name}">  
                        <?php
                        foreach ($option_list as $option_key => $option_detail) {
                            if ($option_detail['tab_id'] !== $tab_name) {
                                continue;
                            }
                         ?>
                            <div class="widget-body no-padding">
                                <div class="form-group">
                                    <label for="{$option_key}" class="control-label col-xs-12 col-sm-2">{:__($option_detail.title)}:</label>
                                    
                                    <div class="col-xs-12 col-sm-4">
                                        <?php
                                                                 switch ($option_detail['view_type'] )
                                                                 {
                                                                   case 'textbox':  
                                       ?>
                                        <input form="form_ctlmst_edit" type="text" class="form-control" id="{$option_key}" name="ctl_mst_setting[{$option_key}]" value="{$option_detail.value}"  data-tip="{$option_detail.data_tip}" data-rule="{$option_detail.data_rule}" {$option_detail.extend}/>
                                            <?php
                                           break;
                                    ?>
                                        
                                        
                                           <?php
                                                                 
                                                                   case 'textbox_timepicker':  
                                       ?>
                                         <input form="form_ctlmst_edit" type="text" class="form-control datetimepicker" id="{$option_key}" name="ctl_mst_setting[{$option_key}]" value="{$option_detail.value}"  data-tip="{$option_detail.data_tip}" data-rule="{$option_detail.data_rule}" {$option_detail.extend}/>
                                      <?php
                                           break;
                                    ?>
                                        
                                        
                                         <?php
                                                                 
                                                                   case 'textarea':  
                                       ?>
                                        <textarea id="{$option_key}" name="ctl_mst_setting[{$option_key}]" class="form-control" data-rule="{$option_detail.data_rule}" rows="5" data-tip="{$option_detail.data_tip}" {$option_detail.extend}>{$option_detail.value}</textarea>
                                      <?php
                                           break;
                                    ?>
                                         
                                         
                                         
                                         <?php
                                                                 
                                                                   case 'select':  
                                       ?>
                                       
                                        
                                        
                                        <select name="ctl_mst_setting[{$option_key}]" class="form-control selectpicker">
                                <?php
                                                                 
                                                                       foreach ($option_detail['select_list'] as $value => $title) {
                                                                           
                                                                       
                                       ?>
                                <option value="{$value}" {$value==$option_detail.value?'selected':''}>{$title}</option>
                                <?php
                                                                 
                                                                 }
                                       ?>
                            </select>
                                        
                                        
                                      <?php
                                           break;
                                    ?>
                                        
                                        
                                        <?php
                                                                 
                                                                   case 'number':  
                                       ?>
                                        <input type="number" name="ctl_mst_setting[{$option_key}]" value="{$option_detail.value}" class="form-control"  data-tip="{$option_detail.data_tip}" data-rule="{$option_detail.data_rule}" {$option_detail.extend}  />
                                      <?php
                                           break;
                                    ?>
                                        
                                        
                                         <?php
                                                                 
                                                                   case 'radio':  
                                                                       
                                                                   foreach ($option_detail['radio_content'] as $radio_key => $radio_title)    
                                                                   {
                                                                       
                                       ?>
                                        
                                                 <label for="ctl_mst_setting[{$option_key}]_{$radio_key}"><input id="ctl_mst_setting[{$option_key}]_{$radio_key}" name="ctl_mst_setting[{$option_key}]" type="radio" class="minimal" value="{$radio_key}" {$radio_key==$option_detail.value?'checked':''} data-tip="{$option_detail.data_tip}" /> {:__($radio_title)}</label>  <!-- {in name="radio_key" value="$option_detail.value"}checked{/in} -->
                                         
                                      <?php
                                                                 }
                                           break;
                                    ?>
                                        
                                        
                                                 
                                                 
                                                  <?php
                                                                 
                                                                   case 'checkbox_group':  
                                                                       
                                                                   foreach ($option_detail['checkbox_group_content'] as $checkbox_key => $checkbox_detail)    
                                                                   {
                                                                       
                                       ?>
                                        
                                                 <label for="ctl_mst_setting[{$checkbox_key}]"><input id="ctl_mst_setting[{$checkbox_key}]" name="ctl_mst_setting[{$checkbox_key}]" type="checkbox"  class="minimal" value="1" {$checkbox_detail.checkbox_value==$option_detail.value?'checked':''}  /> {:__($checkbox_detail.checkbox_title)}</label>  <!-- {in name="radio_key" value="$option_detail.value"}checked{/in} -->
                                         
                                      <?php
                                                                 }
                                           break;
                                    ?>
                                        
                                                 
                                        
                                        
                                        
                                        
                                        <?php
                                                                   default:
                                                                       break;
                                                                 }// end of ---switch ($option_detail['view_type'] )
                         ?>
                                        
                                    </div>
                                    
                                    
                                </div>

                            </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                {/foreach}
                
             
                <div class="form-group" ><!--style="background:#00FF00 "-->
                    <label class="control-label col-xs-12 col-sm-2"></label>
                    <div class="col-xs-12 col-sm-4">
                        <button id="btn_ctl_mst_edit_sumit" type="submit" class="btn btn-success btn-embossed" disabled="">{:__('登録(全タブ)')}</button>
                        <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>  
</div>