<form id="room_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="RoomNo" class="control-label col-xs-12 col-sm-2">{:__('部屋ID')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="RoomTypeNo" name="row[RoomNo]" value="{$row.RoomNo}" disabled />
        </div>
    </div>
    <div class="form-group">
        <label for="RoomName" class="control-label col-xs-12 col-sm-2">{:__('部屋名')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="RoomName" name="row[RoomName]" value="{$row.RoomName}" data-rule="required" />
        </div>
    </div>
    
     <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('部屋タイプ')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[RoomTypeNo]', $RoomTypeData, $row.RoomTypeNo, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>