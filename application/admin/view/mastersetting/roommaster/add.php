<form id="roomtype_mst_add-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="RoomNo" class="control-label col-xs-12 col-sm-2">{:__('部屋番号')}:</label> <!-- for="RoomTypeNo"  一定要有，用来显示客户端验证的结果 -->
        <div class="col-xs-12 col-sm-8">
            <fieldset>
             <input type="text" class="form-control" id="RoomNo" name="row[RoomNo]" value="" data-rule="required;digits;remote[mastersetting/roommaster/check_mul_pk]" autocomplete="off"/>  <!-- 最后的[username]   是对应validator里头的一个rule -->
            </fieldset>
        </div>
    </div>
    
   <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('部屋タイプ')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[RoomTypeNo]', $RoomTypeData, null, ['class'=>'form-control selectpicker', 'data-rule'=>'required'])}
        </div>
    </div>
    
    
    <div class="form-group">
        <label for="RoomName" class="control-label col-xs-12 col-sm-2">{:__('部屋名')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="RoomName" class="form-control" id="password" name="row[RoomName]" value="" data-rule="required" />
        </div>
    </div>
    
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>