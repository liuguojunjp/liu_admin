<form id="corp_mst_add-form" role="form" class="form-horizontal form-ajax"  data-toggle="validator" method="POST" action=""><!-- action="" 自提交 原来的url，这里是 'mastersetting/roommaster/add' role="form" 不能少，不然对应的js找不到这个form，无法绑定各种方法-->
    <div class="form-group">
        <label for="CorpCode" class="control-label col-xs-12 col-sm-2">{:__('会社コード')}:</label> <!-- for="xxxx"  一定要有，用来显示客户端验证的结果 -->
        <div class="col-xs-12 col-sm-8">
            <fieldset>
             <input type="text" class="form-control" id="CorpCode" name="row[CorpCode]" value="" data-rule="required;digits;remote[mastersetting/corpmaster/check_mul_pk]" autocomplete="off"/>  <!-- 最后的[username]   是对应validator里头的一个rule -->
            </fieldset>
        </div>
    </div>
    
      <div class="form-group">
        <label for="Name" class="control-label col-xs-12 col-sm-2">{:__('会社名')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="Name" name="row[Name]" value="" data-rule="required;"/> 
        </div>
    </div>
    
    <div class="form-group">
        <label for="Kana" class="control-label col-xs-12 col-sm-2">{:__('会社カナ')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Kana" name="row[Kana]" value="" data-rule="required" />
        </div>
    </div>
    
     <div class="form-group">
        <label for="Tel" class="control-label col-xs-12 col-sm-2">{:__('会社電話')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Tel" name="row[Tel]" value=""  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Fax" class="control-label col-xs-12 col-sm-2">{:__('会社FAX')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Fax" name="row[Fax]" value=""  />
        </div>
    </div>
    
     <div class="form-group">
        <label for="ZipCode" class="control-label col-xs-12 col-sm-2">{:__('会社郵便番号')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="ZipCode" name="row[ZipCode]" value=""  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Address1" class="control-label col-xs-12 col-sm-2">{:__('都道府県')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Address1" name="row[Address1]" value=""  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Address2" class="control-label col-xs-12 col-sm-2">{:__('都市')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Address2" name="row[Address2]" value=""  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Address3" class="control-label col-xs-12 col-sm-2">{:__('区町村')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Address3" name="row[Address3]" value=""  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Address4" class="control-label col-xs-12 col-sm-2">{:__('番地')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Address4" name="row[Address4]" value=""  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Mail" class="control-label col-xs-12 col-sm-2">{:__('メール')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Mail" name="row[Mail]" value="" data-rule="email" />
        </div>
    </div>
    
    
     <div class="form-group">
        <label for="FreeSummary1" class="control-label col-xs-12 col-sm-2">{:__('会社自由項目1')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="FreeSummary1" name="row[FreeSummary1]" value=""  />
        </div>
    </div>
    
     <div class="form-group">
        <label for="FreeSummary2" class="control-label col-xs-12 col-sm-2">{:__('会社自由項目2')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="FreeSummary2" name="row[FreeSummary2]" value=""  />
        </div>
    </div>
    
     <div class="form-group">
        <label for="FreeSummary3" class="control-label col-xs-12 col-sm-2">{:__('会社自由項目3')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="FreeSummary3" name="row[FreeSummary3]" value=""  />
        </div>
    </div>

    <div class="form-group">
        <label for="Remark" class="control-label col-xs-12 col-sm-2">{:__('会社備考')}:</label>
        <div class="col-xs-12 col-sm-8">
            <textarea name="row[Remark]" class="form-control" rows="3" id="Remark"></textarea>
        </div>
    </div>
    
   
    <div class="form-group hidden layer-footer"><!-- layer-footer  配合着fast.api.open  -->
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>