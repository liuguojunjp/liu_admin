<form id="corp_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="CorpCode" class="control-label col-xs-12 col-sm-2">{:__('会社ID')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="CorpCode" name="row[CorpCode]" value="{$row.CorpCode}" disabled />
        </div>
    </div>
    
  
    <div class="form-group">
        <label for="Name" class="control-label col-xs-12 col-sm-2">{:__('会社名')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="Name" name="row[Name]" value="{$row.Name}" data-rule="required;"/> 
        </div>
    </div>
    
    <div class="form-group">
        <label for="Kana" class="control-label col-xs-12 col-sm-2">{:__('会社カナ')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Kana" name="row[Kana]" value="{$row.Kana}" data-rule="required" />
        </div>
    </div>
    
     <div class="form-group">
        <label for="Tel" class="control-label col-xs-12 col-sm-2">{:__('会社電話')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Tel" name="row[Tel]" value="{$row.Tel}"  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Fax" class="control-label col-xs-12 col-sm-2">{:__('会社FAX')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Fax" name="row[Fax]" value="{$row.Fax}"  />
        </div>
    </div>
    
     <div class="form-group">
        <label for="ZipCode" class="control-label col-xs-12 col-sm-2">{:__('会社郵便番号')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="ZipCode" name="row[ZipCode]" value="{$row.ZipCode}"  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Address1" class="control-label col-xs-12 col-sm-2">{:__('都道府県')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Address1" name="row[Address1]" value="{$row.Address1}"  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Address2" class="control-label col-xs-12 col-sm-2">{:__('都市')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Address2" name="row[Address2]" value="{$row.Address2}"  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Address3" class="control-label col-xs-12 col-sm-2">{:__('区町村')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Address3" name="row[Address3]" value="{$row.Address3}"  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Address4" class="control-label col-xs-12 col-sm-2">{:__('番地')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Address4" name="row[Address4]" value="{$row.Address4}"  />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Mail" class="control-label col-xs-12 col-sm-2">{:__('メール')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Mail" name="row[Mail]" value="{$row.Mail}" data-rule="email" />
        </div>
    </div>
    
    
     <div class="form-group">
        <label for="FreeSummary1" class="control-label col-xs-12 col-sm-2">{:__('会社自由項目1')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="FreeSummary1" name="row[FreeSummary1]" value="{$row.FreeSummary1}"  />
        </div>
    </div>
    
     <div class="form-group">
        <label for="FreeSummary2" class="control-label col-xs-12 col-sm-2">{:__('会社自由項目2')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="FreeSummary2" name="row[FreeSummary2]" value="{$row.FreeSummary2}"  />
        </div>
    </div>
    
     <div class="form-group">
        <label for="FreeSummary3" class="control-label col-xs-12 col-sm-2">{:__('会社自由項目3')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="FreeSummary3" name="row[FreeSummary3]" value="{$row.FreeSummary3}"  />
        </div>
    </div>
    
     <div class="form-group">
        <label for="Remark" class="control-label col-xs-12 col-sm-2">{:__('会社備考')}:</label>
        <div class="col-xs-12 col-sm-8">
            <textarea name="row[Remark]" class="form-control" rows="3" id="Remark"  >{$row.Remark}</textarea>
        </div>
    </div>
    
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>