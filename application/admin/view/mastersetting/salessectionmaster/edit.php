<form id="salessection_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="SalesSectionCode" class="control-label col-xs-12 col-sm-2">{:__('売上部門ID')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="SalesSectionCode" name="row[SalesSectionCode]" value="{$row.SalesSectionCode}" disabled />
        </div>
    </div>
    
  
    <div class="form-group">
        <label for="SalesSectionName" class="control-label col-xs-12 col-sm-2">{:__('売上部門名')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="SalesSectionName" name="row[SalesSectionName]" value="{$row.SalesSectionName}" data-rule="required;"/> 
        </div>
    </div>
    
  
    
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>