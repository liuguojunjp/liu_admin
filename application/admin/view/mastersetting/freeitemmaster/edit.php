<form id="freeitem_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">


    <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('自由集計')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[FreeID]', $FreeitemSelectData, $row.FreeID, ['class'=>'form-control selectpicker', 'data-rule'=>'required' ,'disabled'])}
        </div>
    </div>

    <div class="form-group">
        <label for="FreeItemNo" class="control-label col-xs-12 col-sm-2">{:__('自由集計項目ID')}:</label> <!-- for="xxxx"  一定要有，用来显示客户端验证的结果 -->
        <div class="col-xs-12 col-sm-8">
            <fieldset>
                <input type="text" class="form-control" id="BranchCode" name="row[FreeItemNo]" value="{$row.FreeItemNo}" disabled autocomplete="off"/>  <!-- 最后的[username]   是对应validator里头的一个rule -->
            </fieldset>
        </div>
    </div>

    <div class="form-group">
        <label for="FreeItemName" class="control-label col-xs-12 col-sm-2">{:__('自由集計項目名')}:</label> 
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="FreeItemName" name="row[FreeItemName]" value="{$row.FreeItemName}" data-rule="required;"/> 
        </div>
    </div>

    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>