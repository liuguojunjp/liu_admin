<div class="panel panel-default panel-intro">
    {:build_heading()}<!-- admin/common.php  生成页面Heading-->
    <div class="panel-body">
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="one">
                <div class="widget-body no-padding">
                    <div id="toolbar" class="toolbar">
                      <!--  {:build_toolbar('refresh,delete')}-->
                        {:build_toolbar('refresh,add')}<p id="enable" class="btn btn-default">編集モードへ</p>
                    </div>
                    <table id="table_agentbranchmaster_list" class="table table-striped table-bordered table-hover" 
                           data-operate-edit="{:$auth->check('auth/admin/edit')}" 
                           data-operate-del="{:$auth->check('auth/admin/del')}" 
                           width="100%">
                        <!-- data-operate-del="{:$auth->check('auth/adminlog/del')}" -->
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>