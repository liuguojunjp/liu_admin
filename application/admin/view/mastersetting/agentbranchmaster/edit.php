<form id="agentbranch_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    

    <div class="form-group">
        <label for="role_id" class="control-label col-xs-12 col-sm-2">{:__('業者')}:</label>
        <div class="col-xs-12 col-sm-8">
            {:build_select('row[AgentCode]', $AgentSelectData, $row.AgentCode, ['class'=>'form-control selectpicker', 'data-rule'=>'required' ,'disabled'])}
        </div>
    </div>
    
    <div class="form-group">
        <label for="BranchCode" class="control-label col-xs-12 col-sm-2">{:__('支店コード')}:</label> <!-- for="xxxx"  一定要有，用来显示客户端验证的结果 -->
        <div class="col-xs-12 col-sm-8">
            <fieldset>
             <input type="text" class="form-control" id="BranchCode" name="row[BranchCode]" value="{$row.BranchCode}" disabled autocomplete="off"/>  <!-- 最后的[username]   是对应validator里头的一个rule -->
            </fieldset>
        </div>
    </div>
    
    
    
     <div class="form-group">
        <label for="BranchName" class="control-label col-xs-12 col-sm-2">{:__('支店名')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="BranchName" name="row[BranchName]" value="{$row.BranchName}" data-rule="required;"/> 
        </div>
    </div>
    
     <div class="form-group">
        <label for="BranchKana" class="control-label col-xs-12 col-sm-2">{:__('支店カナ')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="BranchKana" name="row[BranchKana]" value="{$row.BranchKana}" data-rule="required;"/> 
        </div>
    </div>
    
  
    
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>