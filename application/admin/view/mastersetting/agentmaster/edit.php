<form id="salessection_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="AgentCode" class="control-label col-xs-12 col-sm-2">{:__('業者ID')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="AgentCode" name="row[AgentCode]" value="{$row.AgentCode}" disabled />
        </div>
    </div>
    
  

    
       <div class="form-group">
        <label for="AgentName" class="control-label col-xs-12 col-sm-2">{:__('業者名')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="AgentName" name="row[AgentName]" value="{$row.AgentName}" data-rule="required;"/> 
        </div>
    </div>
    
     <div class="form-group">
        <label for="AgentKana" class="control-label col-xs-12 col-sm-2">{:__('業者カナ')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="AgentKana" name="row[AgentKana]" value="{$row.AgentKana}" data-rule="required;"/> 
        </div>
    </div>
    
  
    
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>