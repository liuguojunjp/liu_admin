<form id="agent_mst_add-form" role="form" class="form-horizontal form-ajax"  data-toggle="validator" method="POST" action=""><!-- action="" 自提交 原来的url，这里是 'mastersetting/roommaster/add' role="form" 不能少，不然对应的js找不到这个form，无法绑定各种方法-->
    <div class="form-group">
        <label for="AgentCode" class="control-label col-xs-12 col-sm-2">{:__('業者コード')}:</label> <!-- for="xxxx"  一定要有，用来显示客户端验证的结果 -->
        <div class="col-xs-12 col-sm-8">
            <fieldset>
             <input type="text" class="form-control" id="AgentCode" name="row[AgentCode]" value="" data-rule="required;digits;remote[mastersetting/agentmaster/check_mul_pk]" autocomplete="off"/>  <!-- 最后的[username]   是对应validator里头的一个rule -->
            </fieldset>
        </div>
    </div>
    
      <div class="form-group">
        <label for="AgentName" class="control-label col-xs-12 col-sm-2">{:__('業者名')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="AgentName" name="row[AgentName]" value="" data-rule="required;"/> 
        </div>
    </div>
    
     <div class="form-group">
        <label for="AgentKana" class="control-label col-xs-12 col-sm-2">{:__('業者カナ')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="AgentKana" name="row[AgentKana]" value="" data-rule="required;"/> 
        </div>
    </div>
    
    
    <div class="form-group hidden layer-footer"><!-- layer-footer  配合着fast.api.open  -->
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>