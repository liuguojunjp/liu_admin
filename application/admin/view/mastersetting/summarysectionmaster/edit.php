<form id="summarysection_mst_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="SummarySecCode" class="control-label col-xs-12 col-sm-2">{:__('集計部門ID')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="SummarySecCode" name="row[SummarySecCode]" value="{$row.SummarySecCode}" disabled />
        </div>
    </div>
    
  
    <div class="form-group">
        <label for="SummarySecName" class="control-label col-xs-12 col-sm-2">{:__('集計部門名')}:</label> 
        <div class="col-xs-12 col-sm-8">
             <input type="text" class="form-control" id="SummarySecName" name="row[SummarySecName]" value="{$row.SummarySecName}" data-rule="required;"/> 
        </div>
    </div>
    
  
    
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>