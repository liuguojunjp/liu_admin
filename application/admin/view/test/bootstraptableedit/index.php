<div class="panel panel-default panel-intro">
    {:build_heading()}
<br>
<div class="panel-body">
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="one">
                <div class="widget-body no-padding">
                    <div id="toolbar" class="toolbar">
                        {:build_toolbar('refresh')}<p id="enable" class="btn btn-default">編集モードへ</p>
                    </div>
                    <table id="table_edit_test" class="table table-striped table-bordered table-hover" width="100%">
                    </table>
                </div>
            </div>
        </div>
    </div>
<br>
</div>

