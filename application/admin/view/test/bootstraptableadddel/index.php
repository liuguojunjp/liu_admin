<div class="panel panel-default panel-intro">
    {:build_heading()}
    <br>
    <div class="panel-body">
        <div id="myTabContent" class="tab-content">
            <div id="toolbar" class="btn-group">
                <button id="add" class="btn btn-default" title="添加">
                    <i class="glyphicon glyphicon-plus"></i> 添加
                </button>
                <button id="del" class="btn btn-default" title="删除">
                    <i class="glyphicon glyphicon-minus"></i> 删除
                </button>
            </div>
            <table id="wareTable"
                   data-toolbar="#toolbar">
                <thead>
                    <tr>
                        <th data-field="checkbox" data-checkbox="true"></th>
                        <th data-field="wareName">库房名称</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>



    <br>
</div>

