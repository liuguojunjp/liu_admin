
<div class="panel panel-default panel-intro">
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#addcfg" data-toggle="tab"><i class="fa fa-plus"></i></a>
            </li>
            
            <li >
                <a href="#aaaaaa" data-toggle="tab">aaaaaa</a>
            </li>
        </ul>
    </div>
    <form id="add-form1" name="add-form1" class="form-horizontal" role="form" data-toggle="validator" method="POST" action="{:url('/admin/test/ajax/add')}">
        <div class="panel-body">
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade" id="addcfg">
                        <div class="form-group">
                            <label for="name" class="control-label col-xs-12 col-sm-2">{:__('Name')}:</label>
                            <div class="col-xs-12 col-sm-4">
                                <input form="add-form1" type="text" class="form-control" id="name" name="row[name]" value=""  />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="control-label col-xs-12 col-sm-2">{:__('Title')}:</label>
                            <div class="col-xs-12 col-sm-4">
                                <input type="text" form="add-form1" class="form-control" id="title" name="row[title]" value="" data-rule="required" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="value" class="control-label col-xs-12 col-sm-2">{:__('Value')}:</label>
                            <div class="col-xs-12 col-sm-4">
                                <input type="text" form="add-form1" class="form-control" id="value" name="row[value]" value="" data-rule="" />
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="extend" class="control-label col-xs-12 col-sm-2">{:__('Extend')}:</label>
                            <div class="col-xs-12 col-sm-4">
                                <textarea name="row[extend]" id="extend" form="add-form1" cols="30" rows="5" class="form-control" data-rule=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-2"></label>
                            <div class="col-xs-12 col-sm-4">
                                <button type="submit" class="btn btn-success btn-embossed">{:__('OK')}</button>
                                <button type="reset" class="btn btn-default btn-embossed">{:__('Reset')}</button>
                            </div>
                        </div>
                        <button id="change_url" class="btn btn-success btn-embossed">{:__('change_url')}</button>
                </div>
                <div class="tab-pane fade" id="aaaaaa">

                    <!-- 一定要有name属性-->
                    <div class="form-group">
                        <label for="name1" class="control-label col-xs-12 col-sm-2">{:__('Name1')}:</label>
                        <div class="col-xs-12 col-sm-4">
                            <input type="text" class="form-control" id="name1" name="row[name][name1]" form="add-form1" value="" data-rule="required; length(3~30); " />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name2" class="control-label col-xs-12 col-sm-2">{:__('Name2')}:</label>
                        <div class="col-xs-12 col-sm-4">
                            <input type="text" class="form-control" id="name2" name="row[name][name2]" form="add-form1" value="" data-rule="required; length(3~30); " />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="extend1" class="control-label col-xs-12 col-sm-2">{:__('Extend1')}:</label>
                        <div class="col-xs-12 col-sm-4">
                            <textarea name="row[extend1]" id="extend1" form="add-form1" cols="30" rows="5" class="form-control" data-rule=""></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-2"></label>
                        <div class="col-xs-12 col-sm-4">
                            <button id="btn_add-form1_submit" name="btn_add-form1_submit" type="submit" class="btn btn-success btn-embossed">{:__('OK')}</button>
                            <button type="reset" class="btn btn-default btn-embossed">{:__('Reset')}</button>
                        </div>
                    </div>
                    <button id="direct_ajax" class="btn btn-success btn-embossed">{:__('direct_ajax')}</button>
                    
                     <button id="direct_ajax_para" class="btn btn-success btn-embossed">{:__('direct_ajax_para')}</button>
                    
                     <button id="part_submit" class="btn btn-success btn-embossed">{:__('part_submit')}</button>
                    
                    
                </div>
                
                
                
                
            </div>
        </div>
    </form>

</div>
