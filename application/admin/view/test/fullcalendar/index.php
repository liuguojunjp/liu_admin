<style>
    .radios{margin:20px 0 0}
    .label_canlendar{display: inline-block;width:63px;cursor: pointer}
</style>
<script type="text/javascript">
    var events_json = '{$events|json_encode}';
</script>
<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <div class="main-content">

            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <div ></div>
                                        <div id="calendar" ></div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="widget-box transparent">
                                            <div class="widget-header">
                                                <h4>Draggable events</h4>
                                            </div>
                                            <div class="widget-body">
                                                <div class="widget-main no-padding">
                                                    <div id="external-events">
                                                        <div data-class="bg-gray-active color-palette" class="external-event bg-gray-active color-palette ui-draggable" style="position: relative;">
                                                            <i class="icon-move"></i>
                                                            My Event 1
                                                        </div>

                                                        <div data-class="label-success" class="external-event label-success ui-draggable" style="position: relative;">
                                                            <i class="icon-move"></i>
                                                            My Event 2
                                                        </div>

                                                        <div data-class="label-danger" class="external-event label-danger ui-draggable" style="position: relative;">
                                                            <i class="icon-move"></i>
                                                            My Event 3
                                                        </div>
                                                        <div data-class="label-info" class="external-event label-info ui-draggable" style="position: relative;">
                                                            <i class="icon-move"></i>
                                                            My Event 4
                                                        </div>
                                                        
                                                        <div data-class="bg-navy-active color-palette" class="external-event bg-navy-active color-palette ui-draggable" style="position: relative;">
                                                            <i class="icon-move"></i>
                                                            My Event 5
                                                        </div>
                                                        <div data-class="bg-yellow-active color-palette" class="external-event bg-yellow-active color-palette ui-draggable" style="position: relative;">
                                                            <i class="icon-move"></i>
                                                            My Event 6
                                                        </div>
                                                        
                                                        
                                                         <button id="btn_refresh" class="btn btn-success btn-embossed">{:__('最新')}</button>
                                                         <button id="btn_closetabs" class="btn btn-success btn-embossed">{:__('閉じる')}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
