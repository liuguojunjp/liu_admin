<form id="client_edit-form" class="form-horizontal form-ajax" role="form" data-toggle="validator" method="POST" action="">
    <div class="form-group">
        <label for="ClientNo" class="control-label col-xs-12 col-sm-2">{:__('顧客番号')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="ClientNo" name="row[ClientNo]" value="{$row.ClientNo}" disabled />
        </div>
    </div>
    
    <div class="form-group">
        <label for="Name" class="control-label col-xs-12 col-sm-2">{:__('名前')}:</label>
        <div class="col-xs-12 col-sm-8">
            <input type="text" class="form-control" id="Name" name="row[Name]" value="{$row.Name}" data-rule="required" />
        </div>
    </div>
   
    
    <div class="form-group hidden layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('確認')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>