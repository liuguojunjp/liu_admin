<form id="client_add-form" role="form" class="form-horizontal form-ajax"  data-toggle="validator" method="POST" action=""><!-- action="" 自提交 原来的url，这里是 'mastersetting/roommaster/add' role="form" 不能少，不然对应的js找不到这个form，无法绑定各种方法-->
    <div class="form-group">
        <label for="ClientNo" class="control-label col-xs-12 col-sm-2">{:__('顧客ID')}:</label> 
        <div class="col-xs-12 col-sm-8">
            <fieldset>
                <input type="text" class="form-control" id="ClientNo" name="row[ClientNo]" value="" data-rule="required;remote[clientmana/check_mul_pk]" autocomplete="off" readonly="readonly"/>  
            </fieldset>
        </div>
    </div>
    <div class="form-group">
        <label for="Name" class="control-label col-xs-12 col-sm-2">{:__('顧客名')}:</label> 
        <div class="col-xs-12 col-sm-8">
            <fieldset>
                <input type="text" class="form-control" id="Name" name="row[Name]" value="" data-rule="required;" autocomplete="off" />  
            </fieldset>
        </div>
    </div>

    <div class="form-group hidden layer-footer"><!-- layer-footer  配合着fast.api.open  -->
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled">{:__('保存')}</button>
            <button type="reset" class="btn btn-default btn-embossed">{:__('リセット')}</button>
        </div>
    </div>
</form>