<!DOCTYPE html>
<html lang="{$config.language}">
    <head>
        {include file="common/meta" /}<!-- \admin\view\common\meta.php 内容 title，shortcuticon，加载CSS，构造客户端config对象等 -->
    </head>

    <body class="hold-transition skin-blue sidebar-mini fixed" id="tabs"><!-- 蓝色主题，如果要绿色的话 <body class="hold-transition skin-green sidebar-mini fixed" id="tabs">-->
        <div class="wrapper">
            
            <header id="header" class="main-header">
                {include file='common/header' /} <!-- \admin\view\common\header.php 顶部通栏-->
            </header>

            <aside class="main-sidebar">
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <!--如果想始终显示子菜单,则给ul加上show-submenu类即可-->
                    <ul class="sidebar-menu">

                        {$menulist} <!-- ul的内容li， 菜单 treeview （php根据 auth_rule 表 生成）-->

                        <!--
                                                <li class="treeview">
                                                    <a href="javascript:;" addtabs="37" url="javascript:;">
                                                        <i class="fa fa-apple"></i> <span>テスト</span> 
                                                        <span class="pull-right-container"><i class="fa fa-angle-left"></i> </span></a>
                                                        <ul class="treeview-menu menu-open" style="display: block;">
                                                            
                                                            <li class=""><a href="/admin/test/ajax" addtabs="38" url="/admin/test/ajax"><i class="fa fa-apple"></i> <span>AJAXテスト</span> <span class="pull-right-container"> </span></a> </li>
                                                            <li class=""><a href="/admin/test/jquery" addtabs="39" url="/admin/test/jquery"><i class="fa fa-apple"></i> <span>jqueryテスト</span> <span class="pull-right-container"> </span></a> </li>
                                                            <li class=""><a href="/admin/test/view" addtabs="40" url="/admin/test/view"><i class="fa fa-apple"></i> <span>画面テスト</span> <span class="pull-right-container"> </span></a> </li>
                                                            <li class=""><a href="/admin/test/bootstraptableedit" addtabs="47" url="/admin/test/bootstraptableedit"><i class="fa fa-apple"></i> <span>bootstraptableeditテスト</span> <span class="pull-right-container"> </span></a> </li>
                                                            <li class=""><a href="/admin/test/bootstrapedit" addtabs="48" url="/admin/test/bootstrapedit"><i class="fa fa-apple"></i> <span>bootstrapedititテスト</span> <span class="pull-right-container"> </span></a> </li>
                                                            <li class=""><a href="/admin/test/fullcalendar" addtabs="49" url="/admin/test/fullcalendar"><i class="fa fa-apple"></i> <span>fullcalendarテスト</span> <span class="pull-right-container"> </span></a> </li>
                                                            <li class=""><a href="/admin/test/bootstraptableadddel" addtabs="50" url="/admin/test/bootstraptableadddel"><i class="fa fa-apple"></i> <span>bootstraptableadddelテスト</span> <span class="pull-right-container"> </span></a> </li>
                                                            <li class=""><a href="/admin/testlever" addtabs="51" url="/admin/testlever"><i class="fa fa-apple"></i> <span>テストlever</span> <span class="pull-right-container"> </span></a> </li>
                                                        </ul>
                                                </li>-->


                        <!--
                                                <li class="treeview">
                                                    <a href="#">
                                                        <i class="fa fa-share"></i> <span>Multilevel</span>
                                                        <span class="pull-right-container">
                                                            <i class="fa fa-angle-left pull-right"></i>
                                                        </span>
                                                    </a>
                                                    <ul class="treeview-menu" style="display: block;">
                                                        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                                                        <li class="treeview">
                                                            <a href="#"><i class="fa fa-circle-o"></i> Level One
                                                                <span class="pull-right-container">
                                                                    <i class="fa fa-angle-left pull-right"></i>
                                                                </span>
                                                            </a>
                                                            <ul class="treeview-menu" style="display: block;">
                                                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                                                                <li class="treeview">
                                                                    <a href="#"><i class="fa fa-circle-o"></i> Level Two
                                                                        <span class="pull-right-container">
                                                                            <i class="fa fa-angle-left pull-right"></i>
                                                                        </span>
                                                                    </a>
                                                                    <ul class="treeview-menu" style="display: block;">
                                                                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                                                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                                                    </ul>
                                                </li>
                        -->


                        <li class="header">{:__('リンク')}</li>
                        <li><a href="http://www.navc2.co.jp/" target="_blank"><i class="fa fa-list text-red"></i> <span>{:__('ナバックについて')}</span></a></li>
                        <li><a href="http://www.hanabusa-s.com/" target="_blank"><i class="fa fa-comment text-yellow"></i> <span>{:__('問題ある場合')}</span></a></li>
                    </ul>
                </section>
            </aside>
            <div class="content-wrapper tab-content tab-addtabs"> <!-- Content Wrapper. Contains page content -->

            </div>
            <!-- /.content-wrapper -->
            <div class="control-sidebar-bg"></div> <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.9    </div>
                <strong>Copyright © 2018-2019 <a href="http://http://www.navc2.co.jp/" target="_blank">NAVC</a>.</strong> All Right Reserved
            </footer>
            {include file="common/control"  /}<!-- \admin\view\common\control.php 右边的control-sidebar-->
        </div><!-- end ./wrapper -->
        
        <!-- end main content -->
        {include file="common/script" /}<!-- \admin\view\common\script.php 在这里加载 JAVASCRIPT (用require.js)-->
    </body>
</html>