<!DOCTYPE html>
<html lang="en">
    <head>
        {/*include file="common/meta" /*/}
        <meta charset="utf-8">
        <title>{$title|default=''}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="renderer" content="webkit">
        <link rel="shortcut icon" href="__CDN__/assets/img/72561328.png" />
        <!-- Loading Bootstrap -->
        <link href="__CDN__/assets/css/login.css?v={$Think.config.site.version}" rel="stylesheet"><!-- __CDN__ 是config.php 的 //全局配置view_replace_str 中指定的模板替换变量-->
        <script type="text/javascript">
            var require = {config: {$config | json_encode}};//  左边的代码 就是php的  echo json_encode($config ); 返回JSON 编码 字符串
            //Object Literal 对象字面量（也叫对象初始化器 ,在Javascript中，Object Literal是动态的，你可在任何时候用{和}创建一个典型的对象(object)
            //alert(require.config.site.name);
        </script>
        <style type="text/css">
            body {
                color:#999;
                background:url('/assets/img/login-bg.jpg');
                background-size:cover;
            }
            a {
                color:#3498DB;
            }
            .login-panel{margin-top:150px;}
            .login-screen {
                max-width:400px;
                padding:0;
                margin:100px auto 0 auto;

            }
            .login-screen .well {
                border-radius: 3px;
                -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                background: rgba(255,255,255, 0.2);
            }
            .login-screen .copyright {
                text-align: center;
            }
            @media(max-width:767px) {
                .login-screen {
                    padding:0 20px;
                }
            }
            .profile-img-card {
                width: 100px;
                height: 100px;
                margin: 10px auto;
                display: block;
                -moz-border-radius: 50%;
                -webkit-border-radius: 50%;
                border-radius: 50%;
            }
            .profile-name-card {
                text-align: center;
            }

            #login-form {
                margin-top:20px;
            }
            #login-form .input-group {
                margin-bottom:15px;
            }
        </style>
    </head>
    <body>
         <div class="container">           
            <div class="login-wrapper">
                <div class="login-screen">
                    <div class="login-logo">
<!-- <a href=""><img src="/assets/img/YADO.png" width="80%"></a> -->
                        <a href="">{$hotel_name}のロゴ</a>
                    </div>
                    <div class="well">
                        <div class="login-form">
                            <img id="profile-img" class="profile-img-card" src="__CDN__/assets/img/avatar.png" />
                            <p id="profile-name" class="profile-name-card"></p>                           
                            <form action="" method="post" id="login-form">
                                <div id="errtips" class="hide"></div><!-- 这个div是用来吸收nicevalidator的错误信息，然后隐藏的class="hide"，（因为想用toast不想用缺省错误提示） -->
                                {:token()}<!--  调用框架助手函数自动生成token -->
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                                    <input type="text" class="form-control" id="pd-form-username" placeholder="{:__('担当者名')}" name="username" autocomplete="off" value="" data-rule="{:__('担当者名')}:required;username" />
                                </div>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></div>
                                    <input type="password" class="form-control" id="pd-form-password" placeholder="{:__('パスワード')}" name="password" autocomplete="off" value="" data-rule="{:__('パスワード')}:required;password" />
                                </div>
                                <div class="form-group layer-footer">
                                    <button type="submit" id="submitBtn" class="btn btn-success btn-lg btn-block">{:__('ログイン')}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <form action="{:url('index/conn_clear')}" method="post" id="back-form">
<!--                        <div class="form-group">
                            <label class="inline" for="keeplogin">-->
                                <!--
                                <input type="checkbox" name="keeplogin" id="keeplogin" value="1" />{:__('キープログイン')}
                                -->
                                <button type="submit" class="btn btn-link btn-lg btn-block">{:__('＜＜接続クリアして戻る')}</button>
<!--                            </label>
                        </div>-->
                        {:build_hidden('user_id', $user_id )}
                    </form>
<!--                    <p class="copyright"><a target="_blank" href="http://www.navc2.co.jp/">Powered By NAVC</a></p>-->
                    <p class="copyright"><strong>Copyright &copy; 2018 <a target="_blank" href="http://www.navc2.co.jp/">NAVC</a>.</strong> All rights reserved.</p>
                </div>
            </div>
        </div>
        {/* include file="common/script"   /*/}
       <script src="__CDN__/assets/js/require.js" data-main="__CDN__/assets/js/require-backend.js?v={$site.version}"></script> 
       <!--  <script src="__CDN__/assets/js/require.js" data-main="__CDN__/assets/js/require-backend.js?v={$site.version}"></script>-->
    </body>
</html>