<meta charset="utf-8">
<title>{$title|default=''}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"><!-- (设置屏幕密度为高频，中频，低频自动缩放，禁止用户手动调整缩放) -->
<meta name="renderer" content="webkit">
<link rel="shortcut icon" href="__CDN__/assets/img/72561328.png" />
 <!--<link href="__CDN__/assets/css/backend{$Think.config.app_debug?'':'.min'}.css?v={$Think.config.site.version}" rel="stylesheet">-->
<link href="/assets/css/backend.css?v={$Think.config.site.version}" rel="stylesheet"><!-- 加载(import)了很多css -->
<script type="text/javascript">
    var require = {
        config:  {$config|json_encode}
    };
    //上記意味  config: <?php //echo json_encode($config ); ?> 定义javascript对象 config，下面会频繁用到这个对象  
</script>