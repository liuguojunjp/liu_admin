<div class="panel panel-default panel-intro">
    {:build_heading()}<!-- admin/common.php  生成页面Heading-->
    <div class="panel-body">
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="one">
                <div class="widget-body no-padding">
                    <div id="toolbar" class="toolbar">
                      <!--  {:build_toolbar('refresh,delete')}-->
                        {:build_toolbar('refresh')}
                    </div>
                    <table id="table_usehis_list" class="table table-striped table-bordered table-hover" 
                           width="100%">
                        <!-- data-operate-del="{:$auth->check('auth/adminlog/del')}" -->
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>