<div class="panel panel-default panel-intro">
    {:build_heading()}
</div>
<div class="panel-body">
    <div class="box box-success">
        <div class="panel-body">
            <form id="hotel_update-form" role="form" data-toggle="validator" method="POST" action="{:url('/admin/othermaintenance/hoteldateupdate/update')}">  
                {:token()}
                <div class="box-body box-profile">                
                    <h3 class="profile-username text-center"> {:__('現在のホテル日:')}{$cur_hoteldate}</h3>
                    <input type="hidden" id="cur_hoteldate" name="row[cur_hoteldate]" value="{$cur_hoteldate}" />

                    <div class="form-group">
                        <label for="new_hoteldate" class="control-label">{:__('新ホテル日')}:</label>
                        <input type="datetime"  class="form-control datetimepicker" id="cur_hoteldate" name="row[new_hoteldate]" value="{$cur_hoteldate}" 
                               data-rule="required;date" autocomplete="off"/> 
                    </div>
                    <div class="form-group">
                        <label for="cur_password" class="control-label">{:__('現在パスワード')}:</label>
                        <input type="password" class="form-control" placeholder="{:__('現在のパスワードを入力必要')}" name="row[cur_password]" value="" data-rule="required;password"/>
                    </div>
                    <div class="form-group">
                        <label for="new_password" class="control-label">{:__('新パスワード')}:</label>
                        <input type="password" class="form-control" placeholder="{:__('変更しない場合空白そのまま')}" name="row[new_password]" value="" data-rule="password"/>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">{:__('確認')}</button>
                        <button type="reset" class="btn btn-default">{:__('リセット')}</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
