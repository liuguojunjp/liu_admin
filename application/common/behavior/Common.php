<?php
//  liu 20180104 加载代码 ↓
//application/tags.php→ module_init→'app\\common\\behavior\\Common',設定
//// think/App.php 
//        Hook::listen('module_init', $request);//监听module_init
//

namespace app\common\behavior;

use think\Config;

class Common//本类是在tag.php绑定好的行为定义类，
//绑定 去tag.php　看即可明白，
// 行为标签的位置，这里不是自定义的，查手册可知这是 thinkphp系统定义的mudule_init (模块初始化标签位)` 
 {
    public function run(&$request) {//单个行为定义必须实现run方法，注意参数 有 &
        $isajax = $request->isAjax(); //或者 Request::instance()->isAjax()
        !defined('IS_AJAX') && define('IS_AJAX', $isajax); // 定义是否AJAX请求， 太常用 20180802 把他从backend 搬到了这里

        mb_internal_encoding("UTF-8"); // 设置mbstring字符编码
        // 不是太懂应该是多服务器的设置吧 ↓
        $sdfd = $request->root(); // 获取URL访问的ROOT地址

        $url = preg_replace("/\/(\w+)\.php$/i", '', $request->root()); // 如果修改了index.php入口地址，则需要手动修改cdnurl的值

        if (!Config::get('view_replace_str.__CDN__')) {// 如果未设置__CDN__则自动匹配得出
            Config::set('view_replace_str.__CDN__', $url);
        }
        // 如果未设置__PUBLIC__则自动匹配得出
        if (!Config::get('view_replace_str.__PUBLIC__')) {
            Config::set('view_replace_str.__PUBLIC__', $url . '/');
        }
        // 如果未设置__ROOT__则自动匹配得出
        if (!Config::get('view_replace_str.__ROOT__')) {
            Config::set('view_replace_str.__ROOT__', preg_replace("/\/public\/$/", '', $url . '/'));
        }
        // 如果未设置cdnurl则自动匹配得出
        if (!Config::get('site.cdnurl')) {
            Config::set('site.cdnurl', $url);
        }
        // 如果未设置cdnurl则自动匹配得出
        if (!Config::get('upload.cdnurl')) {
            Config::set('upload.cdnurl', $url);
        }
        // 不是太懂应该是多服务器的设置吧 ↑

        if (Config::get('app_debug')) {
            Config::set('site.version', time()); // 如果是调试模式将version置为当前的时间戳可避免（javascript）缓存　
            Config::set('exception_tmpl', THINK_PATH . 'tpl' . DS . 'think_exception.tpl'); // 如果是开发模式那么将异常模板修改成think官方的
        }
        if (Config::get('app_trace') && $request->isAjax()) {
            Config::set('app_trace', false); // 如果是trace模式且Ajax的情况下关闭trace
        }
    }

}
