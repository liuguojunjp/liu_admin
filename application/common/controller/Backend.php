<?php

namespace app\common\controller;

use app\admin\library\Auth;
use think\Config;
use think\Controller;
use think\Hook;
use think\Lang;
use think\Session;
use think\Db;
use app\admin\model\envdata\EnvData;


// (Trait 是php为了减少单继承语言的限制而准备的代码复用机制)
//在这个例子中 Backend 是继承thinkphp的controler的，如果有一段代码需要复用，又不好写入基类，就用traits
//当然 如果不复用，直接就写在 Backend吧
/**
 * 后台控制器基类
 */
class Backend extends Controller// 继承自thinkphp的Controller类，这里是后台基类，包含着，增删改，权限认证等大量的代码共用
{

    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];

    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $noNeedRight = [];

    /**
     * 布局模板
     * @var string
     */
    protected $layout = 'default';

    /**
     * 权限控制类
     * @var Auth
     */
    protected $auth = null;

    /**
     * 快速搜索时执行查找的字段
     */
    protected $searchFields = 'id';
    protected $relationSearch = false;//* 是否是关联查询* 是否是关联查询

    /**
     * 是否开启数据限制
     * 支持auth/personal
     * 表示按权限判断/仅限个人 
     * 默认为禁用,若启用请务必保证表中存在admin_id字段
     */
    protected $dataLimit = false;
    protected $dataLimitField = 'admin_id';//数据限制字段
    protected $modelValidate = false;//是否开启Validate验证
    protected $modelSceneValidate = false;//* 是否开启模型场景验证

    /**
     * Multi方法可批量修改的字段
     */
    protected $multiFields = 'status';
    
    protected $user_id = 0;

   use \app\admin\library\traits\BackendTraits;//引入后台控制器的traits, 注意，这里虽然用了 use关键字，但是他用的是traits，是代码复用，而不是一般的类引用， 可别放到类的外边去了
   //当然，如果不复用的话，把BackendTraits里头的代码插入到此处就可以了...

   
   public function checkConn($controllername) {
        if (IS_AJAX || IS_DIALOG || $controllername == 'ajax') {
            return;
        }//只要不是ajax，对话框，和向客户端传字典的时候，就检查连接有效性
        $user_id = Session::get('user_id'); //从Session取得客户号
        $conn_tocken = Session::get('conn_tocken'); //取得连接tocken
        $is_conn_OK = true;
        $err_msg_conn = "";
        if (!isset($user_id) || !isset($conn_tocken) || sizeof($conn_tocken) == 0) {
            //$err_msg_conn = "接続を取得していないようです。";
            $err_msg_conn = "セッションの期限が切れました。";
            $is_conn_OK = false;
        } else if (!isset($user_id) || !is_numeric($user_id)) {
            $err_msg_conn = "利用者番号異常。";
            $is_conn_OK = false;
        } 
        
//        else { //20190415 del
//            $reflect = new \ReflectionClass('app\admin\model\user_conn\User' . $user_id);
//            $conn_obj=$reflect->newInstance()->get_obj_by_conn_tocken($conn_tocken);
//            if (!$conn_obj) {//用isset也可以吧
//                $err_msg_conn = "同時接続制限を超えています";
//                $is_conn_OK = false;
//            }
//        }
        
        if ($is_conn_OK !== true) {
            $err_msg_conn .= '再度接続を取得してください。';
           if ($this->auth->isLogin()) {//session已经有[admin]值
               
            } else {
                $this->error(__($err_msg_conn), '/user/' . $user_id);
            }
        }
    }

    public function _initialize()
    {
        !defined('IS_ADDTABS') && define('IS_ADDTABS', input("addtabs") ? TRUE : FALSE); // 定义是否Addtabs请求
        !defined('IS_DIALOG') && define('IS_DIALOG', input("dialog") ? TRUE : FALSE);// 定义是否Dialog请求
        
        $this->auth = Auth::instance();//权限控制类 生成

        $modulename = $this->request->module();// 取得模块名（要传给客户端）
        $controllername = strtolower($this->request->controller());// 取得控制器名（要传给客户端来自动load对应的js文件等，
        //注意对于一些控制器它的目录结构是有路径的，比如说mastersetting.agentbranchmaster有一些是没有路径的，比如说 dayilyupdate）
        $actionname = strtolower($this->request->action());// 取得动作名（要传给客户端）

        $this->checkConn($controllername);
        
        EnvData::ini();
        
        $this->user_id=Session::get('user_id');
        
        $path = str_replace('.', '/', $controllername) . '/' . $actionname;
        $this->auth->setRequestUri($path); // 设置当前请求的URI
        if (!$this->auth->match($this->noNeedLogin)) // 检测是否需要验证登录
        {
            if (!$this->auth->isLogin())//检测是否登录
            {
                Hook::listen('admin_nologin', $this);
                $url = Session::get('referer');//上一个页面的url
                $url = $url ? $url : $this->request->url();
                $this->error(__('Please login first'), url('index/login', ['url' => $url]));
            }
            if (!$this->auth->match($this->noNeedRight))// 判断是否需要验证权限
            {   
                if (!$this->auth->check($path))// 判断控制器和方法判断是否有对应权限
                {
                    Hook::listen('admin_nopermission', $this);
                    $this->error(__('You have no permission'), '');
                }
            }
        }

        // 非选项卡时重定向
        if (!$this->request->isPost() && !IS_AJAX && !IS_ADDTABS && !IS_DIALOG && input("ref") == 'addtabs')
        {
            $url = preg_replace_callback("/([\?|&]+)ref=addtabs(&?)/i", function($matches) {
                return $matches[2] == '&' ? $matches[1] : '';
            }, $this->request->url());
            $this->redirect('index/index', [], 302, ['referer' => $url]);
            exit;
        }
        
        
        // 设置面包屑导航数据
        $breadcrumb = $this->auth->getBreadCrumb($path);//面包屑导航的作用是告诉访问者他们目前在网站中的位置以及如何返回。
        array_pop($breadcrumb);
        $this->view->breadcrumb = $breadcrumb;

      
        
        //if (!IS_AJAX&&$this->layout)
        if ($this->layout)// 如果有使用模板布局  //比如说 Ajax.php 里头就有protected $layout = '';//不加载模板！
        {
            $this->view->engine->layout('layout/' . $this->layout);// view/layout/default (.php) 那里可是有 {include file="common/script" /} 这一句的！不然框架页面里头可加载不了script
        }

        $lang = strip_tags(Lang::detect()); // 语言检测 //strip_tags() 函数剥去字符串中的 HTML、XML 以及 PHP 的标签

        $site = Config::get("site");//site是extra额外配置文件site的内容，并不是config.php定义的，think的 Config::get里头有动态载入额外配置的代码

        $upload = \app\common\model\Config::upload();//本地上传配置信息

        
        Hook::listen("upload_config_init", $upload);// 上传信息配置后 的钩子

        // 配置信息
        $config = [
            'site'           => array_intersect_key($site, array_flip(['name', 'cdnurl', 'version', 'timezone', 'languages'])),
            ////array_flip 反转 Array ( [name] => 0 [cdnurl] => 1 [version] => 2 [timezone] => 3 [languages] => 4 ) 
            //array_intersect_key 取交集
            'upload'         => $upload,
            'modulename'     => $modulename,//告诉客户端 模块名
            'controllername' => $controllername,//告诉客户端 控制器
            'actionname'     => $actionname,//告诉客户端 动作名
            'jsname'         => 'backend/' . str_replace('.', '/', $controllername),//用来指示浏览器加载 backend文件夹（相对于requireJS的相对路径）的$controllername名的js，如加载backend\index.js
            'moduleurl'      => rtrim(url("/{$modulename}", '', false), '/'),
            'language'       => $lang
//          ,'liuadmin'      => Config::get('liuadmin'),
            ,'referer'        => Session::get("referer")//当前选项卡，重新加载的时候会自动加载这个选项卡
        ];
            
        Config::set('upload', array_merge(Config::get('upload'), $upload));
        
        Hook::listen("config_init", $config);// 配置信息后
      
        $this->loadlang($controllername);  //加载当前控制器语言包   lang/语言/控制器名.php
       
        $this->assign('site', $site); //渲染站点配置
        $this->assign('config', $config);//渲染配置信息
        $this->assign('auth', $this->auth);//渲染权限对象
        $this->assign('admin', Session::get('admin'));//渲染管理员对象
    }

    /**
     * 加载语言文件
     * @param string $name
     */
    protected function loadlang($name)//默认只加载了控制器对应的语言名，你还可以根据控制器名来加载额外的语言包
    {
        Lang::load(APP_PATH . $this->request->module() . '/lang/' . Lang::detect() . '/' . str_replace('.', '/', $name) . '.php');
    }

    /**
     * 渲染配置信息
     * @param mixed $name 键名或数组
     * @param mixed $value 值 
     */
    protected function assignconfig($name, $value = '')
    {
        $this->view->config = array_merge($this->view->config ? $this->view->config : [], is_array($name) ? $name : [$name => $value]);
    }

    /**
     * 生成查询所需要的条件,排序方式
     * @param mixed $searchfields 快速查询的字段
     * @param boolean $relationSearch 是否关联查询
     * @return array
     */
    protected function buildparams($searchfields = null, $relationSearch = null)
    {
        $searchfields = is_null($searchfields) ? $this->searchFields : $searchfields;
        $relationSearch = is_null($relationSearch) ? $this->relationSearch : $relationSearch;
        $search = $this->request->get("search", '');
        $filter = $this->request->get("filter", '');//得到filter的json字符串
        $op = $this->request->get("op", '', 'trim');
        $sort = $this->request->get("sort", "id");
        $order = $this->request->get("order", "DESC");
        $offset = $this->request->get("offset", 0);
        $limit = $this->request->get("limit", 0);
        $filter = json_decode($filter, TRUE);//得到filter的php变量
        $op = json_decode($op, TRUE);//得到op的php变量
        $filter = $filter ? $filter : [];
        $where = [];
        $tableName = '';
        if ($relationSearch)
        {
            if (!empty($this->model))
            {
                $class = get_class($this->model);
                $name = basename(str_replace('\\', '/', $class));
                $tableName = $this->model->getQuery()->getTable($name) . ".";
            }
            $sort = stripos($sort, ".") === false ? $tableName . $sort : $sort;
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds))
        {
            $where[] = [$this->dataLimitField, 'in', $adminIds];
        }
        if ($search)
        {
            $searcharr = is_array($searchfields) ? $searchfields : explode(',', $searchfields);
            foreach ($searcharr as $k => &$v)
            {
                $v = stripos($v, ".") === false ? $tableName . $v : $v;
            }
            unset($v);
            $where[] = [implode("|", $searcharr), "LIKE", "%{$search}%"];
        }
        foreach ($filter as $k => $v)
        {
            $sym = isset($op[$k]) ? $op[$k] : '=';
            if (stripos($k, ".") === false)
            {
                $k = $tableName . $k;
            }
            $sym = strtoupper(isset($op[$k]) ? $op[$k] : $sym);
            switch ($sym)
            {
                case '=':
                case '!=':
                    $where[] = [$k, $sym, (string) $v];
                    break;
                case 'LIKE':
                case 'NOT LIKE':
                case 'LIKE %...%':
                case 'NOT LIKE %...%':
                    $where[] = [$k, trim(str_replace('%...%', '', $sym)), "%{$v}%"];
                    break;
                case '>':
                case '>=':
                case '<':
                case '<=':
                    $where[] = [$k, $sym, intval($v)];
                    break;
                case 'IN':
                case 'IN(...)':
                case 'NOT IN':
                case 'NOT IN(...)':
                    $where[] = [$k, str_replace('(...)', '', $sym), explode(',', $v)];
                    break;
                case 'BETWEEN':
                case 'NOT BETWEEN':
                    $arr = array_slice(explode(',', $v), 0, 2);
                    if (stripos($v, ',') === false || !array_filter($arr))
                        continue;
                    //当出现一边为空时改变操作符
                    if ($arr[0] === '')
                    {
                        $sym = $sym == 'BETWEEN' ? '<=' : '>';
                        $arr = $arr[1];
                    }
                    else if ($arr[1] === '')
                    {
                        $sym = $sym == 'BETWEEN' ? '>=' : '<';
                        $arr = $arr[0];
                    }
                    $where[] = [$k, $sym, $arr];
                    break;
                case 'RANGE':
                case 'NOT RANGE':
                    $v = str_replace(' - ', ',', $v);
                    $arr = array_slice(explode(',', $v), 0, 2);
                    if (stripos($v, ',') === false || !array_filter($arr))
                        continue;
                    //当出现一边为空时改变操作符
                    if ($arr[0] === '')
                    {
                        $sym = $sym == 'RANGE' ? '<=' : '>';
                        $arr = $arr[1];
                    }
                    else if ($arr[1] === '')
                    {
                        $sym = $sym == 'RANGE' ? '>=' : '<';
                        $arr = $arr[0];
                    }
                    $where[] = [$k, str_replace('RANGE', 'BETWEEN', $sym) . ' time', $arr];
                    break;
                case 'LIKE':
                case 'LIKE %...%':
                    $where[] = [$k, 'LIKE', "%{$v}%"];
                    break;
                case 'NULL':
                case 'IS NULL':
                case 'NOT NULL':
                case 'IS NOT NULL':
                    $where[] = [$k, strtolower(str_replace('IS ', '', $sym))];
                    break;
                default:
                    break;
            }
        }
        
        // liu 20181221 改造↓
        
//          $where = function($query) use ($where) {
//            foreach ($where as $k => $v)
//            {
//                if (is_array($v))
//                {
//                    call_user_func_array([$query, 'where'], $v);
//                }
//                else
//                {
//                    $query->where($v);
//                }
//            }
//        };
        
        $where_result = array();
        foreach ($where as $v) {
            $where_result[$v[0]] = array($v[1], $v[2]);
        }
         // liu 20181221 改造↑
        
        return [$where_result, $sort, $order, $offset, $limit, $where];
    }

    /**
     * 获取数据限制的管理员ID
     * 禁用数据限制时返回的是null
     * @return mixed
     */
    protected function getDataLimitAdminIds()
    {
        if (!$this->dataLimit)
        {
            return null;
        }
        $adminIds = [];
        if (in_array($this->dataLimit, ['auth', 'personal']))
        {
            $adminIds = $this->dataLimit == 'auth' ? $this->auth->getChildrenAdminIds(true) : [$this->auth->id];
        }
        return $adminIds;
    }

    /**
     * Selectpage的实现方法
     * 
     * 当前方法只是一个比较通用的搜索匹配,请按需重载此方法来编写自己的搜索逻辑,$where按自己的需求写即可
     * 这里示例了所有的参数，所以比较复杂，实现上自己实现只需简单的几行即可
     * 
     */
    protected function selectpage() {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'htmlspecialchars']);

        //搜索关键词,客户端输入以空格分开,这里接收为数组
        $word = (array) $this->request->request("q_word/a");
        //当前页
        $page = $this->request->request("page");
        //分页大小
        $pagesize = $this->request->request("per_page");
        //搜索条件
        $andor = $this->request->request("and_or");
        //排序方式
        $orderby = (array) $this->request->request("order_by/a");
        //显示的字段
        $field = $this->request->request("field");
        //主键
        $primarykey = $this->request->request("pkey_name");
        //主键值
        $primaryvalue = $this->request->request("pkey_value");
        //搜索字段
        $searchfield = (array) $this->request->request("search_field/a");
        //自定义搜索条件
        $custom = (array) $this->request->request("custom/a");
        $order = [];
        foreach ($orderby as $k => $v) {
            $order[$v[0]] = $v[1];
        }
        $field = $field ? $field : 'name';

        //如果有primaryvalue,说明当前是初始化传值
        if ($primaryvalue !== null) {
            $where = [$primarykey => ['in', $primaryvalue]];
        } else {
            $where = function($query) use($word, $andor, $field, $searchfield, $custom) {
                foreach ($word as $k => $v) {
                    foreach ($searchfield as $m => $n) {
                        $query->where($n, "like", "%{$v}%", $andor);
                    }
                }
                if ($custom && is_array($custom)) {
                    foreach ($custom as $k => $v) {
                        $query->where($k, '=', $v);
                    }
                }
            };
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            $this->model->where($this->dataLimitField, 'in', $adminIds);
        }
        $list = [];
        $total = $this->model->where($where)->count();
        if ($total > 0) {
            if (is_array($adminIds)) {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($where)
                    ->order($order)
                    ->page($page, $pagesize)
                    ->field("{$primarykey},{$field}")
                    ->field("password,salt", true)
                    ->select();
        }
        
        return json(['list' => $list, 'total' => $total]);//这里一定要返回有list这个字段,total是可选的,如果total<=list的数量,则会隐藏分页按钮
    }

}
