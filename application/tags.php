<?php

$df=0;

 // 应用行为扩展定义文件
return [
    // 应用初始化
    'app_init'     => [],
    // 应用开始
    'app_begin'    => [],
    
    'module_init'  => [
        'app\\common\\behavior\\Common',// 模块初始化的时候自动运行 common\\behavior\\Common.php 的行为，(默认 run)
    ],
    // 模块初始化
//    'addons_init'  => [
//        'app\\common\\behavior\\Common',
//    ],
    // 操作开始执行
    'action_begin' => [],
    // 视图内容过滤
    'view_filter'  => [],
    // 日志写入
    'log_write'    => [],
    // 应用结束
    'app_end'      => [],
];
