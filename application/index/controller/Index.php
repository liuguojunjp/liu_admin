<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Validate;
use think\Session;
class Index extends Frontend//Index 控制器
{
    protected $layout = '';
    public function _initialize()
    {
       // parent::_initialize();
    }

    public function index($id = '0')//index方法， 最开始的界面，简单的选用户而已，
    {
//        $this->assign('user_id', $id);
        //对应的view ---- application\index\view\index\index.php↓
       // return $this->fetch();//视图实例化写法1
       // return $this->view->fetch();//视图实例化写法2
        return view();//视图实例化写法3，直接使用view助手函数进行模板操作， 最省心的写法，尤其是没有继承系统的控制器基类的情况下用
    }
    
    public function user($id = '0')//方法， 某个用户的接続画面 ，id 用户号
    {
        $err_msg = '';
        $hotel_name = "";
        $user_id = $id;
        if (!is_numeric($user_id) || ((int) $id) <= 0) {//用户号从1开始
            $err_msg = 'URLを確認ください';
        } else {
            $hotel_name .= 'ホテルユーザ'.$user_id; //とりあえず
        }
        $conn_ok=false;
        if ($this->request->isPost())//提交(用户按下「接続」后)的处理代码↓
        {
            $user_id=$this->request->post('user_id');
            $password = $this->request->post('password');
            $token = $this->request->post('__token__');
            
            // ---- 输入的服务器端check ↓
            $rule = [
                'password'  => 'require|length:3,30',
                '__token__' => 'token',//tocken验证
            ];
            $data = [
                'password'  => $password,
                '__token__' => $token,
            ];
            $validate = new Validate($rule);
            $v_result = $validate->check($data);
            if (!$v_result) {
                $err_msg = $validate->getError();//这里会返回thinkphp的默认错误信息，具体看thinkphp的Validate类。  
                //$this->error($validate->getError(), $url, ['token' => $this->request->token()]);
            } 
            // ---- 输入的服务器端check ↑
            else {
                \app\admin\model\AdminLog::setTitle(__('UserConn'));//内部log的标题
                $reflect = new \ReflectionClass('app\admin\model\user_conn\User' . $user_id);//用反射的方式找到对应该用户的model
                $conn_mana_model=$reflect->newInstance();//用反射的方式找到对应该用户的model
                
                $mana_obj =$conn_mana_model->get_mana_obj();//用model取出管理行对象(model对象)
                if ($mana_obj && $mana_obj->UserPassword === $password) {
                   $conn_ok=true;
                   $last_conn= $conn_mana_model->get_last_conn();//用model取出上次登录的信息（model对象）
                    if ($last_conn) {//一般会有的
                        $last_conn->save_conn_detail(//用model保留登录信息
                                $token
                                ,\request()->server('HTTP_USER_AGENT')//获取用户浏览器信息
                                ,\request()->ip()////获取用户ip信息
                                );
                    }
                } else {
                    $err_msg = "パスワードを確認ください";
                }
            }
            if ($conn_ok) {
                Session::set('user_id', $user_id); //客户id放入 session
                Session::set('conn_tocken', $token); //$token 也放入session
                $this->success(__('接続取得しました、担当者ログインしてください。'), '/admin/index/login',null,1);//这里开始转入后台（index/login）   //成功后转入担当者登录画面 
                
                //, [ 'user_id' => $user_id, 'conn_tocken' => $token]
                //$this->redirect('/admin/index/login'); ·//直接重定向 ,不读秒
            }
        }//提交(用户按下「接続」后)的处理代码↑
        return view('', ['user_id' => $user_id , 'hotel_name' => $hotel_name, 'err_msg' => $err_msg ]);//上面的代码可以简化成， 使用助手函数批量赋值模板变量 //对应的view ---- application\index\view\index\user.php    
    }
}
