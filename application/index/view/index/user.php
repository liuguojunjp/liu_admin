<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{$title|default=''}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="renderer" content="webkit">
        <link rel="shortcut icon" href="__CDN__/assets/img/72561328.png" />
        <link href="__CDN__/assets/css/login.css?v={$Think.config.site.version}" rel="stylesheet"><!-- __CDN__ 是config.php 的 //全局配置view_replace_str 中指定的模板替换变量-->
        <style type="text/css">
            body {
                color:#999;
                background:url('/assets/img/bg-middle.jpg');/* 背景图像 */
                background-size:cover;
            }
            a {
                color:#3498DB;
            }
            .login-panel{margin-top:150px;}
            .login-screen {
                max-width:400px;
                padding:0;
                margin:100px auto 0 auto;
            }
            .login-screen .well {
                border-radius: 3px;
                -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                background: rgba(255,255,255, 0.2);
            }
            .login-screen .copyright {
                text-align: center;
            }
            @media(max-width:767px) {
                .login-screen {
                    padding:0 20px;
                }
            }
            .profile-img-card {
                width: 100px;
                height: 100px;
                margin: 10px auto;
                display: block;
                -moz-border-radius: 50%;
                -webkit-border-radius: 50%;
                border-radius: 50%;
            }
            .profile-name-card {
                text-align: center;
            }

            #login-form {
                margin-top:20px;
            }
            #login-form .input-group {
                margin-bottom:15px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <!--                <div class="navbar-header">                    
                                <a class="navbar-brand page-scroll" href="#page-top"><img src="__CDN__/assets/img/72304030.png" style="width:100px;" alt=""></a>
                            </div>-->
            <br><br>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="header-content">
                            <div class="header-content-inner">
                                <div class="login-logo">
                                    <h1>LiuAdminご利用ありがとうございいます</h1>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-wrapper">
                <div class="login-screen">
                    <div class="lockscreen-logo">
                        <a href="">{$hotel_name}のロゴ</a><!-- 显示hotelname变量（模板语法） -->
                    </div>
                    <div class="well">
                        <div class="login-form">
                            <form action="" method="post" id="login-form">
                                <div id="errtips" class="hide"></div>
                                {:token()}<!-- 调用token（）函数生成token（模板语法） -->
                                {:build_hidden('user_id', $user_id )}<!-- 调用build_hidden（）函数生成hidden元素（模板语法） -->
                                <?php if (strlen($err_msg) > 0) { ?><!--php原生语法的例子↓ -->
                                    <div  align="center" style="color:#DD0000;font-size:21px" >
                                        <?php echo $err_msg; ?>	
                                    </div>	
                                    <?php
                                }
                                ?><!-- php原生语法的例子↑ -->
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></div>
                                    <input type="password" class="form-control" id="pd-form-password" placeholder="{:__('パスワード')}" name="password" autocomplete="off" value="" data-rule="{:__('パスワード')}:required;password" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-lg btn-block">{:__('接続')}<!--调用 __（）函数处理多语言 -->
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="help-block text-center">
                            {:__('パスワードを入力して、接続してください')}   <!--调用 __（）函数处理多语言 -->
                        </div>
                        <!--                        <div class="text-center">
                                                    <a href="/">＜＜戻る</a>
                                                </div>-->
                    </div>
                    <p class="copyright"><strong>Copyright &copy; 2018 <a target="_blank" href="http://www.navc2.co.jp/">NAVC</a></strong> <br>All rights reserved.</p>
                </div>
            </div>
        </div>

    </body>
</html>