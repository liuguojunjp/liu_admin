<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>ウェブアプリ勉強</title>
        <link rel="shortcut icon" href="/assets/img/72561328.png" />
        <!-- Bootstrap Core CSS -->
        <link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="__CDN__/assets/css/index.css" rel="stylesheet">
        <!-- Plugin CSS -->
        <link href="//cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="//cdn.bootcss.com/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="//cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
            <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="page-top">
      
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="header-content">
                            <div class="header-content-inner">
                                <h1>LiuAdmin</h1>

                                
                                <ul class="nav navbar-nav navbar-nav">
                                    <li><a href="/user/1" title="ホテル1詳細">ホテル1</a></li>
                                    <li><a href="/user/2" title="ホテル2詳細">ホテル2</a></li>
                                  
                                </ul>
                               
<!--                                <a href="{:url('admin/index/login')}" class="btn btn-outline btn-xl page-scroll">To Admin</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section id="features" class="features">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="section-heading">
                            <h2>function</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="feature-item">
                                        <i class="icon-user text-primary"></i>
                                        <h3>AAAAA</h3>
                                        <p class="text-muted">AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="feature-item">
                                        <i class="icon-screen-smartphone text-primary"></i>
                                        <h3>BBBBBBBBB</h3>
                                        <p class="text-muted">BBBBBBBBBBBBBBBBB</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="feature-item">
                                        <i class="icon-present text-primary"></i>
                                        <h3>CCCCCCC</h3>
                                        <p class="text-muted">CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="feature-item">
                                        <i class="icon-layers text-primary"></i>
                                        <h3>DDDDDD</h3>
                                        <p class="text-muted">DDDDDDDDDDDDDDDDDDD</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="feature-item">
                                        <i class="icon-docs text-primary"></i>
                                        <h3>EEE</h3>
                                        <p class="text-muted">EEEEEEEEEEEEEEEEEEEeeee</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="feature-item">
                                        <i class="icon-puzzle text-primary"></i>
                                        <h3>FFFFFFF</h3>
                                        <p class="text-muted">FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="container">
                <p>&copy; 2018 NAVC. All Rights Reserved.</p>
                <ul class="list-inline">
                    <li><a href="">AAA</a></li>
                    <li><a href="">BBB</a></li>
                    <li>
                        <a href="">CCC</a>
                    </li>
                </ul>
            </div>
        </footer>
    </body>
</html>