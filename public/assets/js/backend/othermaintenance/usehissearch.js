define(['jquery', 'bootstrap', 'backend', 'table'], function ($, undefined, Backend, Table) {
    var Controller = {
        index: function () {
            Table.api.init({// 初始化表格参数配置
                extend: {
                    index_url: 'othermaintenance/usehissearch/index',
                    table_function_title: __('利用変更履歴'),
                }
            });
            var table = $("#table_usehis_list");
            table.bootstrapTable({// 初始化表格
                sortName: 'ID',
                sortOrder: 'desc',
                pk: 'ID',
                url: $.fn.bootstrapTable.defaults.extend.index_url, //表格的参数定义在 jQuery.fn.bootstrapTable.defaults    Bootstrap-table 基础配置 , extend.index_url 其实就是上面定义的那个 'auth/adminlog/index'
                columns: [
                    [
                        {field: 'ID', title: __('利用履歴ID'), operate: 'BETWEEN', placeholder: '利用履歴ID範囲検索'}
                        ,{field: 'IsDel', title: __('削除'), operate: 'LIKE %...%', placeholder: '曖昧検索'}//PG必要！！
                        ,{field: 'UpdateDate', title: __('更新日時'), operate: 'LIKE %...%', placeholder: '曖昧検索'}//PG必要か
                        ,{field: 'UpdatePersonName', title: __('登録者'), operate: 'LIKE %...%', placeholder: '曖昧検索' }
                        ,{field: 'ResvNo', title: __('予約番号'), operate: 'LIKE %...%', placeholder: '曖昧検索' }
                        ,{field: 'UseStatus', title: __('状態'), operate: 'LIKE %...%', placeholder: '曖昧検索' 

                        }//PG必要！！
                        ,{field: 'Cindate', title: __('ﾁｪｯｸｲﾝ日'), operate: 'LIKE %...%', placeholder: '曖昧検索' 
                        }//PG必要か
                       
                        
                        ,{field: 'ClientName', title: __('顧客名'), operate: 'LIKE %...%', placeholder: '曖昧検索' 

                        }
                        ,{field: 'Stay', title: __('泊数'), operate: 'BETWEEN', placeholder: '範囲検索'

                        }
                        ,{field: 'Coutdate', title: __('ﾁｪｯｸｱｳﾄ日'), operate: 'LIKE %...%', placeholder: '曖昧検索' 

                        }//PG必要か
                        ,{field: 'ClientKana', title: __('顧客カナ'), operate: 'LIKE %...%', placeholder: '曖昧検索' 

                        }
                        ,{field: 'BaseItemName', title: __('基本科目'), operate: 'LIKE %...%', placeholder: '曖昧検索' 

                        }
                        ,{field: 'BasePrice', title: __('基本単価'), operate: 'BETWEEN', placeholder: '範囲検索' 

                        },{field: 'TotalSummary', title: __('合計金額'), operate: 'BETWEEN', placeholder: '範囲検索' 

                        }
                        ,{field: 'PaySummary', title: __('入金額'), operate: 'BETWEEN', placeholder: '範囲検索' 

                        }
                        ,{field: 'RoomCount', title: __('部屋数'), operate: 'BETWEEN', placeholder: '範囲検索' 

                        }
                        ,{field: 'PeopleCount', title: __('人数'), operate: 'BETWEEN', placeholder: '範囲検索' 

                        }
                        ,{field: 'Memo', title: __('メモ'), operate: 'LIKE %...%', placeholder: '曖昧検索' 

                        }
                        ,{field: 'MachineName', title: __('端末名'), operate: 'LIKE %...%', placeholder: '曖昧検索' 

                        },{field: 'ViaView', title: __('画面'), operate: 'LIKE %...%', placeholder: '曖昧検索' 

                        }
                    ]
                ]

            });
            Table.api.bindevent(table);// 为表格绑定事件
        }
    };
    return Controller;
});