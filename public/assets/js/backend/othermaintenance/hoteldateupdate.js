define(['jquery', 'bootstrap', 'backend', 'form'], function ($, undefined, Backend, Form) {
    var Controller = {
        index: function () {
            Form.api.bindevent //  Form.events.validator ---  要用到日期控件，不能只绑定validator
         
                    ($("#hotel_update-form"),
                            function (data, ret) {//成功回调，刷新
                                location.reload();
                                $("input[name='row[cur_password]']").val('');
                                $("input[name='row[password]']").val('');//密码框空白
                                return true;
                            });
        },
    };
    return Controller;
});