//对应于 backen index控制器的js
define(['jquery', 'bootstrap', 'backend', 'addtabs', 'adminlte', 'form'], function ($, undefined, Backend, undefined, AdminLTE, Form) {
    //依赖于 'jquery', 'bootstrap', 'backend', 'addtabs', 'adminlte', 'form' //具体位置回到 require-backend去看就懂了
    var Controller = {
        login: function () {//----这里是 admin/index/login的自动加载的代码 ↓
            var lastlogin = localStorage.getItem("lastlogin");//localStorage 是javascript对象
            if (lastlogin) {//如果已经保存了 "lastlogin" 在localStorage，（应该是一个json字符串）
                lastlogin = JSON.parse(lastlogin);//把lastlogin解释成对象
                $("#profile-img").attr("src", Backend.api.cdnurl(lastlogin.avatar));// 简介画像： 上次登录的图像地址
                $("#pd-form-username").val(lastlogin.username);//「担当者名」文本框：　自动输入用户id
            }
            
            Fast.config.toastr.positionClass = "toast-top-center";//设置toastr.positionClass，让错误提示框居中
            
            $("#login-form").data("validator-options", { //为login-form  附加数据"validator-options"，数据是一个对象---
                //于require-form.js里头的 后面的bindevent⇛events.validator方法会合拼这个数据然后配置validate, (那里并没有定义invalid: 和target)
                invalid: function (form, errors) {
                    $.each(errors, function (i, err_msg) {// jquery each() 方法规定为每个匹配元素规定运行的函数。 语法 $(selector).each(function(index,element))
                        Toastr.error(err_msg);//本地验证未通过时提示
                    });
                },
                target: '#errtips'//把错误信息放入#errtips （被隐藏），
            });
       
            Form.api.bindevent(//为表单绑定事件　↓ 20180730 如果
            $("#login-form"),// 找到id为[login-form]的， form对象本身
            function (ajaxResponseData,ajaxResponseObj) { //成功时候回调函数的·
                 //console.info("★ret:"+ret.msg);
                var last_login_obj={id: ajaxResponseData.id, username: ajaxResponseData.username, avatar: ajaxResponseData.avatar};//构造login成功对象
                var jsonStr=JSON.stringify(last_login_obj);//转成json字符串 //JSON.stringify() 方法用于将 JavaScript 值转换为 JSON 字符串
                localStorage.setItem("lastlogin", jsonStr);//把转成的（last_login_obj）json字符串存入localStorage         
                /**    LocalStorage      是对Cookie的优化没有时间限制的数据存储 */
                location.href = Backend.api.fixurl(ajaxResponseData.url);// 在这里 向地址栏写入目标地址进入index！
                //console.info(data.url);
            }//------------success ，这个方法将会被绑到validator那里去，当 （"#login-form"）validate成功（valid之后），将会被执行！
            );//为表单绑定事件↑
        } //----这里是 admin/index/login的自动加载的代码  ↑
        
       
        ,index: function () {//----这里是 admin/index/index的自动加载的代码 ↓  
            
            $(window).resize(function () {//窗口大小改变,修正主窗体最小高度
                $(".tab-addtabs").css("height", $(".content-wrapper").height() + "px");
            });

            $(document).on("dblclick", ".sidebar-menu li > a", function (e) {//双击重新加载页面
                $("#con_" + $(this).attr("addtabs") + " iframe").attr('src', function (i, val) {
                    return val;
                });
                e.stopPropagation();
            });

            $(window).on("blur", function () {//修复在移除窗口时下拉框不隐藏的BUG
                $("[data-toggle='dropdown']").parent().removeClass("open");
                if ($("body").hasClass("sidebar-open")) {
                    $(".sidebar-toggle").trigger("click");
                }
            });

            //切换左侧sidebar显示隐藏
            $(document).on("click fa.event.toggleitem", ".sidebar-menu li > a", function (e) {
                $(".sidebar-menu li").removeClass("active");
                //当外部触发隐藏的a时,触发父辈a的事件
                if (!$(this).closest("ul").is(":visible")) {
                    $(this).closest("ul").prev().trigger("click");//如果不需要左侧的菜单栏联动可以注释此行即可
                }
                var visible = $(this).next("ul").is(":visible");
                if (!visible) {
                    $(this).parents("li").addClass("active");
                } else {
                }
                e.stopPropagation();
            });


            //全屏事件
            $(document).on('click', "[data-toggle='fullscreen']", function () {
                console.info('fullscreen!!!!!!!!!!!!!!!');
                var doc = document.documentElement;
                if ($(document.body).hasClass("full-screen")) {
                    $(document.body).removeClass("full-screen");
                    document.exitFullscreen ? document.exitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen && document.webkitExitFullscreen();
                } else {
                    $(document.body).addClass("full-screen");
                    doc.requestFullscreen ? doc.requestFullscreen() : doc.mozRequestFullScreen ? doc.mozRequestFullScreen() : doc.webkitRequestFullscreen ? doc.webkitRequestFullscreen() : doc.msRequestFullscreen && doc.msRequestFullscreen();
                }
            });

            
            $('#nav').addtabs({iframeHeight: "100%"});//绑定tabs事件
            if ($("ul.sidebar-menu li.active a").size() > 0) {
                $("ul.sidebar-menu li.active a").trigger("click");
            } else {
                $("ul.sidebar-menu li a[url!='javascript:;']:first").trigger("click");
            }
            
            if (Config.referer) {
                Backend.api.addtabs(Config.referer);//刷新页面后跳到到刷新前的页面
            }

            /**
             * List of all the available skins
             *
             * @type Array
             */
            var my_skins = [
                "skin-blue",
                "skin-black",
                "skin-red",
                "skin-yellow",
                "skin-purple",
                "skin-green",
                "skin-blue-light",
                "skin-black-light",
                "skin-red-light",
                "skin-yellow-light",
                "skin-purple-light",
                "skin-green-light"
            ];
            setup();
            /**
             * Retrieve default settings and apply them to the template
             *
             * @returns void
             */
            function setup() {
                var tmp = get('skin');
                if (tmp && $.inArray(tmp, my_skins))
                    change_skin(tmp);

                // 皮肤切换
                $("[data-skin]").on('click', function (e) {
                    if ($(this).hasClass('knob'))
                        return;
                    e.preventDefault();
                    change_skin($(this).data('skin'));
                });

                // 布局切换
                $("[data-layout]").on('click', function () {
                    change_layout($(this).data('layout'));
                });

                // 切换子菜单显示和菜单小图标的显示
                $("[data-menu]").on('click', function () {
                    if ($(this).data("menu") == 'show-submenu') {
                        $("ul.sidebar-menu").toggleClass("show-submenu");
                    } else {
                        $(".nav-addtabs").toggleClass("disable-top-badge");
                    }
                });

                // 右侧控制栏切换
                $("[data-controlsidebar]").on('click', function () {
                    change_layout($(this).data('controlsidebar'));
                    var slide = !AdminLTE.options.controlSidebarOptions.slide;
                    AdminLTE.options.controlSidebarOptions.slide = slide;
                    if (!slide)
                        $('.control-sidebar').removeClass('control-sidebar-open');
                });

                // 右侧控制栏背景切换
                $("[data-sidebarskin='toggle']").on('click', function () {
                    var sidebar = $(".control-sidebar");
                    if (sidebar.hasClass("control-sidebar-dark")) {
                        sidebar.removeClass("control-sidebar-dark")
                        sidebar.addClass("control-sidebar-light")
                    } else {
                        sidebar.removeClass("control-sidebar-light")
                        sidebar.addClass("control-sidebar-dark")
                    }
                });

                // 菜单栏展开或收起
                $("[data-enable='expandOnHover']").on('click', function () {
                    $(this).attr('disabled', true);
                    AdminLTE.pushMenu.expandOnHover();
                    if (!$('body').hasClass('sidebar-collapse'))
                        $("[data-layout='sidebar-collapse']").click();
                });

                // 重设选项
                if ($('body').hasClass('fixed')) {
                    $("[data-layout='fixed']").attr('checked', 'checked');
                }
                if ($('body').hasClass('layout-boxed')) {
                    $("[data-layout='layout-boxed']").attr('checked', 'checked');
                }
                if ($('body').hasClass('sidebar-collapse')) {
                    $("[data-layout='sidebar-collapse']").attr('checked', 'checked');
                }
                if ($('ul.sidebar-menu').hasClass('show-submenu')) {
                    $("[data-menu='show-submenu']").attr('checked', 'checked');
                }
                if ($('ul.nav-addtabs').hasClass('disable-top-badge')) {
                    $("[data-menu='disable-top-badge']").attr('checked', 'checked');
                }

            }

            /**
             * Toggles layout classes
             *
             * @param String cls the layout class to toggle
             * @returns void
             */
            function change_layout(cls) {
                $("body").toggleClass(cls);
                AdminLTE.layout.fixSidebar();
                //Fix the problem with right sidebar and layout boxed
                if (cls == "layout-boxed")
                    AdminLTE.controlSidebar._fix($(".control-sidebar-bg"));
                if ($('body').hasClass('fixed') && cls == 'fixed' && false) {
                    AdminLTE.pushMenu.expandOnHover();
                    AdminLTE.layout.activate();
                }
                AdminLTE.controlSidebar._fix($(".control-sidebar-bg"));
                AdminLTE.controlSidebar._fix($(".control-sidebar"));
            }

            /**
             * Replaces the old skin with the new skin
             * @param String cls the new skin class
             * @returns Boolean false to prevent link's default action
             */
            function change_skin(cls) {
                if (!$("body").hasClass(cls)) {
                    $.each(my_skins, function (i) {
                        $("body").removeClass(my_skins[i]);
                    });
                    $("body").addClass(cls);
                    store('skin', cls);
                    var cssfile = Config.site.cdnurl + "/assets/css/skins/" + cls + ".css";
                    $('head').append('<link rel="stylesheet" href="' + cssfile + '" type="text/css" />');
                }
                return false;
            }

            /**
             * Store a new settings in the browser
             *
             * @param String name Name of the setting
             * @param String val Value of the setting
             * @returns void
             */
            function store(name, val) {
                if (typeof (Storage) !== "undefined") {
                    localStorage.setItem(name, val);
                } else {
                    window.alert('Please use a modern browser to properly view this template!');
                }
            }
            /**
             * Get a prestored setting
             *
             * @param String name Name of of the setting
             * @returns String The value of the setting | null
             */
            function get(name) {
                if (typeof (Storage) !== "undefined") {
                    return localStorage.getItem(name);
                } else {
                    window.alert('Please use a modern browser to properly view this template!');
                }
            }
            
            $(window).resize();
        }//---- //----这里是 admin/index/index的自动加载的代码 ↑
        
    };//---end of  var Controller 
    return Controller;
});