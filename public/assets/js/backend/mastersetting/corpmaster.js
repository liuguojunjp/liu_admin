define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-table-editable', 'bootstrap-editable','my_validator'],
//加载 'jquery', 'bootstrap'，
function ($, undefined, Backend, Table, Form,undefined, undefined,MyValidator) {
    var Controller = {
        index: function () {//admin/auth/adminlog　自动加载内容 ↓
            
            $('#enable').click(function () {//编辑/非编辑 切换
                        $('.editable').editable('toggleDisabled');
                        if ($('#enable').html() === '閲覧モードへ')
                        {
                            $('#enable').html("編集モードへ");
                        } else
                        {
                            $('#enable').html("閲覧モードへ");
                        }

                    });
            
            Table.api.init({// 初始化表格参数配置
                extend: {
                    index_url: 'mastersetting/corpmaster/index',
                    add_url: 'mastersetting/corpmaster/add',
                    edit_url: 'mastersetting/corpmaster/edit',
                    del_url: 'mastersetting/corpmaster/del',
                    table_function_title:__('会社'),
                }
            });

            var table = $("#table_corpmaster_list");

            //console.info('★ ★ ★ ★ ★ ★ '+$.fn.bootstrapTable.defaults.extend.index_url+'★ ★ ★ ★ ★ ★ ');
            table.bootstrapTable({// 初始化表格
                sortName: 'CorpCode',
                pk:'CorpCode',
                url: $.fn.bootstrapTable.defaults.extend.index_url,//表格的参数定义在 jQuery.fn.bootstrapTable.defaults    Bootstrap-table 基础配置 , extend.index_url 其实就是上面定义的那个 'auth/adminlog/index'
                columns: [
                    [
                                {field: 'CorpCode', title: __('会社ID'), operate: 'BETWEEN', placeholder: '表示順番範囲検索'},
                                {field: 'Name', title: __('会社名'), operate: 'LIKE %...%', placeholder: '会社名曖昧検索'
                                ,editable: {
                                                type: 'text',
                                                title: __('会社名'),
                                                validate: function (v) {
                                                    if (!v)
                                                        return '会社名空白不可';
                                                }
                                            }
                        
                        },
                        {field: 'Kana', title: __('会社カナ'), operate: 'LIKE %...%', placeholder: 'カナ曖昧検索'
                        ,editable: {
                                                type: 'text',
                                                title: __('会社カナ'),
                                                validate: function (v) {
                                                    if (!MyValidator.isKana(v))
                                                    {
                                                        return 'カナ文字を入力してください';
                                                    }
                                                }
                                            }
                        },
                                {field: 'Tel', title: __('電話'), operate: 'LIKE %...%', placeholder: '電話曖昧検索'
                                ,editable: {
                                                type: 'text',
                                                title: __('会社電話'),
                                                validate: function (v) {
                                                    if (!MyValidator.isTel(v))
                                                    {
                                                        return '正しい電話文字列を入力してください';
                                                    }
                                                }
                                            }
                        }, //, operate: false 是不参加搜索的意思
                                {field: 'Fax', title: __('FAX'), operate: 'LIKE %...%', placeholder: 'FAX曖昧検索'
                        ,editable: {
                                                type: 'text',
                                                title: __('会社FAX'),
                                                validate: function (v) {
                                                    if (!MyValidator.isTel(v))
                                                    {
                                                        return '正しいFAX文字列を入力してください';
                                                    }
                                                }
                                            }        
                        },
                                {field: 'Address1', title: __('都道府県'), operate: 'LIKE %...%', placeholder: '住所曖昧検索'
                        ,editable: {
                                                type: 'text',
                                                title: __('都道府県'),
                                                
                                            }        
                        },
                                {field: 'Address2', title: __('都市'), operate: 'LIKE %...%', placeholder: '住所曖昧検索'
                                 ,editable: {
                                                type: 'text',
                                                title: __('都市'),
                                                
                                            }     },
                                {field: 'Address3', title: __('区町村'), operate: 'LIKE %...%', placeholder: '住所曖昧検索'
                                 ,editable: {
                                                type: 'text',
                                                title: __('区町村'),
                                                
                                            }     },
                                {field: 'Address4', title: __('番地'), operate: 'LIKE %...%', placeholder: '住所曖昧検索'
                                 ,editable: {
                                                type: 'text',
                                                title: __('番地'),
                                                
                                            }     },
                                {field: 'Mail', title: __('メール'), operate: 'LIKE %...%', placeholder: 'メール曖昧検索'
                                ,editable: {
                                                type: 'text',
                                                title: __('会社メール'),
                                                validate: function (v) {
                                                    if (!MyValidator.isMail(v))
                                                    {
                                                        return '正しいメール文字列を入力してください';
                                                    }
                                                }
                                            }       
                        },
                                {field: 'FreeSummary1', title: __('自由項目1'), operate: 'LIKE %...%', placeholder: '住所曖昧検索'
                         ,editable: {
                                                type: 'text',
                                                title: __('自由項目1'),
                                                
                                            }             
                        },
                                {field: 'FreeSummary2', title: __('自由項目2'), operate: 'LIKE %...%', placeholder: '住所曖昧検索'
                         ,editable: {
                                                type: 'text',
                                                title: __('自由項目2'),
                                                
                                            }             
                        },
                                {field: 'FreeSummary3', title: __('自由項目3'), operate: 'LIKE %...%', placeholder: '住所曖昧検索'
                         ,editable: {
                                                type: 'text',
                                                title: __('自由項目3'),
                                                
                                            }             
                        },
                                 {field: 'Remark', title: __('備考'), operate: 'LIKE %...%', placeholder: '備考曖昧検索'
                         ,editable: {
                                                type: 'textarea',
                                                title: __('備考'),
                                                
                                            }              
                        },
                        {field: 'operate', title: __('　'), table: table, 
                        events: Table.api.events.operate, // 定义了 单元格元素事件(删除和编辑)
                        formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index);//执行（call,后面传入this），执行 require-table 中的 api.formatter.operate 初始化
                                //call 方法可以用来代替另一个对象调用一个方法。call 方法可将一个函数的对象上下文从初始的上下文改变为由 thisObj 指定的新对象。 
                                //如果没有提供 thisObj 参数，那么 Global 对象被用作 thisObj。
                         }}
                    ]
                ]
                        , onEditableSave: function (field, row, oldValue, $el) {
                            Fast.api.ajax({
                                url: "mastersetting/corpmaster/fastedit/field_name/" + field,
                                data: row,
                            } //参数1---AJAX options
                            , function (data, ret) {
//                                alert(ret.msg);alert(ret.data.field_name)
                            }////参数2 ---ajax的服务端验证成功了的话要做的事情
                            , function (data, ret) {
                                alert(ret.msg);
                                $(Table.config.refreshbtn).click();

                            }////参数3 ---ajax的服务端验证失敗了的话要做的事情
                            );

                        }
                        , onLoadSuccess: function () {
                            $('.editable').editable('toggleDisabled');
                        }
            });
            
            Table.api.bindevent(table);// 为表格绑定事件
        },//admin/mastersetting/corpmaster　自动加载内容 ↑
        
        add: function () { // 访问'mastersetting/corpmaster/add'的时候，这里的内容会被加载
            Form.api.bindevent($("form[role=form]"));//为找到本windows的表单，为表单绑定事件! 适用于require form`里头的东西， 没有这行可不能提交
                },
        edit: function () { // 访问'mastersetting/corpmaster/edit'的时候，这里的内容会被加载
            Form.api.bindevent($("form[role=form]"));
        }
        
    };
    return Controller;
});