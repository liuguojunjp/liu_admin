define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-table-editable', 'bootstrap-editable'],
//加载 'jquery', 'bootstrap'，
function ($, undefined, Backend, Table, Form,undefined, undefined) {
    var Controller = {
        index: function () {//admin/auth/adminlog　自动加载内容 ↓
            
             $('#enable').click(function () {//编辑/非编辑 切换
                        $('.editable').editable('toggleDisabled');
                        if ($('#enable').html() === '閲覧モードへ')
                        {
                            $('#enable').html("編集モードへ");
                        } else
                        {
                            $('#enable').html("閲覧モードへ");
                        }

                    });
            
            Table.api.init({// 初始化表格参数配置
                extend: {
                    index_url: 'mastersetting/payitemmaster/index',
                    add_url: 'mastersetting/payitemmaster/add',
                    edit_url: 'mastersetting/payitemmaster/edit',
                    del_url: 'mastersetting/payitemmaster/del',
                    table_function_title:__('入金科目'),
                }
            });

            var table = $("#table_payitemmaster_list");

            //console.info('★ ★ ★ ★ ★ ★ '+$.fn.bootstrapTable.defaults.extend.index_url+'★ ★ ★ ★ ★ ★ ');
            table.bootstrapTable({// 初始化表格
                sortName: 'ItemNo',
                pk:'ItemNo',
                url: $.fn.bootstrapTable.defaults.extend.index_url,//表格的参数定义在 jQuery.fn.bootstrapTable.defaults    Bootstrap-table 基础配置 , extend.index_url 其实就是上面定义的那个 'auth/adminlog/index'
                columns: [
                    [
                        {field: 'ItemNo', title: __('入金科目ID'),  operate: 'BETWEEN', placeholder: '入金科目ID範囲検索'},
                        {field: 'ItemName', title: __('入金科目名'), operate: 'LIKE %...%', placeholder: '曖昧検索' 
                        ,editable: {
                                                type: 'text',
                                                title: __('入金科目名'),
                                                validate: function (v) {
                                                    if (!v)
                                                        return '入金科目名空白不可';
                                                }
                                            }
                        
                        },
                        {field: 'SalesSectionCode', title: __('売上部門'), operate: false 
                        , editable: {
                                                type: 'select',
                                                title: __('売上部門'),
                                                source: SalessectionData
                                            }
                        },
                        {field: 'SummarySecCode', title: __('集計部門名'), operate: false 
                         , editable: {
                                                type: 'select',
                                                title: __('集計部門'),
                                                source: SummarysectionData
                                            }
                        },
                        {field: 'ItemKindCode', title: __('入金種別'), operate: false 
                            
                             , editable: {
                                                type: 'select',
                                                title: __('科目種別'),
                                                source: [{value: "90", text: "現金"}, {value: "92", text: "前受金"}, {value: "93", text: "クーポン"}, {value: "94", text: "クレジット"}, {value: "95", text: "売掛金"}
                                        ,{value: "96", text: "業者売掛金"}       
                                        ]
                                            }
                            
                        
                        },
                        {field: 'ReceiptPrtKbn', title: __('印字区分'), operate: false 
                        , editable: {
                                                type: 'select',
                                                title: __('印字区分'),
                                                source: [{value: "0", text: "非印字"}, {value: "1", text: "印字"}]
                                            }
                        },
                        {field: 'operate', title: __('　'), table: table, 
                        events: Table.api.events.operate, // 定义了 单元格元素事件(删除和编辑)
                        formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index);//执行（call,后面传入this），执行 require-table 中的 api.formatter.operate 初始化
                                //call 方法可以用来代替另一个对象调用一个方法。call 方法可将一个函数的对象上下文从初始的上下文改变为由 thisObj 指定的新对象。 
                                //如果没有提供 thisObj 参数，那么 Global 对象被用作 thisObj。
                         }}
                    ]
                ]
                ,
                      onEditableSave: function (field, row, oldValue, $el) {
                            Fast.api.ajax({
                                url: "mastersetting/payitemmaster/fastedit/field_name/" + field,
                                data: row,
                            } //参数1---AJAX options
                            ,function (data, ret) {
//                                alert(ret.msg);
//                                        alert(ret.data.field_name)
                               }////参数2 ---ajax的服务端验证成功了的话要做的事情
                           ,function (data, ret) {
                               alert(ret.msg);
                                            //location.reload();
                                            //table.bootstrapTable('refresh');
                                            $(Table.config.refreshbtn).click();
                               
                               }////参数3 ---ajax的服务端验证失敗了的话要做的事情
                            );
                                    
                                }
                                
                                , onLoadSuccess: function () {
                                    $('.editable').editable('toggleDisabled');
                                }
            });
            
            Table.api.bindevent(table);// 为表格绑定事件
        },//admin/mastersetting/payitemmaster　自动加载内容 ↑
        
        add: function () { // 访问'mastersetting/payitemmaster/add'的时候，这里的内容会被加载
            Form.api.bindevent($("form[role=form]"));//为找到本windows的表单，为表单绑定事件! 适用于require form`里头的东西， 没有这行可不能提交
                },
        edit: function () { // 访问'mastersetting/payitemmaster/edit'的时候，这里的内容会被加载
            Form.api.bindevent($("form[role=form]"));
        }
        
    };
    return Controller;
});