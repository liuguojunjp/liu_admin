define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-table-editable', 'bootstrap-editable'],
//加载 'jquery', 'bootstrap'，
        function ($, undefined, Backend, Table, Form,undefined, undefined) {
            var Controller = {
                index: function () {//admin/auth/adminlog　自动加载内容 ↓
                     $('#enable').click(function () {//编辑/非编辑 切换
                        $('.editable').editable('toggleDisabled');
                        if ($('#enable').html() === '閲覧モードへ')
                        {
                            $('#enable').html("編集モードへ");
                        } else
                        {
                            $('#enable').html("閲覧モードへ");
                        }

                    });
                    Table.api.init({// 初始化表格参数配置
                        extend: {
                            index_url: 'mastersetting/packmaster/index',
                            add_url: 'mastersetting/packmaster/add',
                            edit_url: 'mastersetting/packmaster/edit',
                            del_url: 'mastersetting/packmaster/del',
                            table_function_title: __('パック'),
                        }
                    });
                    var table = $("#table_packmaster_list");
                    //console.info('★ ★ ★ ★ ★ ★ '+$.fn.bootstrapTable.defaults.extend.index_url+'★ ★ ★ ★ ★ ★ ');
                    table.bootstrapTable({// 初始化表格
                        sortName: 'ItemNo',
                        pk: 'ItemNo',
                        url: $.fn.bootstrapTable.defaults.extend.index_url, //表格的参数定义在 jQuery.fn.bootstrapTable.defaults    Bootstrap-table 基础配置 , extend.index_url 其实就是上面定义的那个 'auth/adminlog/index'
                        columns: [
                            [
                                {field: 'ItemNo', title: __('パックID'), operate: 'BETWEEN', placeholder: '範囲検索'},
                                {field: 'ItemName', title: __('パック名'), operate: 'LIKE %...%', placeholder: '曖昧検索'
                                 ,editable: {
                                                type: 'text',
                                                title: __('パック名'),
                                                validate: function (v) {
                                                    if (!v)
                                                        return 'パック名空白不可';
                                                }
                                            }
                                 }, //, operate: false 是不参加搜索的意思
                                {field: 'child_count', title: __('子科目数'), operate: 'BETWEEN', placeholder: '範囲検索'},
                                {field: 'operate', title: __('　'), table: table,
                                    events: Table.api.events.operate, // 定义了 单元格元素事件(删除和编辑)
                                    formatter: function (value, row, index) {
                                        return Table.api.formatter.operate.call(this, value, row, index); //执行（call,后面传入this），执行 require-table 中的 api.formatter.operate 初始化
                                        //call 方法可以用来代替另一个对象调用一个方法。call 方法可将一个函数的对象上下文从初始的上下文改变为由 thisObj 指定的新对象。 
                                        //如果没有提供 thisObj 参数，那么 Global 对象被用作 thisObj。
                                    }}
                            ]
                        ],
                        onEditableSave: function (field, row, oldValue, $el) {   
                            Fast.api.ajax({
                                url: "mastersetting/packmaster/fastedit/field_name/" + field,
                                data: row,
                            } //参数1---AJAX options
                            ,function (data, ret) {
//                                alert(ret.msg);
//                                        alert(ret.data.field_name)
                               }////参数2 ---ajax的服务端验证成功了的话要做的事情
                           ,function (data, ret) {
                               alert(ret.msg);
                                            //location.reload();
                                            //table.bootstrapTable('refresh');
                                            $(Table.config.refreshbtn).click();
                               
                               }////参数3 ---ajax的服务端验证失敗了的话要做的事情
                            );
                                    
                                }
                                
                                , onLoadSuccess: function () {
                                    $('.editable').editable('toggleDisabled');
                                }
                    });
                    Table.api.bindevent(table); // 为表格绑定事件
                }, //admin/mastersetting/packmaster　自动加载内容 ↑

                add: function () { // 访问'mastersetting/packmaster/add'的时候，这里的内容会被加载
                    Controller.bind_event();
                },
                edit: function () { // 访问'mastersetting/packmaster/edit'的时候，这里的内容会被加载
                    Controller.bind_event();
                },
                bind_event: function (form) {
                    Form.events.validator($("form[role=form]")); //为找到本windows的表单，为表单绑定事件! 适用于require form`里头的东西， 没有这行可不能提交
                    Form.events.selectpicker($("form[role=form]"));//没有这个 'data-live-search'=>'true' 无效！ 要require 
                    Controller.fieldlist($("form[role=form]"));
                    $('#btn_pack_add_sumit').removeAttr("disabled"); //防止还没validator 没有load完用户就提交了
                },

                fieldlist: function (form) {
                    if ($(".fieldlist", form).size() === 0) {
                        return;
                    }
                    $(".fieldlist", form).on("click", ".append", function () {
                        var rel = parseInt($(this).closest("dl").attr("rel"))+1;//数量加一
                        var name = $(this).closest("dl").data("name"); //closest() 方法获得匹配选择器的第一个祖先元素，从当前元素开始沿 DOM 树向上。
                        $(this).closest("dl").attr("rel", rel);//记录到祖先
                        var rel_span = 'span_' + rel;
                        $('<dd class="form-inline">\n\
 ' + '<span id="' + rel_span + '"' + '></span>'
                                + '<input type="number" name="' + name + '[additem_amount][' + rel + ']" class="form-control" value="1" size="10" /> '
                                + '<span class="btn btn-sm btn-danger btn-remove"><i class="fa fa-times"></i></span>'
                                + '<span class="btn btn-sm btn-primary btn-dragsort"><i class="fa fa-arrows"></i></span>'
                                + '</dd>')
                                .insertBefore($(this).parent());
                        $("#" + rel_span).append($("#PackAdditems_0").clone().prop('id', 'PackAdditems_' + rel).prop('name', name + '[additem_id][' + rel + ']'));
                        $('.selectpicker', form).selectpicker();//漏了这句看不见！！
                    });
                    
                   
                    $(".fieldlist", form).on("click", "dd .btn-remove", function () {
                        $(this).parent().remove();
                    });
                    require(['dragsort'], function () {//拖拽排序
                        $("dl.fieldlist", form).dragsort({//绑定拖动排序
                            itemSelector: 'dd',
                            dragSelector: ".btn-dragsort",
                            dragEnd: function () {
                            },
                            placeHolderTemplate: "<dd></dd>"
                        });
                    });
                }
            };
            return Controller;
        }
);