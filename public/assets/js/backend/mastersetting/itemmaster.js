define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-table-editable', 'bootstrap-editable'],
//加载 'jquery', 'bootstrap'，
function ($, undefined, Backend, Table, Form,undefined, undefined) {
    var Controller = {
        index: function () {//admin/auth/adminlog　自动加载内容 ↓
            
                    $('#enable').click(function () {//编辑/非编辑 切换
                        $('.editable').editable('toggleDisabled');
                        if ($('#enable').html() === '閲覧モードへ')
                        {
                            $('#enable').html("編集モードへ");
                        } else
                        {
                            $('#enable').html("閲覧モードへ");
                        }

                    });
            
            Table.api.init({// 初始化表格参数配置
                extend: {
                    index_url: 'mastersetting/itemmaster/index',
                    add_url: 'mastersetting/itemmaster/add',
                    edit_url: 'mastersetting/itemmaster/edit',
                    del_url: 'mastersetting/itemmaster/del',
                    table_function_title:__('科目'),
                }
            });

                    var table = $("#table_itemmaster_list");
                    var taxkbn = '[{value: "0", text: "なし"}, {value: "1", text: "外税"}, {value: "2", text: "内税"}]';
// alert(taxkbn);
// console.info(taxkbn)
// var Salessection = SalessectionData;//在view模板生成
// alert(Salessection);
    console.info(SalessectionData);
   
            //console.info('★ ★ ★ ★ ★ ★ '+$.fn.bootstrapTable.defaults.extend.index_url+'★ ★ ★ ★ ★ ★ ');
            table.bootstrapTable({// 初始化表格
                //sortName: 'SortNo,DispFlg desc,ItemNo',
                sortName: 'ItemNo',
                pk:'ItemNo',
                url: $.fn.bootstrapTable.defaults.extend.index_url,//表格的参数定义在 jQuery.fn.bootstrapTable.defaults    Bootstrap-table 基础配置 , extend.index_url 其实就是上面定义的那个 'auth/adminlog/index'
                columns: [
                    [
                                {field: 'ItemNo', title: __('科目ID'), operate: 'BETWEEN', placeholder: '科目ID範囲検索'},
                                {field: 'SortNo', title: __('表示順番'), operate: 'BETWEEN', placeholder: '表示順番範囲検索'
                                    , editable: {
                                        type: 'text',
                                        title: '表示順番',
                                        validate: function (v) {
                                            if (isNaN(v))
                                            {
                                                return '表示順番は数字必須';
                                            }
                                            var age = parseInt(v);
                                            if (age <= 0)
                                            {
                                                return '表示順番>=0';
                                            }
                                        }
                                    }
                                },
                                {field: 'DispFlg', title: __('表示フラグ'), operate: false
                                , editable: {
                                                type: 'select',
                                                title: __('表示フラグ'),
                                                source: [{value: "0", text: "非表示"}, {value: "1", text: "表示"}]
                                            }
                                },
                                {field: 'ItemName', title: __('科目名'), operate: 'LIKE %...%', placeholder: '曖昧検索'
                                ,editable: {
                                                type: 'text',
                                                title: __('科目名'),
                                                validate: function (v) {
                                                    if (!v)
                                                        return '科目名空白不可';
                                                }
                                            }
                                }, 
                                {field: 'BasePrice', title: __('基本料金'), operate: 'BETWEEN', placeholder: '基本料金範囲検索'
                                , editable: {
                                        type: 'text',
                                        title: __('基本料金'),
                                        validate: function (v) {
                                            if (!v)
                                            {
                                                        return '基本料金空白不可';
                                            }
                                            if (isNaN(v))
                                            {
                                                return '基本料金は数字必須';
                                            }
       
                                        }
                                    }
                                },
                                {field: 'ServiceRate', title: __('サービス料率(%)'), operate: false, align: 'right'
//                                    formatter: function (value, row, index) {
//                                        return value + '%';
//                                    }
                                , editable: {
                                        type: 'text',
                                        title: __('サービス料率'),
                                        validate: function (v) {
                                            if (!v||isNaN(v))
                                            {
                                                return 'サービス料率は数字必須';
                                            }
       
                                        }
                                    }
                                },
                                {field: 'ServiceKbn', title: __('サービス料区分'), operate: false
                                , editable: {
                                                type: 'select',
                                                title: __('サービス料区分'),
                                                source: [{value: "0", text: "なし"}, {value: "1", text: "別"}, {value: "2", text: "込"}]
                                            }
                                },
                                {field: 'TaxKbn', title: __('消費税区分'), operate: false
                                , editable: {
                                                type: 'select',
                                                title: __('消費税区分'),
                                                source: taxkbn
                                            }
                                },
                                {field: 'SpaTaxKbn', title: __('入湯税区分'), operate: false
                                , editable: {
                                                type: 'select',
                                                title: __('入湯税区分'),
                                                source: [{value: "0", text: "なし"}, {value: "1", text: "外税"}, {value: "2", text: "内税"}]
                                            }
                                },
                                {field: 'SalesSectionCode', title: __('売上部門'), operate: false
                                , editable: {
                                                type: 'select',
                                                title: __('売上部門'),
                                                source: SalessectionData
                                            }
                                },
                                {field: 'SummarySecCode', title: __('集計部門'), operate: false
                                 , editable: {
                                                type: 'select',
                                                title: __('集計部門'),
                                                source: SummarysectionData
                                            }
                                
                                },
                                
                                {field: 'ItemKindCode', title: __('科目種別'), operate: false
                                , editable: {
                                                type: 'select',
                                                title: __('科目種別'),
                                                source: [{value: "10", text: "宿泊基本"}, {value: "20", text: "休憩基本"}, {value: "50", text: "追加料理"}, {value: "51", text: "追加飲料"}, {value: "59", text: "追加その他"}
                                        ,{value: "60", text: "立替料理"}, {value: "61", text: "立替飲料"}, {value: "69", text: "立替その他"}        
                                        ]
                                            }
                                },
                                {field: 'PeopleKbn', title: __('入区分'), operate: false
                                 , editable: {
                                                type: 'select',
                                                title: __('入区分'),
                                                source: [{value: "1", text: "大人"}, {value: "2", text: "子A"}, {value: "3", text: "子B"}, {value: "4", text: "子C"}, {value: "5", text: "幼児"}]
                                            }
                                },
                                {field: 'ReceiptPrtKbn', title: __('印字区分'), operate: false
                                , editable: {
                                                type: 'select',
                                                title: __('印字区分'),
                                                source: [{value: "0", text: "非印字"}, {value: "1", text: "印字"}]
                                            }
                                
                                },
                                
//                                {field: '料理区分', title: __('料理区分'), operate: false},
//                                {field: '業務連絡表区分', title: __('業務連絡表区分'), operate: false},
//                                {field: '宿泊税区分', title: __('宿泊税区分'), operate: false},
//                                {field: '宿泊税自動算出区分', title: __('宿泊税自動算出区分'), operate: false},
//                                {field: '宿泊税額', title: __('宿泊税額'), operate: false},
                                
                        {field: 'operate', title: __('　'), table: table, 
                        events: Table.api.events.operate, // 定义了 单元格元素事件(删除和编辑)
                        formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index);//执行（call,后面传入this），执行 require-table 中的 api.formatter.operate 初始化
                                //call 方法可以用来代替另一个对象调用一个方法。call 方法可将一个函数的对象上下文从初始的上下文改变为由 thisObj 指定的新对象。 
                                //如果没有提供 thisObj 参数，那么 Global 对象被用作 thisObj。
                         }}
                    ]
                ]
                  ,
                                onEditableSave: function (field, row, oldValue, $el) {
                                    
//                                    var index = Layer.load();//显示一个loading层
//                                    $.ajax({
//                                        type: "post",
//                                        url: "mastersetting/itemmaster/fastedit/field_name/"+field,
//                                        data: row,
//                                        dataType: 'JSON',
//                                        success: function (data, status) {
//                                            Layer.close(index);//关闭loading层
//                                            if (status == "success") {
//                                                //alert('data commited');
//                                                Toastr.success("更新完了");
//                                                //$(Table.config.refreshbtn).click();
//                                                //table.bootstrapTable('refresh');
//                                            }
//                                        },
//                                        error: function () {
//                                            Layer.close(index);//关闭loading层
//                                            alert('更新失敗');
//                                            //location.reload();
//                                            //table.bootstrapTable('refresh');
//                                            $(Table.config.refreshbtn).click();
//                                        },
//                                        complete: function () {
//
//                                        }
//                                    });
                                    
                                    
                            Fast.api.ajax({
                                url: "mastersetting/itemmaster/fastedit/field_name/" + field,
                                data: row,
                            } //参数1---AJAX options
                            ,function (data, ret) {
//                                alert(ret.msg);
//                                        alert(ret.data.field_name)
                               }////参数2 ---ajax的服务端验证成功了的话要做的事情
                           ,function (data, ret) {
                               alert(ret.msg);
                                            //location.reload();
                                            //table.bootstrapTable('refresh');
                                            $(Table.config.refreshbtn).click();
                               
                               }////参数3 ---ajax的服务端验证失敗了的话要做的事情
                            );
                                    
                                }
                                
                                , onLoadSuccess: function () {
                                    $('.editable').editable('toggleDisabled');
                                }
            });
            Table.api.bindevent(table);// 为表格绑定事件
        },//admin/mastersetting/itemmaster　自动加载内容 ↑
        add: function () { // 访问'mastersetting/itemmaster/add'的时候，这里的内容会被加载
            Form.api.bindevent($("form[role=form]"));//为找到本windows的表单(role 属性为 "form"的表单)，为表单绑定事件! 适用于require form`里头的东西， 没有这行可不能提交
                },
        edit: function () {// 访问'mastersetting/itemmaster/edit'的时候，这里的内容会被加载
            Form.api.bindevent($("form[role=form]"));
        }
    };
    return Controller;
});