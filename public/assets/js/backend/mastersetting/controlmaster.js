define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            $.each(["#form_ctlmst_edit", ],
                    function (index, value) {
                        // alert(value);
                        Form.events.validator // Form.api.bindevent
                                ($(value),
                                        function (data, ret) {//成功回调，刷新
                                            location.reload();
                                            
                                        },
                                        function (data, ret) {//错误回调
       //                                    if (data.db_msg)alert(data.db_msg);
                                        });
                           Controller. timepicker($("#form_ctlmst_edit")); 
                            Controller. icheck($("#form_ctlmst_edit")); 
                           
                                        
                    });
            $('#btn_ctl_mst_edit_sumit').removeAttr("disabled");//防止还没validator 没有load完用户就提交了
        }, // endof --- index: function ()
        
        icheck: function (form) {
            require(['bootstrap-icheck'], function () {
                var cssfile = Config.site.cdnurl + "/assets/libs/iCheck/all.css";
                $('head').append('<link rel="stylesheet" href="' + cssfile + '" type="text/css" />');//别忘了加载 css
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({//iCheck for checkbox and radio inputs
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });
//                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({//Red color scheme for iCheck
//                    checkboxClass: 'icheckbox_minimal-red',
//                    radioClass: 'iradio_minimal-red'
//                });
//                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({//Flat red color scheme for iCheck
//                    checkboxClass: 'icheckbox_flat-green',
//                    radioClass: 'iradio_flat-green'
//                });
            });

        },
        
        timepicker: function (form) { //绑定日期时间元素事件
//            if ($(".datetimepicker", form).size() <= 0) {
//                return;
//            }
            require(['bootstrap-datetimepicker'], function () {
                var options = {
                    format: 'HH:mm',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-history',
                        clear: 'fa fa-trash',
                        close: 'fa fa-remove'
                    },
                    //showTodayButton: true,
                    showClose: true
                };
                $('.datetimepicker', form).parent().css('position', 'relative');
                $('.datetimepicker', form).datetimepicker(options);
            });
        }
        
        
    };//end of --- var Controller =
    return Controller;
});// end of -- defines