define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-table-editable', 'bootstrap-editable','my_validator'],
//加载 'jquery', 'bootstrap'，
function ($, undefined, Backend, Table, Form,undefined, undefined,MyValidator) {
    var Controller = {
        index: function () {//admin/mastersetting/agentbranchmaster/index　自动加载内容 ↓
            $('#enable').click(function () {//编辑/非编辑 切换
                        $('.editable').editable('toggleDisabled');
                        if ($('#enable').html() === '閲覧モードへ')
                        {
                            $('#enable').html("編集モードへ");
                        } else
                        {
                            $('#enable').html("閲覧モードへ");
                        }

                    });
            Table.api.init({// 初始化表格参数配置
                extend: {
                    index_url: 'mastersetting/agentbranchmaster/index',
                    add_url: 'mastersetting/agentbranchmaster/add',
                    edit_url: 'mastersetting/agentbranchmaster/edit',
                    del_url: 'mastersetting/agentbranchmaster/del',
                    table_function_title:__('業者支店'),
                }
            });

            var table = $("#table_agentbranchmaster_list");

            //console.info('★ ★ ★ ★ ★ ★ '+$.fn.bootstrapTable.defaults.extend.index_url+'★ ★ ★ ★ ★ ★ ');
            table.bootstrapTable({// 初始化表格
                sortName: 'AgentCode,BranchCode',
                pk:'AgentCode,BranchCode',
                pk0:'AgentCode',
                pk1:'BranchCode',
                url: $.fn.bootstrapTable.defaults.extend.index_url,//表格的参数定义在 jQuery.fn.bootstrapTable.defaults    Bootstrap-table 基础配置 , extend.index_url 其实就是上面定义的那个 'auth/adminlog/index'
                columns: [
                    [
                                {field: 'AgentCode', title: __('業者コード'), operate: 'BETWEEN', placeholder: '範囲検索'},
                                {field: 'AgentName', title: __('業者名'), operate: 'LIKE %...%', placeholder: '曖昧検索'},
                                {field: 'BranchCode', title: __('支店コード'), operate: 'BETWEEN', placeholder: '範囲検索'},
                                {field: 'BranchName', title: __('支店名'), operate: 'LIKE %...%', placeholder: '曖昧検索'
                         ,editable: {
                                                type: 'text',
                                                title: __('支店名'),
                                                validate: function (v) {
                                                    if (!v)
                                                        return '支店名空白不可';
                                                }
                                            }        
                        },
                        {field: 'BranchKana', title: __('支店カナ'), operate: 'LIKE %...%', placeholder: '曖昧検索'
                        ,editable: {
                                                type: 'text',
                                                title: __('支店カナ'),
                                                validate: function (v) {
                                                    if (!MyValidator.isKana(v))
                                                    {
                                                        return 'カナ文字を入力してください';
                                                    }
                                                }
                                            }        
                        },
                        {field: 'operate', title: __('　'), table: table, 
                        events: Table.api.events.operate, // 定义了 单元格元素事件(删除和编辑)
                        formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index);//执行（call,后面传入this），执行 require-table 中的 api.formatter.operate 初始化
                                //call 方法可以用来代替另一个对象调用一个方法。call 方法可将一个函数的对象上下文从初始的上下文改变为由 thisObj 指定的新对象。 
                                //如果没有提供 thisObj 参数，那么 Global 对象被用作 thisObj。
                         }}
                    ]
                ]
                , onEditableSave: function (field, row, oldValue, $el) {
                            Fast.api.ajax({
                                url: "mastersetting/agentbranchmaster/fastedit/field_name/" + field,
                                data: row,
                            } //参数1---AJAX options
                            , function (data, ret) {
//                                alert(ret.msg);alert(ret.data.field_name)
                            }////参数2 ---ajax的服务端验证成功了的话要做的事情
                            , function (data, ret) {
                                alert(ret.msg);
                                $(Table.config.refreshbtn).click();

                            }////参数3 ---ajax的服务端验证失敗了的话要做的事情
                            );

                        }
                        , onLoadSuccess: function () {
                            $('.editable').editable('toggleDisabled');
                        }
            });
            
            Table.api.bindevent(table);// 为表格绑定事件
        },//admin/mastersetting/agentbranchmaster/index　自动加载内容 ↑
        
        add: function () { // 访问'mastersetting/corpmaster/add'的时候，这里的内容会被加载
            Form.api.bindevent($("form[role=form]"));//为找到本windows的表单，为表单绑定事件! 适用于require form`里头的东西， 没有这行可不能提交
                },
        edit: function () { // 访问'mastersetting/corpmaster/edit'的时候，这里的内容会被加载
            Form.api.bindevent($("form[role=form]"));
        }
        
    };
    return Controller;
});