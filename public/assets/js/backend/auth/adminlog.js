define(['jquery', 'bootstrap', 'backend', 'table', 'form'],
//加载 'jquery', 'bootstrap'，
function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {//admin/auth/adminlog　自动加载内容 ↓
            Table.api.init({// 初始化表格参数配置
                extend: {
                    index_url: 'auth/adminlog/index',
                    add_url: '',
                    edit_url: '',
                    del_url: '',//del_url: 'auth/adminlog/del',自动出del按钮
                    multi_url: '',//multi_url: 'auth/adminlog/multi',
                }
            });

            var table = $("#table");

            //console.info('★ ★ ★ ★ ★ ★ '+$.fn.bootstrapTable.defaults.extend.index_url+'★ ★ ★ ★ ★ ★ ');

            table.bootstrapTable({// 初始化表格
                url: $.fn.bootstrapTable.defaults.extend.index_url,//表格的参数定义在 jQuery.fn.bootstrapTable.defaults    Bootstrap-table 基础配置 , extend.index_url 其实就是上面定义的那个 'auth/adminlog/index'
                columns: [
                    [
                        //{field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID', operate: false},
                        {field: 'username', title: __('担当者名'), operate: false },//, operate: false 是不参加搜索的意思
                        {field: 'title', title: __('タイトル'), operate: 'LIKE %...%', placeholder: '曖昧検索'},
                        {field: 'url', title: __('Url'), align: 'left', formatter: Table.api.formatter.url},
                        {field: 'ip', title: __('IP'), events: Table.api.events.ip, formatter: Table.api.formatter.search},//formatter: Table.api.formatter.search 点击可以自动搜索
                        {field: 'browser', title: __('ブラウザ'), operate: false, formatter: Controller.api.formatter.browser},// operate: false  不搜索 ，只显示浏览器信息
                        {field: 'createtime', title: __('ログ時間'), formatter: Table.api.formatter.datetime, operate: 'BETWEEN', type: 'datetime', addclass: 'datetimepicker', data: 'data-date-format="YYYY-MM-DD HH:mm:ss"'},
//                        {field: 'operate', title: __('詳細'), table: table,
//                            //events: Table.api.events.operate,
//                            buttons: [{
//                                    name: 'detail',
//                                    text: '',
//                                    icon: 'fa fa-list',//text: __('Detail'),
//                                    classname: 'btn btn-info btn-xs btn-detail btn-dialog',// 在backend.js定义了 -------点击包含.btn-dialog的元素时弹出dialog
//                                    url: 'auth/adminlog/detail'
//                                }],
//                            formatter: Table.api.formatter.operate
//                        }
                    ]
                ]
            });
            Table.api.bindevent(table);// 为表格绑定事件
        },//admin/auth/adminlog　自动加载内容 ↑
        
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));//在Bootstrap框架中，role="form"；form表单属性，类似与辅助工具，转换角色使用；role="form"定义form表单元素为form功能角色使用
            },
            formatter: {
                browser: function (value, row, index) {
                    return '<a class="btn btn-xs btn-browser">' + row.useragent.split(" ")[0] + '</a>';
                },
            },
        }
    };
    return Controller;
});