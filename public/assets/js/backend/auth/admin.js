define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        // 访问'auth/admin/index'的时候，这里的内容会被加载↓
        index: function () {
            
            Table.api.init({// 初始化表格参数配置
                extend: {
                    index_url: 'auth/admin/index',
                    add_url: 'auth/admin/add',
                    edit_url: 'auth/admin/edit',
                    del_url: 'auth/admin/del',
                    multi_url: '',
                    table_function_title:__('担当者'),
                }
            });
            var table = $("#table_admin_list");//选择器，找到id为table_admin的对象
            table.bootstrapTable({// 初始化表格
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        //{field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'username', title: __('担当者名')},
                        {field: 'nickname', title: __('表示名')},
                      // {field: 'groups_text', title: __('Group'), operate:false, formatter: Table.api.formatter.label},
                      // {field: 'email', title: __('Email')},
                      // {field: 'status', title: __("Status"), formatter: Table.api.formatter.status},
                      // {field: 'logintime', title: __('Login time'), formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('　'), table: table, 
                        events: Table.api.events.operate, // 定义了 单元格元素事件(删除和编辑)
                        formatter: function (value, row, index) {
                                if (row.id == Config.admin.id) {//现在正在用的用户不能编辑
                                    return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index);//执行（call,后面传入this），执行 require-table 中的 api.formatter.operate 初始化
                                //call 方法可以用来代替另一个对象调用一个方法。call 方法可将一个函数的对象上下文从初始的上下文改变为由 thisObj 指定的新对象。 
                                //如果没有提供 thisObj 参数，那么 Global 对象被用作 thisObj。     
                         }}
                    ]
                ]
            });
            Table.api.bindevent(table);// 为表格绑定事件
        },
        // 访问'auth/admin/index'的时候，这里的内容会被加载↑
        
         // 访问'auth/admin/add'的时候，这里的内容会被加载↓
        add: function () {
            Form.api.bindevent($("form[role=form]"));//为找到本windows的表单，为表单绑定事件! 适用于require form`里头的东西， 没有这行可不能提交
        },
         // 访问'auth/admin/add'的时候，这里的内容会被加载↑
         
         // 访问'auth/admin/edit'的时候，这里的内容会被加载↓
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        }
         // 访问'auth/admin/edit'的时候，这里的内容会被加载↑
    };
    return Controller;
});