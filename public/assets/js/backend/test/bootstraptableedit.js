define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-table-editable', 'bootstrap-editable'],
        function ($, undefined, Backend, Table, Form,BootstrapTableEditable, Bootstrap_editable) {
            // alert(Bootstrap_table_editable);
            var Controller = {
                index: function () {
                    $('#enable').click(function () {//编辑/非编辑 切换
                        $('.editable').editable('toggleDisabled');
                        if ($('#enable').html() === '閲覧モードへ')
                        {
                            $('#enable').html("編集モードへ");
                        } else
                        {
                            $('#enable').html("閲覧モードへ");
                        }
                        
                    });
                    Table.api.init({// 初始化表格参数配置
                        extend: {
                            index_url: 'test/bootstraptableedit/index',
                        }
                    });
                    var table = $("#table_edit_test");
                    table.bootstrapTable(
                            {
                                sortName: 'SortNo,DispFlg desc,ItemNo',
                                pk: 'ItemNo',
                                url: $.fn.bootstrapTable.defaults.extend.index_url, //表格的参数定义在 jQuery.fn.bootstrapTable.defaults    Bootstrap-table 基础配置 , extend.index_url 其实就是上面定义的那个 'auth/adminlog/index'
                                columns: [
                                    [
                                        
                                            {field: 'ItemNo', title: __('科目ID'), operate: 'BETWEEN', placeholder: '入金科目ID範囲検索'},
                                            {field: 'SortNo', title: __('表示順番'), operate: 'BETWEEN', placeholder: '表示順番範囲検索'
                                            , editable: {
                                                type: 'text',
                                                title: '表示順番',
                                                validate: function (v) {
                                                    if (isNaN(v))
                                                    {
                                                        return '表示順番は数字必須';
                                                    }
                                                    var age = parseInt(v);
                                                    if (age <= 0)
                                                    {
                                                        return '表示順番>=0';
                                                    }
                                                }
                                            }
                                        },
                                        
                                        {field: 'DateEditTest', title: __('DateEditTest'), operate: false
                                            , editable: {
                                                type: 'combodate',//type: 'date',
                                                format: 'YYYY/MM/DD',
                                                viewformat: "YYYY/MM/DD",
                                                template: "YYYY/MM/DD",
                                                combodate: {
                                                    firstItem: 'none',
                minYear: 2017,
                maxYear: 2030,
                minuteStep: 1
           },
                                                
                                                title: 'DateEditTest',
                                                validate: function (v) {
                                                    var date = moment(v);
                                                    if(!date.isValid())
                                                    {
                                                        return '日付エラー';
                                                    }
                                                }
                                            }
                                        },

                                        {field: 'DispFlg', title: __('表示フラグ'), operate: false
                                            , editable: {
                                                type: 'select',
                                                title: '表示フラグ',
                                                source: [{value: "0", text: "非表示"}, {value: "1", text: "表示"}]
                                            }

                                        },

                                        
                                        {field: 'ItemName', title: __('科目名'), operate: false,
                                            editable: {
                                                type: 'text',
                                                title: '科目名',
                                                validate: function (v) {
                                                    if (!v)
                                                        return '科目名空白不可';
                                                }
                                            }



                                        }, 
                                        {field: 'SalesSectionCode', title: __('売上部門'), operate: false
                                            , editable: {
                                                type: 'select',
                                                title: '売上部門',
                                                showbuttons:false,
                                                source: function () {
                                                    var result = [];
                                                    $.ajax({
                                                        url: 'test/bootstraptableedit/get_sale_dep_list',
                                                        async: false,
                                                        type: "get",
                                                        data: {},
                                                        success: function (data, status) {
                                                            $.each(data, function (key, value) {
                                                                result.push({value: key, text: value});
                                                            });
                                                        }
                                                    });
                                                    return result;
                                                }
                                            }
                                        },
                                    ]
                                ]



                                ,
                                onEditableSave: function (field, row, oldValue, $el) {
                                    $.ajax({
                                        type: "post",
                                        url: "test/bootstraptableedit/edit1",
                                        data: row,
                                        dataType: 'JSON',
                                        success: function (data, status) {
                                            if (status == "success") {
                                                //alert('data commited');
                                                Toastr.success("data commited！");
                                                //$(Table.config.refreshbtn).click();
                                                //table.bootstrapTable('refresh');

                                            }
                                        },
                                        error: function () {
                                            alert('data commit errir');
                                            //location.reload();
                                            //table.bootstrapTable('refresh');
                                            $(Table.config.refreshbtn).click();
                                        },
                                        complete: function () {

                                        }
                                    });
                                }
                                , onLoadSuccess: function () {
                                    $('.editable').editable('toggleDisabled');
                                }
                                
                            });

                    Table.api.bindevent(table);// 为表格绑定事件
                    
                }, //　自动加载内容 ↑
            };
            return Controller;
        });