define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-table-editable', 'bootstrap-editable'],
        function ($, undefined, Backend, Table, Form, BootstrapTableEditable, Bootstrap_editable) {
            // alert(Bootstrap_table_editable);
            var Controller = {
                index: function () {
                    var table = $("#wareTable");
                    table.bootstrapTable({
                        height: 700,
                        formatLoadingMessage: function () {
                            return "请稍等，正在加载中...";
                        },
                        formatNoMatches: function () {
                            return '无符合条件的记录';
                        }
                    });
                    var $add = $('#add');
                    var $remove = $('#del');
                    $remove.click(function () {
                        var ids = $.map(table.bootstrapTable('getSelections'), function (row) {
                            return row.id;
                        });
                        if (ids.length != 1) {
                            alert("请选择一行删除!");
                            return;
                        }
                        table.bootstrapTable('remove', {
                            field: 'id',
                            values: ids
                        });
                    });
                    $add.click(function () {
                        var index = table.bootstrapTable('getData').length;
                        table.bootstrapTable('insertRow', {
                            index: index,
                            row: {
                                id: index+1,
                                wareName: 'wareName'+(index+1)
                            }
                        });
                    });

                }, //　自动加载内容 ↑
            };
            return Controller;
        });