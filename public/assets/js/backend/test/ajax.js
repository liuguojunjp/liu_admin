define(['jquery', 'bootstrap', 'backend', 'table', 'form'],
//加载 'jquery', 'bootstrap'，
        function ($, undefined, Backend, Table, Form) {
            var Controller = {
                isArray: function (input) {
                    return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
                },
                validator_a: function (form, success, error, submit) {// 被bindevent调用， 此成员方法用来定义valid规则，用的库是 nice validator，这个是很重要的绑定，因为（客户端）验证成功后会submit  ajax↓
                    form.validator(// nice validator的 DOM 配置规则 ↓
                            $.extend(
                                    {//绑定表单事件,这里参考nice validator的用法，
                                        validClass: 'has-success', //给验证通过的输入框添加的class名
                                        invalidClass: 'has-error', //给验证不通过的输入框添加的class名
                                        bindClassTo: '.form-group', //设置 validClass 和 invalidClass 添加到的位置
                                        formClass: 'n-default n-bootstrap', //主题的 class 名称，添加在 form 上
                                        msgClass: 'n-right', //字段消息的 class 名称，添加在消息容器上
                                        stopOnError: true, //在第一次错误时停止验证 
                                        display: function (elem) {//自定义消息中{0}的替换字符
                                            return $(elem).closest('.form-group').find(".control-label").text().replace(/\:/, '');
                                        },
                                        target: function (input) {//自定义消息的显示位置
                                            var $formitem = $(input).closest('.form-group'),
                                                    $msgbox = $formitem.find('span.msg-box');
                                            if (!$msgbox.length) {
                                                return [];
                                            }
                                            return $msgbox;
                                        },
                                        valid: function (ret) {//  表单客户端验证通过时的回调函数， ret是表单本身↓
                                            var that = this, submitBtn = $(".layer-footer [type=submit]", form);
                                            that.holdSubmit();
                                            $(".layer-footer [type=submit]", form).addClass("disabled");
                                            //★★★★★★★★★★★★★★★★★★★★★★★★客户端验证完成，表单AJAX提交!!　★★★★★★★★★★★★★★★★★★★★★★★★↓
                                            Form.api.submit(//将要调用ajax提交
                                                    $(ret), //表单对象
                                                    function (data, ret) {//如果Form.api.submit 成功 ， 就会回调到这里， data是服务器回来的json对象，ret
                                                        that.holdSubmit(false);
                                                        submitBtn.removeClass("disabled");
                                                        if (typeof success === 'function') {// successs是表单定义的回调函数（比如index.js里头的login方法定义的Form.api.bindevent 里头定义的那个success回调）
                                                            if (false === success.call($(this), data, ret)) {//login form 是在这里进入后台index的。。，success是backend/index.js后面Form.api.bindevent定义的success函数！
                                                                return false;
                                                            }
                                                        }
                                                        var msg = ret.hasOwnProperty("msg") && ret.msg !== "" ? ret.msg : __('操作完了');//提示及关闭当前窗口
                                                        parent.Toastr.success(msg);
                                                        parent.$(".btn-refresh").trigger("click");//trigger() 方法触发被选元素的指定事件类型。
                                                        var index = parent.Layer.getFrameIndex(window.name);
                                                        parent.Layer.close(index);
                                                        return false;
                                                    },
                                                    function (data, ret) {//提交失败的回调函数
                                                        that.holdSubmit(false);
                                                        submitBtn.removeClass("disabled");
                                                        if (typeof error === 'function') {
                                                            if (false === error.call($(this), data, ret)) {
                                                                return false;
                                                            }
                                                        }
                                                    },
                                                    submit);
                                            //★★★★★★★★★★★★★★★★★★★★★★★★客户端验证完成，表单AJAX提交!!　★★★★★★★★★★★★★★★★★★★★★★★★↑
                                            return true;
                                        }//  后表单验证通过时调用此函数↑
                                    },
                                    form.data("validator-options") || {}
                            )

                            );//// nice validator的 DOM 配置规则 ↑

                },



                index: function () {//　自动加载内容 ↓
                    // Form.events.validator($("#add-form1"), 
                    Controller.validator_a($("#add-form1"),
                            function (data, ret) { //成功时候回调函数的内容·
                                if (Controller.isArray(data))
                                {
                                    $.each(data, function (index, value) {
                                        alert(index + "..." + value.RoomTypeShortName);
                                    });

                                }
                                //alert(data.name1 + '  ddd  ' + data.extend1 + '\n' + data.array1.slip + ' dfd ' + data.array1.payment);
                            },
                            );//这个定义Form的客户端校验器！而校验如果没问题，会ajax提交！




                    $('#direct_ajax').click(function () {
                        Fast.api.ajax(//★★★★★★★★★★★★调用Fast模块的Ajax请求方法,来提交！！★★★★★★★★★★★★★★★↓
                                {//参数1---AJAX options     
                                    url: '/admin/test/ajax/direct_ajax',
                                    data: $("#add-form1").serialize(),
                                    complete: function (xhr) {
                                    }
                                }, //参数1---AJAX options
                                function (data, ret) {//参数2 ---ajax的服务端验证成功了的话要做的事情↓

                                    if (data && typeof data === 'object') {

                                    }

                                }////参数2 ---ajax的服务端验证成功了的话要做的事情↑

                        , function (data, ret) {//参数3-------- ajax的服务端验证错误了,或者ajax错误的话要做的事情↓

                        }//参数3--------ajax的服务端验证错误了的话要做的事情↑
                        );
                    });


  $('#direct_ajax_json').click(function () {//这是一个原生的jquery ajax的例子
                         var index = Layer.load();//显示一个loading层
                                    $.ajax({
                                        type: "post",
                                        url: '/admin/test/ajax/direct_ajax_json',
                                        data: {fullName: "Mario Izquierdo",
    address: {
    city: "San Francisco",
    state: {
        name: "California",
        abbr: "CA"
        }
    },

    jobbies: ["code", "climbing"],

    projects: {
        '0': { name: "serializeJSON", language: "javascript", popular: "1" },
        '1': { name: "tinytest.js",   language: "javascript", popular: "0" }
    },

    selectOne: "rock",
    selectMultiple: ["red", "blue"]
},
                                        dataType: 'JSON',
                                        success: function (data, status) {
                                            Layer.close(index);//关闭loading层
                                            
                                            var ret =data; 
                                            if(typeof data !== 'object')//如果不是js对象，那认为过来的是json字符串，
                                            {
                                              ret=  JSON.parse(data);//转成 js对象
                                            }
                                            //
                                            
                                            var ret_string=JSON.stringify(ret);
                                            //alert(ret[1]);//服务端是MSalessection::get_id_name_list()的时候
                                            if (status == "success") {
                                                //alert('data commited');
                                                //Toastr.success(ret[0].text);
                                                //Toastr.success(ret);
                                                //$(Table.config.refreshbtn).click();
                                                //table.bootstrapTable('refresh');
                                            }
                                        },
                                        error: function () {
                                            Layer.close(index);//关闭loading层
                                           // alert('更新失敗');
                                            //location.reload();
                                            //table.bootstrapTable('refresh');
                                           // $(Table.config.refreshbtn).click();
                                        }
                                    });
                    });

                    $('#direct_ajax_para').click(function () {
                        Fast.api.ajax(//★★★★★★★★★★★★调用Fast模块的Ajax请求方法,来提交！！★★★★★★★★★★★★★★★↓
                                {//参数1---AJAX options     
                                    url: '/admin/test/ajax/direct_ajax_para',
                                    //data: $.param($("#add-form1").serializeArray()),
                                    data: $("#name2,#extend1").serialize(),//序列化要ajax提交给服务器的对象
                                }, //参数1---AJAX options
                                function (data, ret) {//参数2 ---ajax的服务端验证成功了的话要做的事情↓
                                    alert(ret.msg);
                                    // alert(data);
                                    if (data && typeof data === 'object') {

                                    }

                                }////参数2 ---ajax的服务端验证成功了的话要做的事情↑

                        , function (data, ret) {//参数3-------- ajax的服务端验证错误了,或者ajax错误的话要做的事情↓

                        }//参数3--------ajax的服务端验证错误了的话要做的事情↑
                        );
                
                
                
                                   
                
                
                    });


                    $('#change_url').click(function () {
                        // alert('change_url');
                        $("#add-form1").attr("action", "/admin/test/ajax/change_url");
                        //$('#test_ajax').click();
                        $('#btn_add-form1_submit').click();


//                        $("#add-form1").submit(function (e) {
//                            // alert("Submitted");
//                        });
                        $("#add-form1").attr("action", "/admin/test/ajax/add");


                    });


                    $('#part_submit').click(function () {

                        // alert('part_submit');

                        $("#add-form1").attr("action", "/admin/test/ajax/part_submit");
                        $('#extend1').attr("form", "");

                        $('#btn_add-form1_submit').click();

                        $('#extend1').attr("form", "add-form1")
                        $("#add-form1").attr("action", "/admin/test/ajax/add");
                    });


                    $('#btn_showpara').click(function () {
                        $("#div_showpara").text($("#add-form1").serialize());
                    });

                    $('#btn_closetabs').click(function () {
                        Backend.api.closetabs('test/ajax');
                    });



                }, //　自动加载内容 ↑
            };
            return Controller;
        });