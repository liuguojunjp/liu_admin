//define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'fullcalendar', 'bootbox', 'jquery-ui', 'chosen', 'gritter'],
//        function ($, undefined, Backend, Table, Form, undefined, undefined, undefined, undefined, undefined) {
            
      define(['jquery', 'bootstrap', 'backend', 'table', 'form','fullcalendar', 'bootbox', 'jquery-ui', 'touch-punch','chosen', 'gritter','gmt'],
        function ($, undefined, Backend, Table, Form,Fullcalendar,undefined,undefined,undefined,undefined,undefined,undefined) {
            
            var Controller = {
                index: function () {//　自动加载内容 ↓
                    //alert('datatable');
                    var cssfile = Config.site.cdnurl + "/assets/css/fullcalendar.css";
                    $('head').append('<link rel="stylesheet" href="' + cssfile + '" type="text/css" />');//别忘了加载 css
                    var  cssfile1 = Config.site.cdnurl + "/assets/css/jquery.gritter.css";
                    $('head').append('<link rel="stylesheet" href="' + cssfile1 + '" type="text/css" />');//别忘了加载 css
                    var   cssfile2 = Config.site.cdnurl + "/assets/css/jquery-ui-1.10.3.full.min.css";
                    $('head').append('<link rel="stylesheet" href="' + cssfile2 + '" type="text/css" />');//别忘了加载 css
                    var   cssfile3 = Config.site.cdnurl + "/assets/css/chosen.css";
                    $('head').append('<link rel="stylesheet" href="' + cssfile3 + '" type="text/css" />');//别忘了加载 css
                    var   cssfile4 = Config.site.cdnurl + "/assets/css/font-awesome.min.css";
                    $('head').append('<link rel="stylesheet" href="' + cssfile4 + '" type="text/css" />');//别忘了加载 css
                    
                     $('#btn_refresh').click(function () {
                        // location.reload();
                         $('#calendar').fullCalendar('refresh');
                         
                    });
                    
                    $('#btn_closetabs').click(function () {
                        Backend.api.closetabs('test/fullcalendar');
                    });

              
                    $('#external-events div.external-event').each(function () {
                        var eventObject = {
                            title: $.trim($(this).text()) // use the element's text as the event title
                        };
                        $(this).data('eventObject', eventObject);
                        $(this).draggable({
                            zIndex: 999,
                            revert: true, // will cause the event to go back to its
                            revertDuration: 0  //  original position after the drag
                        });
                    });
                    
          
                    var events = eval("(" + events_json + ")");//events_json 在view模板生成
                    var calendar = $('#calendar').fullCalendar({
                        buttonText: {
                            prev: '<i class="icon-chevron-left"></i>',
                            next: '<i class="icon-chevron-right"></i>'
                        },
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            //right: 'month,agendaWeek,agendaDay'
                        },
                        longPressDelay: 1,
                        events: events,
                        editable: true, //允许拖动  
                        droppable: true, // this allows things to be dropped onto the calendar !!!
                        eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) { //事件drop
                            daysEvent(event.id, dayDelta, 'drop');
                        },
                        eventResize: function (event, dayDelta, minuteDelta, revertFunc) { //事件拖动
//                console.log(event.id + "|" + dayDelta + "|" + minuteDelta);
                            daysEvent(event.id, dayDelta, 'resize')
                        },
                        drop: function (date, allDay) { // this function is called when something is dropped
//                var start = DateUtil.Format("yyyy/MM/dd hh:mm:ss", date);
                            var start = date;
                            var end = date;
                            console.log("drop2");
                            // retrieve the dropped element's stored Event Object
                            var originalEventObject = $(this).data('eventObject');
                            var $extraEventClass = $(this).attr('data-class');
                            var title = $.trim($(this).text());
                            addEvent(title, start, end, $extraEventClass)
                        },
                        selectable: true,
                        selectHelper: true,
                        select: function (start, end, allDay) {
                            bootbox.prompt("标题:", function (title) {
                                var code = $("input[name=code]:checked").val();
                                addEvent(title, start, end, code);
                            });
                            calendar.fullCalendar('unselect');
                        },
                        eventClick: function (calEvent, jsEvent, view) {

                            var code_chose = calEvent.className;
                            var radios = "<div class='radios'>\n\
<label class='label_canlendar'><input class='ace' type='radio' value='bg-gray-active color-palette' name='code' " + getChecked(code_chose, 'bg-gray-active color-palette') + "><span class='lbl'style='color:#abbac3'>灰色</span></label>\n\
<label class='label_canlendar'><input class='ace' type='radio' value='label-success' name='code' " + getChecked(code_chose, 'label-success') + "><span class='lbl'style='color:#82af6f'>绿色</span></label>\n\
<label class='label_canlendar'><input class='ace' type='radio' value='label-danger' name='code' " + getChecked(code_chose, 'label-danger') + "><span class='lbl'style='color:#d15b47'>红色</span></label>\n\
<label class='label_canlendar'><input class='ace' type='radio' value='label-info' name='code' " + getChecked(code_chose, 'label-info') + "><span class='lbl'style='color:#00c0ef'>青色</span></label>\n\
<label class='label_canlendar'><input class='ace' type='radio' value='bg-navy-active color-palette' name='code' " + getChecked(code_chose, 'bg-navy-active color-palette') + "><span class='lbl'style='color:#00c0ef'>Navy</span></label>\n\
<label class='label_canlendar'><input class='ace' type='radio' value='bg-yellow-active color-palette' name='code' " + getChecked(code_chose, 'bg-yellow-active color-palette') + "><span class='lbl'style='color:#00c0ef'>黄色</span></label>\n\
</div>";
                            var form = $("<form class='form-inline'><label>标题：</label><input class='middle' autocomplete=off type=text value='" + calEvent.title + "' /></form>");
                            form.append(radios);
                            form.append("<p class='center' style='margin:20px 0 0'><button type='submit' style='margin:20px 0 0' class='btn btn-sm btn-success'><i class='icon-ok'></i> Save</button></p>");
                            var div = bootbox.dialog({
                                message: form,
                                buttons: {
                                    "削除": {
                                        "label": "<i class='icon-trash'></i> Delete Event",
                                        "className": "btn-sm btn-danger",
                                        "callback": function () {
                                            $("#calendar").fullCalendar("removeEvents", calEvent._id);
                                            delEvent(calEvent._id);
                                        }
                                    },
                                    "閉じる": {
                                        "label": "<i class='icon-remove'></i> Close",
                                        "className": "btn-sm"
                                    }
                                }
                            });
                            form.on('submit', function () {
                                var code = $("input[name=code]:checked").val();
                                calEvent.title = form.find("input[type=text]").val();
                                calEvent.className = code;
                                calendar.fullCalendar('updateEvent', calEvent);
                                div.modal("hide");
                                editEvent(calEvent.id, calEvent.title, calEvent.start, calEvent.end, code)
                                return false;
                            });
                        }
                    });

                    function getUrl(strs) {
                        var url =  "/" + strs;
                        return url;
                    }
                    //两个值比较，若是相等则选中
                    function getChecked(v, v2) {
                        if (v == v2) {
                            return "checked";
                        } else {
                            return "";
                        }
                    }
                    //判断日期是否是全天
                    function getAllDay(start, end) {
                        var start_his = start.indexOf("00:00:00");
                        var end_his = end.indexOf("00:00:00");
                        if (start_his == -1 || end_his == -1) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                    //编辑修改
                    function editEvent(id, title, start, end, code) {
                        if (title == null || title == '') {
                            return false;
                        }
                        if (checkStr(title) == false) {
                            alert("invalid str！");
                            return;
                        }
                        start = DateUtil.Format("yyyy/MM/dd hh:mm:ss", start);
                        if (end == null) {
                            end = start;
                        } else {
                            end = DateUtil.Format("yyyy/MM/dd hh:mm:ss", end);
                        }
                        $.post(getUrl("admin/test/fullcalendar/postEvent"), {id: id, title: title, start: start, end: end, code: code}, function (data) {
                        }, "json")
                    }
                    
                    
                    
                    // code类型： 拖动resize (延长或缩短天数) ，drop 整体延长或缩短天数
                    function daysEvent(id, days, code) {
                        $.post(getUrl("admin/test/fullcalendar/daysEvent"), {id: id, days: days, code: code}, function (data) {

                        })
                    }
                    //删除
                    function delEvent(id) {
                        $.post(getUrl("admin/test/fullcalendar/delEvent"), {id: id}, function (data) {
                        })
                    }
                    //添加
                    function addEvent(title, start, end, code) {
                        if (title == null || title == '') {
                            return false;
                        }
                        if (checkStr(title) == false) {
                            alert("请不要输入非法字符！");
                            return;
                        }
                        start = DateUtil.Format("yyyy/MM/dd hh:mm:ss", start);
                        if (end == null) {
                            end = start;
                        } else {
                            end = DateUtil.Format("yyyy/MM/dd hh:mm:ss", end);
                        }
                        var allDay = getAllDay(start, end);
                        $.post(getUrl("admin/test/fullcalendar/postEvent"), {id: 0, title: title, start: start, end: end, code: code}, function (data) {
                            $('#calendar').fullCalendar('renderEvent', {title: title, start: start, end: end, allDay: allDay, id: data.id, className: code}, true);
                        }, "json");
                    }
                    function checkStr(str) {
                        var pattern = /[~#^$@%&!*'"]/gi;
                        if (pattern.test(str)) {
                            return false;
                        }
                        return true;
                    }
                }, //　自动加载内容 ↑

            };
            return Controller;
        });