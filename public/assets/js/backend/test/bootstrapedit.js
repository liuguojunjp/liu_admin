define(['jquery', 'bootstrap', 'backend', 'table', 'bootstrap-editable'],
        function ($, undefined, Backend, Table, Bootstrap_editable) {

            var Controller = {
                index: function () {//　自动加载内容 ↓

//                    var cssfile = Config.site.cdnurl + "/assets/libs/x-editable-develop/dist/bootstrap3-editable/css/bootstrap-editable.css";
//                    $('head').append('<link rel="stylesheet" href="' + cssfile + '" type="text/css" />');//别忘了加载 css

                    $('#username').editable({
                        type: "text", //编辑框的类型。支持text|textarea|select|date|checklist等
                        title: "用户名", //编辑框的标题
                        disabled: false, //是否禁用编辑
                        emptytext: "空文本", //空值的默认文本
                        mode: "popup", //编辑框的模式：支持popup和inline两种模式，默认是popup
                        validate: function (value) { //字段验证
                            if (!$.trim(value)) {
                                return '不能为空';
                            }
                        }
                    });


                    $('#department').editable({
                        type: "select", //编辑框的类型。支持text|textarea|select|date|checklist等
                        source: [{value: 1, text: "開発部門"}, {value: 2, text: "営業部門"}, {value: 3, text: "総務部門"}],
                        title: "选择部门", //编辑框的标题
                        disabled: false, //是否禁用编辑
                        emptytext: "空文本", //空值的默认文本
                        mode: "popup", //编辑框的模式：支持popup和inline两种模式，默认是popup
                        validate: function (value) { //字段验证
                            if (!$.trim(value)) {
                                return '不能为空';
                            }
                        }
                    });

                    var curRow = {};
                    Table.api.init({// 初始化表格参数配置
                        extend: {
                            index_url: 'test/bootstrapedit/index',
                        }
                    });

                    var table = $("#table1");
                    //console.info('★ ★ ★ ★ ★ ★ '+$.fn.bootstrapTable.defaults.extend.index_url+'★ ★ ★ ★ ★ ★ ');
                    table.bootstrapTable({// 初始化表格

                        sortName: 'RoomTypeNo',
                        pk: 'RoomTypeNo',
                        url: $.fn.bootstrapTable.defaults.extend.index_url, //表格的参数定义在 jQuery.fn.bootstrapTable.defaults    Bootstrap-table 基础配置 , extend.index_url 其实就是上面定义的那个 'auth/adminlog/index'
                        columns: [
                            [
                                {
                                    checkbox: true
                                },
                                {field: 'RoomTypeNo', title: __('部屋タイプID'), operate: 'BETWEEN', placeholder: '部屋タイプID範囲検索'},
                                {field: 'RoomTypeShortName',
                                    title: __('部屋タイプ'),
                                    operate: false,
                                    formatter: function (value, row, index) {
                                        return "<a href=\"#\" name=\"RoomTypeShortName\" data-type=\"text\" data-pk=\"" + row.Id + "\" data-title=\"部屋タイプ\">" + value + "</a>";
                                    }
                                },

                                {field: 'operate', title: __('　'), table: table,
                                    events: Table.api.events.operate, // 定义了 单元格元素事件(删除和编辑)
                                    formatter: function (value, row, index) {
                                        return Table.api.formatter.operate.call(this, value, row, index);//执行（call,后面传入this），执行 require-table 中的 api.formatter.operate 初始化
                                        //call 方法可以用来代替另一个对象调用一个方法。call 方法可将一个函数的对象上下文从初始的上下文改变为由 thisObj 指定的新对象。 
                                        //如果没有提供 thisObj 参数，那么 Global 对象被用作 thisObj。
                                    }}
                            ]
                        ]
                        ,
                        onClickRow: function (row, $element) {
                            curRow = row;
                        },
                        onLoadSuccess: function (aa, bb, cc) {
                            $("#table1 a").editable({
                                url: function (params) {
                                    var sName = $(this).attr("name");
                                    curRow[sName] = params.value;
                                    $.ajax({
                                        type: 'POST',
                                        url: "test/bootstrapedit/edit1",
                                        data: curRow,
                                        dataType: 'JSON',
                                        success: function (data, textStatus, jqXHR) {
                                            alert('save ok！');
                                        },
                                        error: function () {
                                            alert("error");
                                        }
                                    });
                                },
                                type: 'text'
                            });
                        },

                    });
                    Table.api.bindevent(table);// 为表格绑定事件
                }, //　自动加载内容 ↑
            };
            return Controller;
        });