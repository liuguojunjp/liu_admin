define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'upload'], function ($, undefined, Backend, Table, Form, Upload) {
    var Controller = {
        index: function () {
            Table.api.init({// 初始化表格参数配置
                search: false,
                advancedSearch: true,
                pagination: true,
                extend: {
                    "index_url": "general/profile/index",
                    "add_url": "",
                    "edit_url": "",
                    "del_url": "",
                    "multi_url": "",
                }
            });

            var table = $("#table");
            table.bootstrapTable({// 初始化表格
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'id', title: 'ID',operate: 'BETWEEN',},
                        {field: 'title', title: __('タイトル'),operate: 'LIKE %...%'},
                        {field: 'url', title: __('Url'), align: 'left', formatter: Table.api.formatter.url,operate: 'LIKE %...%'},
                        {field: 'ip', title: __('ip'),operate: 'LIKE %...%'},
                        {field: 'createtime', title: __('時間'), formatter: Table.api.formatter.datetime,operate: 'LIKE %...%'},
                    ]
                ],
                commonSearch: true
            });

            Table.api.bindevent(table);//当内容渲染完成后  // 为表格绑定事件
            $("#plupload-avatar").data("upload-success", function (data) {// 给上传按钮添加上传成功事件
                var url = Backend.api.cdnurl(data.url);
                $(".profile-user-img").prop("src", url);
                Toastr.success("アップロード成功！");
            });
            
            Form.api.bindevent($("#profile_update-form"), function () {// 给表单绑定事件
                $("input[name='row[cur_password]']").val('');
                $("input[name='row[password]']").val('');//密码框空白
                var url = Backend.api.cdnurl($("#c-avatar").val());
                top.window.$(".user-panel .image img,.user-menu > a > img,.user-header > img").prop("src", url);//更新画像
                return true;
            });
        },
    };
    return Controller;
});