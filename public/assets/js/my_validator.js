define([], function () {
    var MyVaildator = { 
        isKana: function (str) {
                str = (str == null) ? "" : str;
                if (str.match(/^[ァ-ヶー -~｡-ﾟa-z A-Z 0-9 　 ａ-ｚ Ａ-Ｚ ０-９]*$/)) {    //"ー"の後ろの文字は全角スペースです。
                    return true;
                } else {
                    return false;
                }
            }
            , isZenKatakana: function (str) {
                str = (str == null) ? "" : str;
                if (str.match(/^[ァ-ヶー　]*$/)) {    //"ー"の後ろの文字は全角スペースです。
                    return true;
                } else {
                    return false;
                }
            }
            , isHanKana: function (str) {
                var reg = new RegExp(/^[ｦ-ﾟ]*$/);
                return reg.test(str);
            }
            , isHiragana: function (str) {
                str = (str == null) ? "" : str;
                if (str.match(/^[ぁ-んー　]*$/)) {    //"ー"の後ろの文字は全角スペースです。
                    return true;
                } else {
                    return false;
                }
            }
             , isTel: function (str) {
                str = (str == null) ? "" : str;
                if (str.match(/^[0-9\-]+$/)) {    //"ー"の後ろの文字は全角スペースです。
                    return true;
                } else {
                    return false;
                }
            },
            isMail: function (str) {
            str = (str == null) ? "" : str;
            var mail_regex1 = new RegExp('(?:[-!#-\'*+/-9=?A-Z^-~]+\.?(?:\.[-!#-\'*+/-9=?A-Z^-~]+)*|"(?:[!#-\[\]-~]|\\\\[\x09 -~])*")@[-!#-\'*+/-9=?A-Z^-~]+(?:\.[-!#-\'*+/-9=?A-Z^-~]+)*');
            var mail_regex2 = new RegExp('^[^\@]+\@[^\@]+$');
            if (str.match(mail_regex1) && str.match(mail_regex2)) {
                // 全角チェック
                if (str.match(/[^a-zA-Z0-9\!\"\#\$\%\&\'\(\)\=\~\|\-\^\\\@\[\;\:\]\,\.\/\\\<\>\?\_\`\{\+\*\} ]/)) {
                    return false;
                }
                // 末尾TLDチェック（〜.co,jpなどの末尾ミスチェック用）
                if (!str.match(/\.[a-z]+$/)) {
                    return false;
                }
                return true;
            } else {
                return false;
            }

            }

    };
    return MyVaildator;//返回对象，别的地方如果加载了这个js，就可以用 返回值 来使用这个js库
});// -----------end of define(['fast', 'moment'], function (Fast, moment)