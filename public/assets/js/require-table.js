define(['jquery', 'bootstrap', 
'moment',
//'moment/locale/zh-cn', 
'moment/locale/ja', 
'bootstrap-table', 'bootstrap-table-lang', 'bootstrap-table-mobile', 'bootstrap-table-export', 'bootstrap-table-commonsearch', 'bootstrap-table-template'], 

function ($, undefined, moment) {
    var Table = {//
        list: {},        
        defaults: {// Bootstrap-table 基础配置　↓
            url: '',//url一般是请求后台的url地址,调用ajax获取数据。
            sidePagination: 'server', //分页方式：client客户端分页，server服务端分页（*）
            method: 'get', //请求方法 //使用get请求到服务器获取数据
            dataType: "json",//数据类型 json
            contentType: 'application/json,charset=utf-8',
            toolbar: ".toolbar", //工具栏 //一个jQuery 选择器，指明自定义的toolbar 例如:#toolbar, .toolbar.
            search: false, //是否启用快速搜索, 20180806 `还是不用了
            cache: false, // 不缓存
            commonSearch: true, //是否启用通用搜索        
            //uniqueId: "id",                    //每一行的唯一标识，一般为主键列
            //height: document.body.clientHeight-165,   //动态获取高度值，可以使表格自适应页面
            //striped: true,                      // 隔行加亮
            //queryParamsType: "limit",           //设置为"undefined",可以获取pageNumber，pageSize，searchText，sortName，sortOrder 
            // sortable: true,                     //是否启用排序;意味着整个表格都会排序
            searchFormVisible: false, //是否始终显示搜索表单
            titleForm: '', //为空则不显示标题，不定义默认显示：普通搜索
            idTable: 'commonTable',//不知道什么意思
            
            showExport: false,//showExport: true,//表格控件的输出表示,不用了
            //exportDataType: "all",
            //exportTypes: ['json', 'xml', 'csv', 'txt', 'doc', 'excel'],
            
            pageSize: 10,
            pageList: [10, 25,50,100,500],// 20181031 'All'的时候，日语并且linux的时候出现问题，post limit是 'NaN', 没时间研究，把·all换成 100回避问题。
            pagination: true,//分页
            clickToSelect: true, //是否启用点击选中
            singleSelect: false, //是否启用单选
            showRefresh: false,
            //locale: 'zh-CN',
            locale: 'ja-JP',//注意和require-backend.js 的 'bootstrap-table-lang': '../libs/bootstrap-table/dist/locale/bootstrap-table-ja-JP', 对应
            //locale: requirejs.s.contexts._.config.config.language,
            showToggle: true,//是否显示详细视图和列表视图.
            showColumns: true,//是否显示所有的列
            pk: 'id',//主键，定义为id
            sortName: 'id',//排序，定义为id
            sortOrder: 'asc', //排序方式
            paginationFirstText: __("最初"),//First
            paginationPreText: __("前"),//Previous
            paginationNextText: __("次"),//Next
            paginationLastText: __("最後"),//Last
            mobileResponsive: true, //是否自适应移动端
            cardView: false, //（初始）卡片视图
            checkOnInit: true, //是否在初始化时判断
            escape: true, //是否对内容进行转义
            extend: {
                index_url: '',
                add_url: '',
                edit_url: '',
                del_url: '',
                import_url: '',
                multi_url: '',
                dragsort_url: 'ajax/weigh',
            }
        },// Bootstrap-table 基础配置　↑
       
        columnDefaults: { // Bootstrap-table 列配置
            align: 'center',
            valign: 'middle',
        },
        
        config: {
            firsttd: 'tbody tr td:first-child:not(:has(div.card-views))',
            toolbar: '.toolbar',
            refreshbtn: '.btn-refresh',
            addbtn: '.btn-add',
            editbtn: '.btn-edit',
            delbtn: '.btn-del',
            importbtn: '.btn-import',
            multibtn: '.btn-multi',
            disabledbtn: '.btn-disabled',
            editonebtn: '.btn-editone',//双击单元格的时候用？
            
            dragsortfield: 'weigh',
        },
        
        api: {//api: 成员↓
            
            init: function (defaults, columnDefaults, locales) {//初始化表格参数配置, 编程的时候用 Table.api.init(
                
                defaults = defaults ? defaults : {};
                columnDefaults = columnDefaults ? columnDefaults : {};
                locales = locales ? locales : {};
                
                $.extend(true, $.fn.bootstrapTable.defaults, Table.defaults, defaults);// 写入bootstrap-table默认配置
                
                $.extend($.fn.bootstrapTable.columnDefaults, Table.columnDefaults, columnDefaults);// 写入bootstrap-table column配置
               
                $.extend($.fn.bootstrapTable.locales[Table.defaults.locale], { // 写入bootstrap-table locale配置
                    formatCommonSearch: function () {
                        return __('Common search');
                    },
                    formatCommonSubmitButton: function () {
                        return __('Submit');
                    },
                    formatCommonResetButton: function () {
                        return __('Reset');
                    },
                    formatCommonCloseButton: function () {
                        return __('Close');
                    },
                    formatCommonChoose: function () {
                        return __('Choose');
                    }
                }, locales);
            },
            
            bindevent: function (table) {// 绑定事件↓
                var parenttable = table.closest('.bootstrap-table');//Bootstrap-table的父元素,包含table,toolbar,pagnation　（closest() 方法获得匹配选择器的第一个祖先元素，从当前元素开始沿 DOM 树向）
                
                var options = table.bootstrapTable('getOptions');//获得Bootstrap-table的配置
                
                var toolbar = $(options.toolbar, parenttable);//Bootstrap操作区
                
                table.on('load-error.bs.table', function (status, res) {//当刷新表格时
                    Toastr.error(__('Unknown data format'));
                });
                
                table.on('refresh.bs.table', function (e, settings, data) {
                    $(Table.config.refreshbtn, toolbar).find(".fa").addClass("fa-spin");//当刷新表格时
                });
                
                table.on('dbl-click-row.bs.table', function (e, row, element, field) {//当双击单元格时
                    $(Table.config.editonebtn, element).trigger("click");
                });
                
                table.on('post-body.bs.table', function (e, settings, json, xhr) {//当内容渲染完成后
                    $(Table.config.refreshbtn, toolbar).find(".fa").removeClass("fa-spin");
                    $(Table.config.disabledbtn, toolbar).toggleClass('disabled', true);
                    if ($(Table.config.firsttd, table).find("input[type='checkbox'][data-index]").size() > 0) {
                        // 挺拽选择,需要重新绑定事件
                        require(['drag', 'drop'], function () {
                            $(Table.config.firsttd, table).drag("start", function (ev, dd) {
                                return $('<div class="selection" />').css('opacity', .65).appendTo(document.body);
                            }).drag(function (ev, dd) {
                                $(dd.proxy).css({
                                    top: Math.min(ev.pageY, dd.startY),
                                    left: Math.min(ev.pageX, dd.startX),
                                    height: Math.abs(ev.pageY - dd.startY),
                                    width: Math.abs(ev.pageX - dd.startX)
                                });
                            }).drag("end", function (ev, dd) {
                                $(dd.proxy).remove();
                            });
                            $(Table.config.firsttd, table).drop("start", function () {
                                Table.api.toggleattr(this);
                            }).drop(function () {
                                Table.api.toggleattr(this);
                            }).drop("end", function () {
                                Table.api.toggleattr(this);
                            });
                            $.drop({
                                multi: true
                            });
                        });
                    }
                });
                
                table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table fa.event.check', function () {// 处理选中筛选框后按钮的状态统一变更
                    var ids = Table.api.selectedids(table);
                    $(Table.config.disabledbtn, toolbar).toggleClass('disabled', !ids.length);
                });
                
                $(toolbar).on('click', Table.config.refreshbtn, function () {// 刷新按钮事件
                    table.bootstrapTable('refresh');
                });
              
                        $(toolbar).on('click', Table.config.addbtn, function () { // 添加add按钮事件   //Table.config.addbtn 具体值为 .btn-add
                            var ids = Table.api.selectedids(table);
                            var width = 800;
                            var height = 600;
                            var isFull=false;
                            if (options.extend.table_function_title === __('担当者')//table.attr('id') === 'table_admin_list' でもいい   
                                    )
                            {
                                width = 500;
                                height = 350;
                            } else if (table.attr('id') === 'table_roomtypemaster_list')
                            {
                                width = 400;
                                height = 300;
                            } else if (table.attr('id') === 'table_roommaster_list')
                            {
                                width = 400;
                                height = 350;
                            }
                            
                            else if (table.attr('id') === 'table_client_list')
                            {
                                isFull=true;
                            }
                            var area = [$(window).width() > width ? width + 'px' : '95%', $(window).height() > height ? height + 'px' : '95%'];
                            Fast.api.open(
                                    options.extend.add_url + (ids.length > 0 ? (options.extend.add_url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + ids.join(",") : ''), //Fast.api.openのパラ---url
                                    (typeof options.extend.table_function_title === "undefined" ? "" : options.extend.table_function_title) + __('追加'), //Fast.api.openのパラ--- title, 
                                    $(this).data() || {}, //Fast.api.openのパラ----options
                                    area,isFull?'true':'false'
                                    );
                        });
               
//                if ($(Table.config.importbtn, toolbar).size() > 0) { // 导入按钮事件
//                    require(['upload'], function (Upload) {
//                        Upload.api.plupload($(Table.config.importbtn, toolbar), function (data, ret) {
//                            Fast.api.ajax({
//                                url: options.extend.import_url,
//                                data: {file: data.url},
//                            }, function () {
//                                table.bootstrapTable('refresh');
//                            });
//                        });
//                    });
//                }
                
               
//                $(toolbar).on('click', Table.config.editbtn, function () { // 批量编辑按钮事件
//                    var ids = Table.api.selectedids(table);
//                    var that = this;
//                    
//                    $.each(ids, function (i, j) {//循环弹出多个编辑框
//                        Fast.api.open(options.extend.edit_url + (options.extend.edit_url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + j, __('Edit'), $(that).data() || {});
//                    });
//                });
//               
//                $(toolbar).on('click', Table.config.multibtn, function () { // 批量操作按钮事件
//                    var ids = Table.api.selectedids(table);
//                    Table.api.multi($(this).data("action"), ids, table, this);
//                });
                
//                $(toolbar).on('click', Table.config.delbtn, function () {// 批量删除按钮事件
//                    var that = this;
//                    var ids = Table.api.selectedids(table);
//                    var index = Layer.confirm(
//                            __('Are you sure you want to delete the %s selected item?', ids.length),
//                            {icon: 3, title: __('Warning'), offset: 0, shadeClose: true},
//                            function () {
//                                Table.api.multi("del", ids, table, that);
//                                Layer.close(index);
//                            }
//                    );
//                });
                
                require(['dragsort'], function () {// 拖拽排序
                    $("tbody", table).dragsort({//绑定拖动排序
                        itemSelector: 'tr',
                        dragSelector: "a.btn-dragsort",
                        dragEnd: function () {
                            var data = table.bootstrapTable('getData');
                            var current = data[parseInt($(this).data("index"))];
                            var options = table.bootstrapTable('getOptions');
                            
                            var ids = $.map($("tbody tr:visible", table), function (tr) {//改变的值和改变的ID集合
                                return data[parseInt($(tr).data("index"))][options.pk];
                            });
                            var changeid = current[options.pk];
                            var pid = typeof current.pid != 'undefined' ? current.pid : '';
                            var params = {
                                url: table.bootstrapTable('getOptions').extend.dragsort_url,
                                data: {
                                    ids: ids.join(','),
                                    changeid: changeid,
                                    pid: pid,
                                    field: Table.config.dragsortfield,
                                    orderway: options.sortOrder,
                                    table: options.extend.table
                                }
                            };
                            Fast.api.ajax(params, function (data) {
                                table.bootstrapTable('refresh');
                            });
                        },
                        placeHolderTemplate: ""
                    });
                });
                
                $(table).on("click", "input[data-id][name='checkbox']", function (e) {
                    table.trigger('fa.event.check');
                });
                $(table).on("click", "[data-id].btn-change", function (e) {
                    e.preventDefault();
                    Table.api.multi($(this).data("action") ? $(this).data("action") : '', [$(this).data("id")], table, this);
                });
                
                $(table).on("click", "[data-id].btn-edit", function (e) {
                    e.preventDefault();
                   // Fast.api.open(options.extend.edit_url + (options.extend.edit_url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + $(this).data("id"), __('Edit'), $(this).data() || {});
                });
                
                $(table).on("click", "[data-id].btn-del", function (e) {
//                    e.preventDefault();
//                    var id = $(this).data("id");
//                    var that = this;
//                    var index = Layer.confirm(
//                            __('Are you sure you want to delete this item?'),
//                            {icon: 3, title: __('Warning'), shadeClose: true},
//                            function () {
//                                Table.api.multi("del", id, table, that);
//                                Layer.close(index);
//                            }
//                    );
                });
                var id = table.attr("id");
                Table.list[id] = table;
                return table;
            },//绑定事件·↑
            
            
            multi: function (action, ids, table, element,password) {// 批量操作请求
                
                var options = table.bootstrapTable('getOptions');
                var data = element ? $(element).data() : {};
                var url = typeof data.url !== "undefined" ? data.url : ((action === "del"||action === "del_password" ) ? options.extend.del_url : options.extend.multi_url);
                
                url = url + (url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + ($.isArray(ids) ? ids.join(",") : ids);
                if(action === "del_password")
                {
                    url = url + (url.match(/(\?|&)+/) ? "&password=" : "/password/") + password;
                }
                var params = typeof data.params !== "undefined" ? (typeof data.params == 'object' ? $.param(data.params) : data.params) : '';
                //alert(url);
                var options = {url: url, data: {action: action, ids: ids, params: params}};
                
                Fast.api.ajax(options, //发送ajax
                function (data) {//ajax完成后回调，
                    table.bootstrapTable('refresh');//刷新表格
                });  
            },
           
            events: { // 单元格元素事件(删除和编辑)
                operate: {
                    'click .btn-editone': function (e, value, row, index) {//编辑单条记录的代码
                        e.stopPropagation();
                        e.preventDefault();
                                var that = this;
                                var top = $(that).offset().top - $(window).scrollTop();
                                var left = $(that).offset().left - $(window).scrollLeft() - 260;
                                if (top + 154 > $(window).height()) {
                                    top = top - 154;
                                }
                                if ($(window).width() < 480) {
                                    top = left = undefined;
                                }

                                var parent_table_id = $(that).closest('table').attr('id');
                                if (parent_table_id === 'table_admin_list')
                                {
                                    var index = Layer.prompt({
                                        formType: 1, //パスワード
                                        value: '',
                                        title: __('パスワードを入力してください'),
                                        icon: 3,
                                        offset: [top, left],
                                        shadeClose: true
                                    }, function (value, index, elem) {
                                        layer.close(index);//关掉密码输入框
                                        var ajax_options = {url: 'auth/admin/check_edit_pass', data: {password: value, id: row['id']}};
                                        Fast.api.ajax(ajax_options, //发送ajax
                                                function (data,response) {//ajax完成后回调，
                                                    ret = Fast.events.onAjaxResponse(response);
                                                    if (ret.code === 1) {
                                                        var options = $(that).closest('table').bootstrapTable('getOptions');
                                                        width = 400;
                                                        height = 300;
                                                        var area = [$(window).width() > width ? width + 'px' : '95%', $(window).height() > height ? height + 'px' : '95%'];
                                                        Fast.api.open(options.extend.edit_url + (options.extend.edit_url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + row[options.pk], __('編集'), $(that).data() || {},area);
                                                    } 
                                                }
                                        );  
                                       
                                    });
                                }
                                else if (parent_table_id === 'table_roomtypemaster_list'||parent_table_id === 'table_roommaster_list')
                                {
                                    var options = $(that).closest('table').bootstrapTable('getOptions');
                                                        width = 400;
                                                        height = 300;
                                                        var area = [$(window).width() > width ? width + 'px' : '95%', $(window).height() > height ? height + 'px' : '95%'];
                                                        Fast.api.open(options.extend.edit_url + (options.extend.edit_url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + row[options.pk], __('部屋タイプマスタ編集'), $(that).data() || {},area);
                                
                                }
                                else if (parent_table_id === 'table_agentbranchmaster_list' || parent_table_id === 'table_freeitemmaster_list')
                                {
                                    var options = $(that).closest('table').bootstrapTable('getOptions');
                                    width = 800;
                                    height = 600;
                                    var area = [$(window).width() > width ? width + 'px' : '95%', $(window).height() > height ? height + 'px' : '95%'];
                                    var arr = [row[options.pk0], row[options.pk1]];
                                    Fast.api.open(options.extend.edit_url + (options.extend.edit_url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + arr.join(","),
                                   (typeof options.extend.table_function_title === "undefined" ? "" : options.extend.table_function_title)+  __('編集'), 
                                    $(that).data() || {},
                                    area);
                                }
                                else if (parent_table_id === 'table_taxmaster_list' )
                                {
                                    var options = $(this).closest('table').bootstrapTable('getOptions');
                                    width = 500;
                                    height = 400;
                                    var area = [$(window).width() > width ? width + 'px' : '95%', $(window).height() > height ? height + 'px' : '95%'];
                                    Fast.api.open(options.extend.edit_url + (options.extend.edit_url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + new Date(row[options.pk]).format("yyyy-MM-dd"),
                                    __('tax編集'), 
                                    $(that).data() || {},area);

                                }
                                else if (parent_table_id === 'table_client_list' )
                                {
                                    var options = $(this).closest('table').bootstrapTable('getOptions');
                                   
                                    Fast.api.open(options.extend.edit_url + (options.extend.edit_url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + row[options.pk],
                                    __('顧客編集'), 
                                    $(that).data() || {},area,'true');

                                }
                                
                                else
                                {
                                    var options = $(this).closest('table').bootstrapTable('getOptions');
                                    Fast.api.open(options.extend.edit_url + (options.extend.edit_url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + row[options.pk],
                                    __('編集'), 
                                    $(that).data() || {});
                                }
                    },
                    
                    'click .btn-delone': function (e, value, row, index) {//删除单条记录的代码
                        e.stopPropagation();
                        e.preventDefault();
                        var that = this;
                        var top = $(that).offset().top - $(window).scrollTop();
                                    var left = $(that).offset().left - $(window).scrollLeft() - 260;
                                    if (top + 154 > $(window).height()) {
                                        top = top - 154;
                                    }
                                    if ($(window).width() < 480) {
                                        top = left = undefined;
                                    }
                        
                              //  var parent_table_id = $(this).parents('table').attr('id');
                                var parent_table_id = $(that).closest('table').attr('id');
                                if (parent_table_id === 'table_admin_list')
                                {
                                    var index = Layer.prompt({
                                        formType: 1,//パスワード
                                        value: '',
                                        title: __('パスワードを入力してください'),
                                        icon: 3,
                                        offset: [top, left],
                                        shadeClose: true
                                    }, function (value, index, elem) {
                                        //alert(value); //得到value
                                        var table = $(that).closest('table');
                                        var options = table.bootstrapTable('getOptions');
                                        Table.api.multi("del_password", row['id'], table, that,value);//调用multi（批量操作请求）方法、发ajax删除
                                        layer.close(index);//关掉密码输入框
                                    });
                                } 
                                else
                                {
                                    var index = Layer.confirm(
                                            __('本当に削除しますか?'),
                                            {icon: 3, title: __('確認'), offset: [top, left], shadeClose: true},
                                            function () {//确认后的回调
                                                var table = $(that).closest('table');
                                                var options = table.bootstrapTable('getOptions');
                                                
                                                if (parent_table_id === 'table_agentbranchmaster_list' || parent_table_id === 'table_freeitemmaster_list')
                                                {
                                                    var arr = [row[options.pk0],row[options.pk1]];
                                                    Table.api.multi("del", arr, table, that);//调用multi（批量操作请求）方法、发ajax删除
                                                } 
                                                else
                                                {
                                                // alert(row[options.pk]);
                                                    Table.api.multi("del", row[options.pk], table, that);//调用multi（批量操作请求）方法、发ajax删除
                                                }               
                                                Layer.close(index);//关掉确认框
                                            }
                                    );
                                }
                    }
                    
                    
                }
            },
            
            formatter: {// 单元格数据格式化
                icon: function (value, row, index) {
                    if (!value)
                    {
                        return '';
                    }
                    value = value.indexOf(" ") > -1 ? value : "fa fa-" + value;
                    return '<i class="' + value + '"></i> ' + value;//渲染fontawesome图标
                },
                image: function (value, row, index) {
                    value = value ? value : '/assets/img/blank.gif';
                    var classname = typeof this.classname !== 'undefined' ? this.classname : 'img-sm img-center';
                    return '<img class="' + classname + '" src="' + Fast.api.cdnurl(value) + '" />';
                },
                images: function (value, row, index) {
                    value = value.toString();
                    var classname = typeof this.classname !== 'undefined' ? this.classname : 'img-sm img-center';
                    var arr = value.toString().split(',');
                    var html = [];
                    $.each(arr, function (i, value) {
                        value = value ? value : '/assets/img/blank.gif';
                        html.push('<img class="' + classname + '" src="' + Fast.api.cdnurl(value) + '" />');
                    });
                    return html.join(' ');
                },
                status: function (value, row, index) {
                    //颜色状态数组,可使用red/yellow/aqua/blue/navy/teal/olive/lime/fuchsia/purple/maroon
                    var colorArr = {normal: 'success', hidden: 'grey', deleted: 'danger', locked: 'info'};
                    //如果字段列有定义custom
                    if (typeof this.custom !== 'undefined') {
                        colorArr = $.extend(colorArr, this.custom);
                    }
                    value = value.toString();
                    var color = value && typeof colorArr[value] !== 'undefined' ? colorArr[value] : 'primary';
                    value = value.charAt(0).toUpperCase() + value.slice(1);
                    //渲染状态
                    var html = '<span class="text-' + color + '"><i class="fa fa-circle"></i> ' + __(value) + '</span>';
                    return html;
                },
                url: function (value, row, index) {
                    //return '<div class="input-group input-group-sm" style="width:250px;"><input type="text" class="form-control input-sm" value="' + value + '"><span class="input-group-btn input-group-sm"><a href="' + value + '" target="_blank" class="btn btn-default btn-sm"><i class="fa fa-link"></i></a></span></div>';
                    return '<div class="input-group input-group-sm" style="width:250px;"><input type="text" class="form-control input-sm" value="' + value + '" disabled></div>';
                },
                search: function (value, row, index) {//search:
                    return '<a href="javascript:;" class="searchit" data-field="' + this.field + '" data-value="' + value + '">' + value + '</a>';
                },
                addtabs: function (value, row, index) {
                    var url = Table.api.replaceurl(this.url, value, row, this.table);
                    var title = this.atitle ? this.atitle : __("Search %s", value);
                    return '<a href="' + Fast.api.fixurl(url) + '" class="addtabsit" data-value="' + value + '" title="' + title + '">' + value + '</a>';
                },
                dialog: function (value, row, index) {
                    var url = Table.api.replaceurl(this.url, value, row, this.table);
                    var title = this.atitle ? this.atitle : __("View %s", value);
                    return '<a href="' + Fast.api.fixurl(url) + '" class="dialogit" data-value="' + value + '" title="' + title + '">' + value + '</a>';
                },
                flag: function (value, row, index) {
                    var colorArr = {index: 'success', hot: 'warning', recommend: 'danger', 'new': 'info'};
                    if (typeof this.custom !== 'undefined') {//如果字段列有定义custom
                        colorArr = $.extend(colorArr, this.custom);
                    }
                    if (typeof this.customField !== 'undefined' && typeof row[this.customField] !== 'undefined') {
                        value = row[this.customField];
                    }
                    
                    var html = [];//渲染Flag
                    var arr = value.toString().split(',');
                    $.each(arr, function (i, value) {
                        value = value.toString();
                        if (value == '')
                            return true;
                        var color = value && typeof colorArr[value] !== 'undefined' ? colorArr[value] : 'primary';
                        value = value.charAt(0).toUpperCase() + value.slice(1);
                        html.push('<span class="label label-' + color + '">' + __(value) + '</span>');
                    });
                    return html.join(' ');
                },
                label: function (value, row, index) {
                    return Table.api.formatter.flag.call(this, value, row, index);
                },
                datetime: function (value, row, index) {
                    return value ? moment(parseInt(value) * 1000).format("YYYY-MM-DD HH:mm:ss") : __('None');
                },
                operate: function (value, row, index) {
                    var table = this.table;
                    
                    var options = table ? table.bootstrapTable('getOptions') : {};// 操作配置
                    
                    var buttons = $.extend([], this.buttons || []);// 默认按钮组
                    buttons.push({name: 'dragsort', icon: 'fa fa-arrows', classname: 'btn btn-xs btn-primary btn-dragsort'});
                    buttons.push({name: 'edit', icon: 'fa fa-pencil', classname: 'btn btn-xs btn-success btn-editone', url: options.extend.edit_url});
                    buttons.push({name: 'del', icon: 'fa fa-trash', classname: 'btn btn-xs btn-danger btn-delone'});
                    var html = [];
                    var url, classname, icon, text, title, extend;
                    $.each(buttons, function (i, j) {
                        if (j.name === 'dragsort' && typeof row[Table.config.dragsortfield] === 'undefined') {
                            return true;
                        }
                        if (['add', 'edit', 'del', 'multi', 'dragsort'].indexOf(j.name) > -1 && !options.extend[j.name + "_url"]) {
                            return true;
                        }
                        var attr = table.data("operate-" + j.name);
                        if (typeof attr === 'undefined' || attr) {
                            url = j.url ? j.url : '';
                            if (url.indexOf("{ids}") === -1) {
                                url = url ? url + (url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + row[options.pk] : '';
                            }
                            url = Table.api.replaceurl(url, value, row, table);
                            url = url ? Fast.api.fixurl(url) : 'javascript:;';
                            classname = j.classname ? j.classname : 'btn-primary btn-' + name + 'one';
                            icon = j.icon ? j.icon : '';
                            text = j.text ? j.text : '';
                            title = j.title ? j.title : text;
                            extend = j.extend ? j.extend : '';
                            html.push('<a href="' + url + '" class="' + classname + '" ' + extend + ' title="' + title + '"><i class="' + icon + '"></i>' + (text ? ' ' + text : '') + '</a>');
                        }
                    });
                    return html.join(' ');
                },
                buttons: function (value, row, index) {
                    var table = this.table;
                   
                    var options = table ? table.bootstrapTable('getOptions') : {}; // 操作配置
                    
                    var buttons = $.extend([], this.buttons || []);// 默认按钮组
                    var html = [];
                    var url, classname, icon, text, title, extend;
                    $.each(buttons, function (i, j) {
                        var attr = table.data("buttons-" + j.name);
                        if (typeof attr === 'undefined' || attr) {
                            url = j.url ? j.url : '';
                            if (url.indexOf("{ids}") === -1) {
                                url = url ? url + (url.match(/(\?|&)+/) ? "&ids=" : "/ids/") + row[options.pk] : '';
                            }
                            url = Table.api.replaceurl(url, value, row, table);
                            url = url ? Fast.api.fixurl(url) : 'javascript:;';
                            classname = j.classname ? j.classname : 'btn-primary btn-' + name + 'one';
                            icon = j.icon ? j.icon : '';
                            text = j.text ? j.text : '';
                            title = j.title ? j.title : text;
                            extend = j.extend ? j.extend : '';
                            html.push('<a href="' + url + '" class="' + classname + '" ' + extend + ' title="' + title + '"><i class="' + icon + '"></i>' + (text ? ' ' + text : '') + '</a>');
                        }
                    });
                    return html.join(' ');
                }
            },
            
            replaceurl: function (url, value, row, table) {//替换URL中的{ids}和{value}
                url = url ? url : '';
                url = url.replace(/\{value\}/ig, value);
                if (table) {
                    var options = table.bootstrapTable('getOptions');
                    url = url.replace(/\{ids\}/ig, row[options.pk]);
                } else {
                    url = url.replace(/\{ids\}/ig, 0);
                }
                return url;
            },
            
            selectedids: function (table) {// 获取选中的条目ID集合
                var options = table.bootstrapTable('getOptions');
                if (options.templateView) {
                    return $.map($("input[data-id][name='checkbox']:checked"), function (dom) {
                        return $(dom).data("id");
                    });
                } else {
                    return $.map(table.bootstrapTable('getSelections'), function (row) {
                        return row[options.pk];
                    });
                }
            },            
            toggleattr: function (table) {// 切换复选框状态
                $("input[type='checkbox']", table).trigger('click');
            }
        },//api: 成员↑
        
    };// end of  var Table
    return Table;//返回对象，别的地方如果加载了这个js，就可以用 返回值 来使用这个js库
});
