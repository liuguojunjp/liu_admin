//Backend 模块(整个Fast应用程序后台用的js)
define(['fast', 'moment'], function (Fast, moment) {//依赖于 fast(.js)和moment(package)
    var Backend = {
        
        api: {//api成员↓
//            sidebar: function (params) {//api成员的sidebar成员方法 ----- 不知道在哪里被调用，拿来干什么 20180730 tempdel
//                colorArr = ['red', 'green', 'yellow', 'blue', 'teal', 'orange', 'purple'];
//                $colorNums = colorArr.length;
//                badgeList = {};
//                $.each(params, function (k, v) {
//                    $url = Fast.api.fixurl(k);
//
//                    if ($.isArray(v))
//                    {
//                        $nums = typeof v[0] !== 'undefined' ? v[0] : 0;
//                        $color = typeof v[1] !== 'undefined' ? v[1] : colorArr[(!isNaN($nums) ? $nums : $nums.length) % $colorNums];
//                        $class = typeof v[2] !== 'undefined' ? v[2] : 'label';
//                    } else
//                    {
//                        $nums = v;
//                        $color = colorArr[(!isNaN($nums) ? $nums : $nums.length) % $colorNums];
//                        $class = 'label';
//                    }
//                    //必须nums大于0才显示
//                    badgeList[$url] = $nums > 0 ? '<small class="' + $class + ' pull-right bg-' + $color + '">' + $nums + '</small>' : '';
//                });
//                $.each(badgeList, function (k, v) {
//                    var anchor = top.window.$("li a[addtabs][url='" + k + "']");
//                    if (anchor) {
//                        top.window.$(".pull-right-container", anchor).html(v);
//                        top.window.$(".nav-addtabs li a[node-id='" + anchor.attr("addtabs") + "'] .pull-right-container").html(v);
//                    }
//                });
//            },
            addtabs: function (url, title, icon) {//新增选项卡
                console.info('★★★★★★ backend.js 新增选项卡★★★★★★');
                // alert('addtabs ');
                var dom = "a[url='{url}']"
                var leftlink = top.window.$(dom.replace(/\{url\}/, url));
                if (leftlink.size() > 0) {
                    leftlink.trigger("click");
                } else {
                    url = Fast.api.fixurl(url);
                    leftlink = top.window.$(dom.replace(/\{url\}/, url));
                    if (leftlink.size() > 0) {
                        var event = leftlink.parent().hasClass("active") ? "dblclick" : "click";
                        leftlink.trigger(event);
                    } else {
                        var baseurl = url.substr(0, url.indexOf("?") > -1 ? url.indexOf("?") : url.length);
                        leftlink = top.window.$(dom.replace(/\{url\}/, baseurl));
                        //能找到相对地址
                        if (leftlink.size() > 0) {
                            icon = typeof icon !== 'undefined' ? icon : leftlink.find("i").attr("class");
                            title = typeof title !== 'undefined' ? title : leftlink.find("span:first").text();
                            leftlink.trigger("fa.event.toggleitem");
                        }
                        var navnode = top.window.$(".nav-tabs ul li a[node-url='" + url + "']");
                       // alert('navnode '+navnode.size());
                        if (navnode.size() > 0) {
                            navnode.trigger("click");
                        } else {
                            //追加新的tab(自动click一下)
                            var id = Math.floor(new Date().valueOf() * Math.random());
                            icon = typeof icon !== 'undefined' ? icon : 'fa fa-circle-o';
                            title = typeof title !== 'undefined' ? title : '';
                            top.window.$("<a />").append('<i class="' + icon + '"></i> <span>' + title + '</span>').prop("href", url)
                                    .attr({url: url, addtabs: id}).addClass("hide")
                                    .appendTo(top.window.document.body)
                                    .trigger("click");//自动click一下
                        }
                    }
                }
            },
            closetabs: function (url) {//----- 关闭选项卡  好像目前还没用到 知道在哪里被调用，拿来干什么 20180730 tempdel
                if (typeof url === 'undefined') {
                    top.window.$("ul.nav-addtabs li.active .close-tab").trigger("click");
                } else {
                    var dom = "a[url='{url}']"
                    var navlink = top.window.$(dom.replace(/\{url\}/, url));
                    if (navlink.size() === 0) {
                        url = Fast.api.fixurl(url);
                        navlink = top.window.$(dom.replace(/\{url\}/, url));
                        if (navlink.size() === 0) {
                        } else {
                            var baseurl = url.substr(0, url.indexOf("?") > -1 ? url.indexOf("?") : url.length);
                            navlink = top.window.$(dom.replace(/\{url\}/, baseurl));
                            //能找到相对地址
                            if (navlink.size() === 0) {
                                navlink = top.window.$(".nav-tabs ul li a[node-url='" + url + "']");
                            }
                        }
                    }
                    if (navlink.size() > 0 && navlink.attr('addtabs')) {
                        top.window.$("ul.nav-addtabs li#tab_" + navlink.attr('addtabs') + " .close-tab").trigger("click");
                    }
                }
            },
            replaceids: function (elem, url) {//如果有需要替换ids的
                if (url.indexOf("{ids}") > -1) {
                    var ids = 0;
                    var tableId = $(elem).data("table-id");
                    if (tableId && $(tableId).size() > 0 && $(tableId).data("bootstrap.table")) {
                        var Table = require("table");
                        ids = Table.api.selectedids($(tableId)).join(",");
                    }
                    url = url.replace(/\{ids\}/g, ids);
                }
                return url;
            }
           
        },//api成员↑
        
        init: function () {//Backend 模块 的初始化`方法， 被加载的时候会运行（因为有  Backend.init();//默认初始化执行的代码）
            //公共代码
            if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {//添加ios-fix兼容iOS下的iframe
                $("html").addClass("ios-fix");
            }
            
            //Toastr.options.positionClass = Config.controllername === 'index' ? "toast-top-right-index" : "toast-top-right";//配置Toastr的参数
            
            Toastr.options.positionClass = "toast-top-center" ;//配置Toastr的参数
           
            $(document).on('click', '.btn-dialog,.dialogit', function (e) { //点击包含.btn-dialog的元素时弹出dialog
                var options = $(this).data() || {};
                Backend.api.open(Backend.api.replaceids(this, $(this).attr('href')), $(this).attr('title'), options);
                return false;
            });
            
            $(document).on('click', '.btn-addtabs,.addtabsit', function (e) {//点击包含.btn-addtabs的元素时新增选项卡
                //alert ('dfd');
                Backend.api.addtabs(Backend.api.replaceids(this, $(this).attr('href')), $(this).attr("title"));
                return false;
            });
            
            $(document).on('click', '.btn-ajax,.ajaxit', function (e) {//点击包含.btn-ajax的元素时发送Ajax请求
                var options = $(this).data();
                if (typeof options.url === 'undefined' && $(this).attr("href")) {
                    options.url = $(this).attr("href");
                }
                options.url = Backend.api.replaceids(this, options.url);
                Backend.api.ajax(options);
                
                return false;
            });
            
            if ($(".fixed-footer").size() > 0) {//修复含有fixed-footer类的body边距
                $(document.body).css("padding-bottom", $(".fixed-footer").outerHeight());
            }
            if ($(".layer-footer").size() > 0 && self === top) {//修复不在iframe时layer-footer隐藏的问题
                $(".layer-footer").show();
            }
        },// -----------end of init: function () {
    };// -----------end of var Backend = {
    Backend.api = $.extend(Fast.api, Backend.api);//把Fast.api并入Backend.api（所以有时候可以用·Backend.api.XXX ）
    window.moment = moment;//将moment渲染至全局,以便于在子框架中调用
    window.Backend = Backend;//将Backend渲染至全局,以便于在子框架中调用
    Backend.init();//初始化
    return Backend;//返回对象，别的地方如果加载了这个js，就可以用 返回值 来使用这个js库
});// -----------end of define(['fast', 'moment'], function (Fast, moment)