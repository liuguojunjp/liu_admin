//Fast 模块(整个Fast应用程序用的js)
//require.js加载的模块，采用AMD规范。也就是说，模块必须按照AMD的规定来写。
define(['jquery', 'bootstrap', 'toastr', 'layer', 'lang'], //在这里用了define而没有用require，因为这里要定义Fast对象给以后要加载的js使用（ window.Fast = Fast;//将Fast渲染至全局(以后在程序中用 Fast 代表 Fast模块)）
//先加载'jquery', 'bootstrap', 'toastr', 'layer', 'lang' （其中 'jquery', 'bootstrap'应该早已加载）
//'lang'// 'lang':是个path url，加载'lang'的时候就会访问url，然后服务器写好了代码，根据语言判断动态向客户端输出lang文件
//需要依赖 'jquery', 'bootstrap', 'toastr', 'layer', 'lang' 这几个模块， bootstrap没有用到
function ($, undefined, Toastr, Layer, Lang) {//然后执行的回调函数
    var Fast = {//定义对象
        config: {//成员config
            toastr: {//config里头的成员toastr，
                //内容  toastr默认配置
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        },
                events: {//events成员，onAjaxSuccess，onAjaxError，onAjaxResponse函数成员，  他们在 api成员的ajax中（还有table.js里）被调用，
                    onAjaxSuccess: function (ajaxResponseObj, successCallback) {//events成员里头的成员函数 onAjaxSuccess--------功能：  Ajax请求成功的回调，执行定义好的回调函数 和toastr显示信息
                        var ajaxResponseData = typeof ajaxResponseObj.data !== 'undefined' ? ajaxResponseObj.data : null;//取响应的data部分,没有为null
                        var ajaxResponseMsg = typeof ajaxResponseObj.msg !== 'undefined' && ajaxResponseObj.msg ? ajaxResponseObj.msg : __('操作完了');//取响应的msg部分，没有用'操作完了'
                        if (typeof successCallback === 'function') {
                            if (successCallback.call(
                            this,// 在这里 this 是 events成员，一个object 
                            ajaxResponseData, ajaxResponseObj) === false)////回调到调用者（使用events.onAjaxSuccess的地方,一般是·api的ajax方法中的option中的success）定义的函数
                            {
                                return;
                            }
                        }
                        if (!ajaxResponseData || typeof ajaxResponseData.no_msg === 'undefined' || ajaxResponseData.no_msg !== 1)
                        {
                            Toastr.success(ajaxResponseMsg);//AjaxSuccess后，必要的时候用Toastr提示信息
                        }
                    },

                    onAjaxError: function (ajaxResponseObj, errorCallback) {//events成员里头的成员函数 onAjaxError --------功能： 请求错误的回调 执行定义好的回调函数 和toastr显示错误信息
                        var ajaxResponseData = typeof ajaxResponseObj.data !== 'undefined' ? ajaxResponseObj.data : null;
                        if (typeof errorCallback === 'function') {
                            if (errorCallback.call(this, ajaxResponseData, ajaxResponseObj) === false) { //回调到调用者（使用events.onAjaxError,一般是·api的ajax方法中的option中的error）定义的函数
   //call 方法可以用来代替另一个对象调用一个方法。call 方法可将一个函数的对象上下文从初始的上下文改变为由 thisObj 指定的新对象。 
//如果没有提供 thisObj 参数，那么 Global 对象被用作 thisObj。 
                                return;
                            }
                        }
                        var showmsg = ajaxResponseObj.msg;
                        if (ajaxResponseObj.code !== 0)//非0的时候才显示code
                        {
                            showmsg += ("(code:" + ajaxResponseObj.code + ")");
                        }
                        if (!ajaxResponseData || typeof ajaxResponseData.no_msg === 'undefined' || ajaxResponseData.no_msg !== 1)
                        {
                            Toastr.error(showmsg);
                        }
                    },

                    onAjaxResponse: function (ajaxResponseObj) {//events成员里头的成员函数 onAjaxResponse --------功能： 服务器响应数据后，怕服务端的数据不严谨做的一些处理
                        try {
                            var ret = null;
                            //以下是为了保持 返回来的是一个json对象而不是一个数据
                            if (typeof ajaxResponseObj === 'object')//如果返回的是对象 
                            {
                                ret = ajaxResponseObj;
                            } else
                            {
                                ret = JSON.parse(ajaxResponseObj); //那应该是json字符串，那解释它，转换为对象，怕服务端写错了搞成Json字符串了
                            }
                            if (!ret.hasOwnProperty('code')) {//如果对象没有code属性？
                                //判断一个属性是定义在对象本身而不是继承自原型链，我们需要使用从 Object.prototype 继承而来的 hasOwnProperty 方法
                                $.extend(ret, {code: -2, msg: ajaxResponseObj, data: null});//把 {code: -2, msg: response, data: null}并入对象
                            }
                        } catch (e) {
                            var ret = {code: -1, msg: e.message, data: null};
                        }
                        return ret;
                    }
                },
        
        api: { //Fast里头的， api成员 ，里头 有下面的成员函数↓
            ajax: function (ajaxOption, successCallback, errorCallback) {//api成员里头的成员函数 ajax    --------★★★★功能：发送Ajax请求！！★★★★
                ajaxOption =((typeof ajaxOption === 'string') ? {url: ajaxOption} : ajaxOption);//如果只是一个string，包装成一个对象，成员url就是那个string
                var idxOfLoadingLayer = Layer.load();//用Layer显示一个loading层
                ajaxOption = $.extend({//把success, error回调函数的设定合并入options对象（以外面传过来的ajaxOption对象为准）
                    type: "POST",
                    dataType: "json",
                    success: function (ajaxResponseObj) {//ajax成功后会来到这里（成功回调函数）
                        debugger;
                        Layer.close(idxOfLoadingLayer);//关闭loading层
                        ajaxResponseObj = Fast.events.onAjaxResponse(ajaxResponseObj);//处理返回的数据
                        if (ajaxResponseObj.code === 1) {//这个重要，成功代码默认为1 ！！
                            Fast.events.onAjaxSuccess(ajaxResponseObj, successCallback);//ajax成功后执行成功函数回调和显示信息
                        } else {
                            Fast.events.onAjaxError(ajaxResponseObj, errorCallback);//如果返回码不是1，也算ajax失败（比如说login密码不对）
                        }
                    },
                    error: function (xhr) {//ajax失败后
                        Layer.close(idxOfLoadingLayer);//关闭loading层
                        var ret = {code: xhr.status, msg: xhr.statusText, data: null};//包装失败的信息
                        Fast.events.onAjaxError(ret, errorCallback); //执行失败函数回调和显示信息
                    }
                }, ajaxOption);
                debugger;
                $.ajax(ajaxOption);//---★★★★★★★★★★★★★★★★★★ 发送ajax！！，在被调用前 url:  data: 什么的应该已经设定好★★★★★★★★★★★★★★★★★★★★★★★★
            },
            
            fixurl: function (url) {//api成员里头的成员函数 fixurl   --------功能：修复URL
                if (url.substr(0, 1) !== "/") {
                    var r = new RegExp('^(?:[a-z]+:)?//', 'i');
                    if (!r.test(url)) {
                        url = Config.moduleurl + "/" + url;
                    }
                }
                return url;
            },
            
            cdnurl: function (url) {//api成员里头的成员函数 cdnurl   --------功能：获取修复后可访问的cdn链接
                return /^(?:[a-z]+:)?\/\//i.test(url) ? url : Config.upload.cdnurl + url;
            },

            query: function (name, url) {//api成员里头的成员函数 query   --------功能：查询Url参数 在bootstrap-table-commonsearch和attachment被调用
                if (!url) {
                    url = window.location.href;
                }
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                        results = regex.exec(url);
                if (!results)
                    return null;
                if (!results[2])
                    return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));// 函数可对 encodeURIComponent() 函数编码的 URI 进行解码。
            },
            
            open: function (url, title, options,area,isfull) {//api成员里头的成员函数 open   --------功能：打开一个弹出窗口 ，窗口内容 从url加载
                title = title ? title : "";
                url = Fast.api.fixurl(url);
                url = url + (url.indexOf("?") > -1 ? "&" : "?") + "dialog=1";
                area =  (typeof area  === "undefined" ? [$(window).width() > 800 ? '800px' : '95%', $(window).height() > 600 ? '600px' : '95%']:area);
                
                options = $.extend({
                    type: 2,
                    title: title,
                    shadeClose: true,// shadeClose: true,
                    shade: [0.8, '#393D49'],//shade: false,
                    maxmin: true,
                    moveOut: true,
                    area: area,
                    content: url,
                    zIndex: Layer.zIndex,
                    success: function (layero, index) {//定义窗口提交成功的回调函数？
                        
                        var that = this;            
                        $(layero).data("callback", that.callback);//存储callback事件
                        //$(layero).removeClass("layui-layer-border");
                        Layer.setTop(layero);
                        var frame = Layer.getChildFrame('html', index);
                        var layerfooter = frame.find(".layer-footer");
                        Fast.api.layerfooter(layero, index, that);

                        //绑定事件
                        if (layerfooter.size() > 0) {
                            
                            // 监听窗口内的元素及属性变化
                            // Firefox和Chrome早期版本中带有前缀
                            var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver
                            var target = layerfooter[0];// 选择目标节点                            
                            var observer = new MutationObserver(function (mutations) {// 创建观察者对象
                                Fast.api.layerfooter(layero, index, that);
                                mutations.forEach(function (mutation) {
                                });
                            });
                            var config = {attributes: true, childList: true, characterData: true, subtree: true}// 配置观察选项:
                            observer.observe(target, config);// 传入目标节点和观察选项                           
                            // observer.disconnect();// 随后,你还可以停止观察
                        }
                    }
                }, options ? options : {});
                
                if ($(window).width() < 480 || (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream && top.$(".tab-pane.active").size() > 0)) {
                    options.area = [top.$(".tab-pane.active").width() + "px", top.$(".tab-pane.active").height() + "px"];
                    options.offset = [top.$(".tab-pane.active").scrollTop() + "px", "0px"];
                }
                
                        var index = Layer.open(options);//打开窗口
                        if (isfull === 'true')
                        {
                            Layer.full(index);//最大化
                        }
               return false;
            },

            close: function (data) {//api成员里头的成员函数 close   --------功能：关闭窗口并回传数据
                var index = parent.Layer.getFrameIndex(window.name);
                var callback = parent.$("#layui-layer" + index).data("callback");
                
                parent.Layer.close(index);//再执行关闭
                
                if (typeof callback === 'function') {//再调用回传函数
                    callback.call(undefined, data);
                }
            },
            
            layerfooter: function (layero, index, that) {//api成员里头的成员函数 layerfooter，为弹出窗口底部的放着[提交]按钮的 layerfooter 部分定义功能   --------功能： 在open方法中被调用，定义了弹出窗口提交后
                var frame = Layer.getChildFrame('html', index);
                var layerfooter = frame.find(".layer-footer");
                if (layerfooter.size() > 0) {
                    $(".layui-layer-footer", layero).remove();
                    var footer = $("<div />").addClass('layui-layer-btn layui-layer-footer');
                    footer.html(layerfooter.html());
                    if ($(".row", footer).size() === 0) {
                        $(">", footer).wrapAll("<div class='row'></div>");
                    }
                    footer.insertAfter(layero.find('.layui-layer-content'));
                    //绑定事件
                    footer.on("click", ".btn", function () {
                        if ($(this).hasClass("disabled") || $(this).parent().hasClass("disabled")) {
                            return;
                        }
                        $(".btn:eq(" + $(this).index() + ")", layerfooter).trigger("click");
                    });

                    var titHeight = layero.find('.layui-layer-title').outerHeight() || 0;
                    var btnHeight = layero.find('.layui-layer-btn').outerHeight() || 0;
                    //重设iframe高度
                    $("iframe", layero).height(layero.height() - titHeight - btnHeight);
                }
                //修复iOS下弹出窗口的高度和iOS下iframe无法滚动的BUG
                if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
                    var titHeight = layero.find('.layui-layer-title').outerHeight() || 0;
                    var btnHeight = layero.find('.layui-layer-btn').outerHeight() || 0;
                    $("iframe", layero).parent().css("height", layero.height() - titHeight - btnHeight);
                    $("iframe", layero).css("height", "100%");
                }
            },
            
//            success: function (options, callback) {//api成员里头的成员函数 success   -------- 不知道在哪里被调用，拿来干什么 20180730 tempdel
//                var type = typeof options === 'function';
//                if (type) {
//                    callback = options;
//                }
//                return Layer.msg(__('Operation completed'), $.extend({
//                    offset: 0, icon: 1
//                }, type ? {} : options), callback);
//            },

            
//            error: function (options, callback) {//api成员里头的成员函数 error   -------- 不知道在哪里被调用，拿来干什么 20180730 tempdel
//                var type = typeof options === 'function';
//                if (type) {
//                    callback = options;
//                }
//                return Layer.msg(__('Operation failed'), $.extend({
//                    offset: 0, icon: 2
//                }, type ? {} : options), callback);
//            },
            
            toastr: Toastr,
            layer: Layer
        },//api成员↑
        
        lang: function () {//lang成员方法   客户端的多语言转换，在后面代码可以看到，在全局用__代表
            var args = arguments,
                    string = args[0],
                    i = 1;
            string = string.toLowerCase();
             
            //string = typeof Lang[string] != 'undefined' ? Lang[string] : string;
            if (typeof Lang[string] != 'undefined') {
                if (typeof Lang[string] == 'object')
                    return Lang[string];
                string = Lang[string];
            } else if (string.indexOf('.') !== -1 && false) {
                var arr = string.split('.');
                var current = Lang[arr[0]];
                for (var i = 1; i < arr.length; i++) {
                    current = typeof current[arr[i]] != 'undefined' ? current[arr[i]] : '';
                    if (typeof current != 'object')
                        break;
                }
                if (typeof current == 'object')
                    return current;
                string = current;
            } else {
                string = args[0];
            }
            return string.replace(/%((%)|s|d)/g, function (m) {
                // m is the matched format, e.g. %s, %d
                var val = null;
                if (m[2]) {
                    val = m[2];
                } else {
                    val = args[i];
                    // A switch statement so that the formatter can be extended. Default is %s
                    switch (m) {
                        case '%d':
                            val = parseFloat(val);
                            if (isNaN(val)) {
                                val = 0;
                            }
                            break;
                    }
                    i++;
                }
                return val;
               
            });
        },
        
        init: function () {//Fast 模块 的初始化！！ 被加载的时候会运行（因为有  Fast.init();//默认初始化执行的代码）
            $.ajaxSetup({// 对相对地址进行处理  为所有 AJAX 请求设置默认 URL 和 success 函数：
                beforeSend: function (xhr, setting) {
                    setting.url = Fast.api.fixurl(setting.url);
                }
            });
            Layer.config({
                skin: 'layui-layer-fast'
            });
            $(window).keyup(function (e) { // 绑定ESC关闭layer窗口事件
                if (e.keyCode == 27) {
                    if ($(".layui-layer").size() > 0) {
                        var index = 0;
                        $(".layui-layer").each(function () {
                            index = Math.max(index, parseInt($(this).attr("times")));
                        });
                        if (index) {
                            Layer.close(index);//关闭layer窗口
                        }
                    }
                }
            });
            
            Toastr.options = Fast.config.toastr;//配置Toastr的参数 
        }//  end of  init: function () 
    };//--- end of  var Fast = {
    
    window.Layer = Layer;//将Layer暴露到全局中去(以后在程序中用 Layer 代表 Layer模块)
    window.Toastr = Toastr;//将Toastr暴露到全局中去(以后在程序中用 Toastr 代表 Toastr模块)
    window.__ = Fast.lang;//将语言方法暴露到全局中去(以后在程序中用 __ 代表 Fast模块的lang方法)
    window.Fast = Fast;//将Fast渲染至全局(以后在程序中用 Fast 代表 Fast模块)
    Fast.init();//默认初始化执行的代码
    //alert( typeof(Fast.config.toastr));
    return Fast;//返回对象，别的地方如果加载了这个js，就可以用 返回值 来使用这个js库
});
