require.config({
    urlArgs: "v=" + requirejs.s.contexts._.config.config.site.version,//防止缓存    
    packages: [{ //packages从CommonJS包(package)中加载模块。
            name: 'moment',   //一个日時計算的js模块,加载后的使用方法， moment.XXX
            location: '../libs/moment',
            main: 'moment'
        }
    ],
    include: ['css', 'layer', 'toastr', 'prototype_extend','fast', 'backend', 'table', 'form', 'dragsort', 'drag', 'drop', 'addtabs', 'selectpage'],//在打包压缩时将会把include中的模块合并到主文件中
    paths: {//require.config 的path，是用来配置模块加载位置，简单点说就是给模块起一个更短更好记的名字 ,的path 用于一些常用库或者文件夹路径映射，方便后面使用，省得每次都要输入一长串路径。（js 文件的后缀可以省略）
        'lang': "empty:",//定不定义应该都可以，后面定义了是个url，加载'lang'的时候就会访问url，然后服务器写好了代码，根据语言判断动态向客户端输出lang文件
        'form': 'require-form',//以后用 form 这个标识代替 同文件夹的require-form（.js）文件 ,这个js可以定义窗体的validate等功能。具体看 Form.api.bindevent(
        'table': 'require-table',//以后用 table 这个标识代替 同文件夹的require-table（.js）文件 ，这个是bootstraptable关联定义，初始化等设置等
        'upload': 'require-upload',//以后用 upload 这个标识代替 同文件夹的require-upload（.js）文件 ，上传相关js
        'validator': 'require-validator',//以后用 validator 这个标识代替 同文件夹的require-validator（.js）文件 ，表单校验插件
        'my_validator': 'my_validator',//自己写的一些用于验证的函数的集合
        'drag': 'jquery.drag.min',//以后用 drag 这个标识代替 同文件夹的jquery.drag.min（.js）文件
        'drop': 'jquery.drop.min',//以后用 drop 这个标识代替 同文件夹的jquery.drop.min（.js）文件
        'adminlte': 'adminlte',//以后用 adminlte 这个标识代替 同文件夹的adminlte（.js）文件，画面模板
        'bootstrap-table-commonsearch': 'bootstrap-table-commonsearch',//以后用 bootstrap-table-commonsearch 这个标识代替 bootstrap-table-commonsearch（.js）文件
        'bootstrap-table-template': 'bootstrap-table-template',//以后用 bootstrap-table-template 这个标识代替 bootstrap-table-template（.js）文件 (将BootstrapTable的行使用自定义的模板来渲染)
        'gmt': 'gmt',//一些日期函数
        // ★★★★以下的包从bower的libs目录加载↓
        'jquery': '../libs/jquery/dist/jquery.min',// 如果是 "jquery" : ["../libs/jquery/dist/jquery.min", "http://libs.baidu.com/jquery/2.0.3/jquery"],就是可以配置多个路径，如果远程cdn库没有加载成功，可以加载本地的库
        'bootstrap': '../libs/bootstrap/dist/js/bootstrap.min',
        'bootstrap-datetimepicker': '../libs/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min',//datetimepicker
        'bootstrap-icheck': '../libs/iCheck/icheck.min',//一个选择框美化包
        
        // for fullcalendar ↓
        'fullcalendar': '../libs/fullcalendar/fullcalendar.min',
        'bootbox': '../libs/bootbox/bootbox.min',//using Bootstrap modals,模态对话框
        //'jquery-ui': '../libs/jquery_ui/jquery-ui-1.10.3.full.min',
        'jquery-ui': '../libs/jquery-ui/jquery-ui.min',//建立在jQuery JavaScript 库上的一组用户界面交互、特效、小部件及主题
        'touch-punch': '../libs/jquery_ui_touch_punch/jquery.ui.touch-punch.min',//jquery-ui上的拖拽支持 
        'chosen': '../libs/chosen/chosen.jquery.min',//一个选择框插件
        'gritter': '../libs/gritter/jquery.gritter.min',//一个通知插件,类似toast
        // for fullcalendar ↑
        
        'bootstrap-daterangepicker': '../libs/bootstrap-daterangepicker/daterangepicker',//日期选择插件
        'bootstrap-select': '../libs/bootstrap-select/dist/js/bootstrap-select.min',//下拉框插件
        //'bootstrap-select-lang': '../libs/bootstrap-select/dist/js/i18n/defaults-zh_CN',
        'bootstrap-select-lang': '../libs/bootstrap-select/dist/js/i18n/defaults-ja_JP',
        'bootstrap-table': '../libs/bootstrap-table/dist/bootstrap-table',////'bootstrap-table': '../libs/bootstrap-table/dist/bootstrap-table.min', 表格插件
        'bootstrap-editable': '../libs/x-editable-develop/dist/bootstrap3-editable/js/bootstrap-editable',//editable 插件
        
        'bootstrap-table-export': '../libs/bootstrap-table/dist/extensions/export/bootstrap-table-export.min',//bootstrap-table的输出功能，少用
        'bootstrap-table-mobile': '../libs/bootstrap-table/dist/extensions/mobile/bootstrap-table-mobile',
        'bootstrap-table-editable': '../libs/bootstrap-table/src/extensions/editable/bootstrap-table-editable',//bootstrap-table用来连接bootstrap-editable的初始化代码
        //'bootstrap-table-lang': '../libs/bootstrap-table/dist/locale/bootstrap-table-zh-CN',
        'bootstrap-table-lang': '../libs/bootstrap-table/dist/locale/bootstrap-table-ja-JP',
        //'bootstrap-table-lang': '../libs/bootstrap-table/dist/locale/bootstrap-table-'+requirejs.s.contexts._.config.config.language,
        
        'tableexport': '../libs/tableExport.jquery.plugin/tableExport.min', //表格数据输出
        'slimscroll': '../libs/jquery-slimscroll/jquery.slimscroll',//滚动条插件，把内容放在一个盒子里面，固定一个高度，超出的则使用滚动
        'sortable': '../libs/Sortable/Sortable.min',//排序器
        'addtabs': '../libs/jquery-addtabs/jquery.addtabs',//MDI里头的tab追加功能
        'validator-core': '../libs/nice-validator/dist/jquery.validator',//nice-validator 客户端校验器
        //
        //
        //'validator-lang': '../libs/nice-validator/dist/local/zh-CN',
        //'validator-lang': '../libs/nice-validator/dist/local/ja',
        //'validator-lang': '../libs/nice-validator/dist/local/en',
        'validator-lang': '../libs/nice-validator/dist/local/'+requirejs.s.contexts._.config.config.language,//动态加载浏览器对应的语言校验器
        
        'plupload': '../libs/plupload/js/plupload.min',//上传插件
        'layer': '../libs/layer/src/layer',//layui弹层
        'toastr': '../libs/toastr/toastr',//toastr， https://codeseven.github.io/toastr/demo.html
        
        'template': '../libs/art-template/dist/template-native',//art-template 是一个简约、超快的模板引擎 (在upload里头用到)
        'selectpage': '../libs/selectpage/selectpage',//简洁而强大的下拉分页选择插件
        'cookie': '../libs/jquery.cookie/jquery.cookie',//cookie 用
        'dragsort': '../libs/dragsort/jquery.dragsort',//还不能删除， ，拖动排序插件， 好像加载jquery就用到了
        
        //liu del↓
        
        // 'echarts': 'echarts.min',
        //'echarts-theme': 'echarts-theme',
        // 'jstree': '../libs/jstree/dist/jstree.min',//树形插件
        //'summernote': '../libs/summernote/dist/lang/summernote-zh-CN.min',---文字 editor
        //'cxselect': '../libs/jquery-cxselect/js/jquery.cxselect',//基于jQuery 的多级联动菜单插件
        //'qrcode': '../libs/jquery-qrcode/jquery.qrcode.min',
        //'citypicker': '../libs/city-picker/dist/js/city-picker.min',
        //'citypicker-data': '../libs/city-picker/dist/js/city-picker.data',
        ////'datatable': '../libs/DataTables/media/js/jquery.dataTables',//
        
        //liu del↑
         
    },
    //理论上，require.js加载的模块，必须是按照AMD规范、用define()函数定义的模块。但是实际上，虽然已经有一部分流行的函数库（比如jQuery）符合AMD规范，更多的库并不符合。
    //这样的模块在用require()加载之前，要先用require.config()方法，定义它们的一些特征。
    // shim依赖配置：部分时候需要加载非AMD规范的js，这时候就需要用到另一个功能：shim shim直接翻译为”垫”
    // 1.非AMD模块输出，将非标准的AMD模块”垫”成可用的模块　
    // 2.非AMD模块依赖, 例如bootstrap-table依赖bootstrap   
    shim: {//shim属性，专门用来配置不兼容的模块,  shim：加载非 AMD 规范的 js，并解决其载入顺序。
       // 'addons': ['backend'],//liu del
        'bootstrap': ['jquery'],
        'bootstrap-table': {
            deps: [//deps数组，表明该模块的依赖性
                'bootstrap',
//                'css!../libs/bootstrap-table/dist/bootstrap-table.min.css'
            ],
            exports: '$.fn.bootstrapTable'//exports值（输出的变量名），表明这个模块外部调用时的名称；
        },
        'bootstrap-table-lang': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-export': {
            deps: ['bootstrap-table', 'tableexport'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-mobile': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'////暴露全局的名称
        },
        // 20181210 重要 整合 x·editable和 table-editable
        'bootstrap-editable': {
            deps: ['bootstrap','css!../libs/x-editable-develop/dist/bootstrap3-editable/css/bootstrap-editable.css'],
            exports: '$.fn.editable'
        },
         'bootstrap-table-editable': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        // 20181210 重要·

        'bootstrap-table-advancedsearch': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-commonsearch': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-template': {
            deps: ['bootstrap-table', 'template'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        
        'tableexport': {
            deps: ['jquery'],
            exports: '$.fn.extend'
        },
        'slimscroll': {
            deps: ['jquery'],
            exports: '$.fn.extend'
        },
        'adminlte': {
            deps: ['bootstrap', 'slimscroll'],
            exports: '$.AdminLTE'
        },
        'bootstrap-datetimepicker': [
            'moment/locale/zh-cn',
//            'css!../libs/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
        ],
        'bootstrap-select': ['css!../libs/bootstrap-select/dist/css/bootstrap-select.min.css', ],
        'bootstrap-select-lang': ['bootstrap-select'],
        //'summernote': ['../libs/summernote/dist/summernote.min', 'css!../libs/summernote/dist/summernote.css'],//liu del
//      'toastr': ['css!../libs/toastr/toastr.min.css'],
        //'jstree': ['css!../libs/jstree/dist/themes/default/style.css', ],//liu del
        'plupload': {
            deps: ['../libs/plupload/js/moxie.min'],
            exports: "plupload"
        },
        'jquery-ui': ['jquery'],
        'touch-punch': ['jquery-ui'],
        
//        'layer': ['css!../libs/layer/build/skin/default/layer.css'],
//        'validator-core': ['css!../libs/nice-validator/dist/jquery.validator.css'],
        'validator-lang': ['validator-core'],
//        'selectpage': ['css!../libs/selectpage/selectpage.css'],
       // 'citypicker': ['citypicker-data', 'css!../libs/city-picker/dist/css/city-picker.css']//liu del
    },
    baseUrl: requirejs.s.contexts._.config.config.site.cdnurl + '/assets/js/', //资源基础路径,这里用的是网站的绝对路径
    
    map: {
        '*': {
            'css': '../libs/require-css/css.min'
        }
    },
   // map 还支持“*”，意思是“对于所有的模块加载，使用本 map 配置”。如果还有更细化的 map 配置，会优先于“*”配置。
    /*
     * 
     * map: {
        'script/newmodule': {
            'foo': 'foo1.2'
        },
        'script/oldmodule': {
            'foo': 'foo1.0'
        }
    },
    上面的代码的意思是
    当 some/newmodule 调用了 require('foo')，它将获取到 foo1.2.js 文件。
    当 some/oldmodule 调用 require('foo')，时它将获取到 foo1.0.js 文件。
     * 
     * 
     比如下面配置，除了“some/oldmodule”外的所有模块，当要用“foo”时，都使用“foo1.2”来替代。
requirejs.config({
    map: {
        '*': {
            'foo': 'foo1.2'
        },
        'some/oldmodule': {
            'foo': 'foo1.0'
        }
    }
});
     * 
     */
    
    
    waitSeconds: 30,
    charset: 'utf-8' // 文件编码
});// endof require.config(↑

//以上定义完毕↑

//ロード開始↓
require(['jquery', 'bootstrap'], //先加载'jquery'和'bootstrap'，

function ($, undefined) {//然后,做下面的事情，$代表装入的jqurery↓    
    var Config = requirejs.s.contexts._.config.config;//初始配置,看样子就是模板里头开始 定义的 var require = {config: {$config | json_encode}};// 从
    
    
    window.Config = Config;//将Config渲染到全局　,Window 对象表示浏览器中打开的窗口
    var paths = {};// 配置语言包的路径 
    //console.info(Config.moduleurl);///admin
    paths['lang'] = Config.moduleurl + '/ajax/lang?callback=define&controllername=' + Config.controllername;//由于前面定义的'lang'是空
    //console.info(paths['lang']); //例：/admin/ajax/lang?callback=define&controllername=index
    //20180104 上面的路径，会向服务器请求js！！，(不是ajax（调试的结果不是ajax））
    // 'lang': "empty:",//定不定义应该都可以，后面定义了是个url，加载'lang'的时候就会访问url，然后服务器写好了代码，根据语言判断动态向客户端输出lang文件
    paths['backend/'] = 'backend/';// 避免目录冲突,有一个backend.js ,怕搞混？
    require.config(
            {
                paths: paths//再追加两个paths？
            }
    );
    
    // 用JQuery继续初始化加载
    $(function () {
        
        require(['prototype_extend'], //加载相应基本模块(同目录的 prototype_extend.js)，//用来自动加载一些原型扩展，比如说Date
        function (PrototypeExtend) {
       console.info('start to load Fast... ');//fast.js
        require(['fast'], //加载相应基本模块(同目录的 fast.js),他内部将会加载jquery,bootstrap,toastr,layer,lang
        function (Fast) {
            console.info('Fast loaded,Continue to load  backend ');
            require(['backend'],//（fast.js加载完成后），加载相应后台模块(backend.js)他内部将会加载 Fast, Moment
            function (Backend) { 
                console.info('Backend loaded ');//相应后台模块(backend.js)
                if (Config.jsname) {// jsname在backend.php  'jsname'         => 'backend/' . str_replace('.', '/', $controllername),
                    console.info('start to load '+Config.jsname);// 初始配置的jsname,例如 backend/dashboard.js ,或 backend/mastersetting/corpmaster.js
                    require([Config.jsname], //例如 加载(backend/index.js),他又加载'jquery', 'bootstrap', 'backend', 'addtabs', 'adminlte', 'form'
                    function (Controller) { //例如 加载(backend/dashboard.js),后，Controller就是返回来的 （backend/dashboard.js）对象, 
                        console.info('ControllerのJS--- '+Config.jsname+' loaded!!! ')
                        if(Controller[Config.actionname] != undefined )//20180612 如果对应.js对象里头定义了本次action的名字的函数，那么执行他
                        {
                            //console.info('EXE' + Config.jsname+' の '+Config.actionname);
                            Controller[Config.actionname]();
                            console.info('' + Config.jsname+' の '+Config.actionname+' function EXE End!');
                        }
                        //比如说本次的action是index， （backend/dashboard.js）对象里头如果定义了一个index的函数， 那么就去执行 backend/dashboard.js里头定义的index函数
                        //20180612 官方:在FastAdmin中每一个功能模块至少对应一个功能模块JS文件，也就是说每一个控制器都对应一个同名的JS文件，其次每一个控制器的方法对应JS文件中同名的方法。
                    }//---end of   if (Config.jsname) {
                    , function (e) {
                        console.error(e);// 这里可捕获模块加载的错误
                    });
                }//---end of   if (Config.jsname) {                
            });//---end of  //加载相应后台模块backend.js
        });// ---end of  require(['fast'], function (Fast) {//加载相应基本模块 fast.js
          
        });// ---end of  require(['prototype_extend'], function (prototype_extend) {
    });// ---end of $(function () {//jquery 调用函数
});// ---end of [ require(['jquery', 'bootstrap'], function ($, undefined)  ]
