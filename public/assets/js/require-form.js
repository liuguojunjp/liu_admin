define(['jquery', 'bootstrap', 
    'upload',//通过加载require-upload.js,加载上传插件pupload
    'validator'], //通过加载require-validator.js,加载验证插件nice-validator

function ($, undefined, Upload, Validator) {
    var Form = {
        config: {//config（设置）成员
        },
        events: {//events成员↓
            validator: function (form, success_callback, error_callback, submit_callback) {// 被bindevent调用， 此成员方法用来 配置规则，用的库是 nice validator，这个是很重要的绑定，因为（客户端）验证成功后会submit  ajax↓
                if (!form.is("form"))//如果传过来的不是表单对象
                {
                    alert("ini validator，para[form] not a form!");
                    return;
                }
                form.validator(// nice validator的 DOM 配置规则 ↓能用这个是因为在加载 require-form.js之后，自动也加载了require-validator.js，那里加载了nice-validator所需要的库
                        $.extend(
                        {//绑定表单事件,这里参考nice validator的用法，
                    validClass: 'has-success',//给验证通过的输入框添加的class名
                    invalidClass: 'has-error',//给验证不通过的输入框添加的class名
                    bindClassTo: '.form-group',//设置 validClass 和 invalidClass 添加到的位置
                    formClass: 'n-default n-bootstrap',//主题的 class 名称，添加在 form 上
                    msgClass: 'n-right',//字段消息的 class 名称，添加在消息容器上
                    stopOnError: true,//在第一次错误时停止验证 
                    display: function (elem) {//自定义消息中{0}的替换字符
                        return $(elem)
                                .closest('.form-group')//向上找form-group类的元素
                                .find(".control-label")//再向下找control-label类的元素
                                .text()
                                .replace(/\:/, '');
                    },
                    target: function (input) {//自定义消息的显示位置
                        var $formitem = $(input).closest('.form-group'),
                            $msgbox = $formitem.find('span.msg-box');
                        if (!$msgbox.length) {
                            return [];
                        }
                        return $msgbox;
                    },
                    valid: function (form) {//  表单客户端验证通过时的回调函数， ret是表单本身↓
                    //nice-validator官方--- 如果传递了valid参数回调或者valid.form事件，表单即使验证通过也不会被提交，而是由valid参数和valid.form事件接管。然后你需要自己决定如何提交表单。
                    //以下是自定义提交的代码
                    //debugger;
                        var this_validator = this, //如果没有这个替代，在函数里头的那个 .holdSubmit() 调用写成 this.holdSubmit()的话，那个this是windows对象！而不是validator对象
                        
                        submitBtn = $(".layer-footer [type=submit]", form);//选择类为layer-footer ,类型为submit的元素，（界面应该设计为一个提交按钮）
                        this_validator.holdSubmit();// ajax提交表单之前，先防止表单重复提交的措施（holdSubmit是nicevalidator的一个函数）
                        submitBtn.addClass("disabled");//选择类为layer-footer ,类型为submit的元素,(用jquery的addClass方法)追加 [disabled]类 
                        //★★★★★★★★★★★★★★★★★★★★★★★★客户端验证完成，提交!!　★★★★★★★★★★★★★★★★★★★★★★★★↓                        
//     如果不是ajax，可以post提交，下面是例子
//        $.post("path/to/server", $(form).serialize())
//            .done(function(d){
//                // some code
//            });
                        
                        Form.api.submit(//将要调用ajax提交()
                        $(form), //参数1，表单对象(用jquery包装起来)
                        function (data, ret) {//参数2，如果Form.api.submit 成功 ，就会回调到这里， data是服务器回来的json对象，ret
                           this_validator.holdSubmit(false);
                           //this.holdSubmit(false); 这里是错误的用法，因为这里的this并不是validator对象，是全局对象，全局可没有定义过 holdSubmit方法 ，具体参考 [深入浅出 JavaScript 中的 this]
                           submitBtn.removeClass("disabled");
                            if (typeof success_callback === 'function') {// successs是表单定义的回调函数（比如index.js里头的login方法定义的Form.api.bindevent 里头定义的那个success回调）
                                if (false === success_callback.call($(this), data, ret)) {//login form 是在这里进入后台index的，success是backend/index.js后面Form.api.bindevent定义的success函数！
                                    return false;
                                }
                            } 
                            var msg = ret.hasOwnProperty("msg") && ret.msg !== "" ? ret.msg : __('操作完了');//提示及关闭当前窗口↓
                            parent.Toastr.success(msg);
                            parent.$(".btn-refresh").trigger("click");//trigger() 方法触发被选元素的指定事件类型。找 btn-refresh的元素，自动单击
                            var index = parent.Layer.getFrameIndex(window.name);
                            parent.Layer.close(index);
                            return false;
                        }, 
                        
                        function (data, ret) {//参数3，提交失败的回调函数
                            this_validator.holdSubmit(false);
                            submitBtn.removeClass("disabled");
                            if (typeof error_callback === 'function') {
                                if (false === error_callback.call($(this), data, ret)) {
                                    return false;
                                }
                            }
                        },
                        submit_callback );
                        
                        //★★★★★★★★★★★★★★★★★★★★★★★★客户端验证完成，表单AJAX提交!!　★★★★★★★★★★★★★★★★★★★★★★★★↑
                        return true;
                    }//end of valid  后表单验证通过时调用此函数↑
                }, 
                form.data("validator-options") || {}//合并调用者（比如index.js里头的login方法 定义的 "validator-options",以"validator-options"为主（相同覆盖））
                        )// end of  [ $.extend( ]
                        
                        );//// nice validator的 DOM 配置规则 ↑
                $(".layer-footer [type=submit],.fixed-footer [type=submit],.normal-footer [type=submit]", form).removeClass("disabled");//移除提交按钮的disabled类
            },// 此成员方法用来定义valid规则，用的库是 nice validator  ↑
            
            selectpicker: function (form) {
                if ($(".selectpicker", form).size() > 0) {//绑定select元素事件
                    require(['bootstrap-select', 'bootstrap-select-lang'], function () {
                        $('.selectpicker', form).selectpicker();
                    });
                }
            },
            selectpage: function (form) {
                
                if ($(".selectpage", form).size() > 0) {//绑定selectpage元素事件
                    require(['selectpage'], function () {
                        $('.selectpage', form).selectPage({
                            source: 'ajax/selectpage',
                        });
                    });
                    $(form).on("change", ".selectpage-input-hidden", function () {//给隐藏的元素添加上validate验证触发事件
                        $(this).trigger("validate");
                    });
                }
            },
            
//            
//            cxselect: function (form) {//绑定cxselect元素事件 ,cxselect 一个多级连动选项
//                
//                if ($("[data-toggle='cxselect']", form).size() > 0) {// HTML5允许开发者自由为其标签添加属性，这种自定义属性一般用“data-”开头。
//                    require(['cxselect'], function () {
//                        $.cxSelect.defaults.jsonName = 'name';
//                        $.cxSelect.defaults.jsonValue = 'value';
//                        $.cxSelect.defaults.jsonSpace = 'data';
//                        $("[data-toggle='cxselect']", form).cxSelect();
//                    });
//                }
//            },
//            citypicker: function (form) {//绑定城市远程插件
//                if ($("[data-toggle='city-picker']", form).size() > 0) {
//                    require(['citypicker'], function () {});
//                }
//            },
            
            datetimepicker: function (form) {
                //绑定日期时间元素事件
               
                if ($(".datetimepicker", form).size() > 0) {
                    require(['bootstrap-datetimepicker'], function () {
                        var options = {
                            format: 'YYYY/MM/DD',
                            icons: {
                                time: 'fa fa-clock-o',
                                date: 'fa fa-calendar',
                                up: 'fa fa-chevron-up',
                                down: 'fa fa-chevron-down',
                                previous: 'fa fa-chevron-left',
                                next: 'fa fa-chevron-right',
                                today: 'fa fa-history',
                                clear: 'fa fa-trash',
                                close: 'fa fa-remove'
                            },
                            showTodayButton: true,
                            showClose: true
                        };
                        $('.datetimepicker', form).parent().css('position', 'relative');
                        $('.datetimepicker', form).datetimepicker(options);
                    });
                }
            },
            plupload: function (form) {
                //绑定plupload上传元素事件
                if ($(".plupload", form).size() > 0) {
                    Upload.api.plupload($(".plupload", form));
                }
            },
            faselect: function (form) {
                //绑定fachoose选择附件事件
                if ($(".fachoose", form).size() > 0) {
                    $(".fachoose", form).on('click', function () {
                        var that = this;
                        var multiple = $(this).data("multiple") ? $(this).data("multiple") : false;
                        var mimetype = $(this).data("mimetype") ? $(this).data("mimetype") : '';
                        parent.Fast.api.open(
                        "general/attachment/select?element_id=" + $(this).attr("id") + "&multiple=" + multiple + "&mimetype=" + mimetype, __('Choose'), 
                        {
                            callback: function (data) {
                                var button = $("#" + $(that).attr("id"));
                                var maxcount = $(button).data("maxcount");
                                var input_id = $(button).data("input-id") ? $(button).data("input-id") : "";
                                maxcount = typeof maxcount !== "undefined" ? maxcount : 0;
                                if (input_id && data.multiple) {
                                    var urlArr = [];
                                    var inputObj = $("#" + input_id);
                                    var value = $.trim(inputObj.val());
                                    if (value !== "") {
                                        urlArr.push(inputObj.val());
                                    }
                                    urlArr.push(data.url)
                                    var result = urlArr.join(",");
                                    if (maxcount > 0) {
                                        var nums = value === '' ? 0 : value.split(/\,/).length;
                                        var files = data.url !== "" ? data.url.split(/\,/) : [];
                                        var remains = maxcount - nums;
                                        if (files.length > remains) {
                                            Toastr.error(__('You can choose up to %d file%s', remains));
                                            return false;
                                        }
                                    }
                                    inputObj.val(result).trigger("change");
                                } else {
                                    $("#" + input_id).val(data.url).trigger("change");
                                }
                            }
                        }
                                );
                        return false;
                    });
                }
            },
            
            fieldlist: function (form) {
                
                if ($(".fieldlist", form).size() > 0) {
                    $(".fieldlist", form).on("click", ".append", function () {
                        var rel = parseInt($(this).closest("dl").attr("rel")) + 1;
                        var name = $(this).closest("dl").data("name");//closest() 方法获得匹配选择器的第一个祖先元素，从当前元素开始沿 DOM 树向上。
                        $(this).closest("dl").attr("rel", rel);
                        $('<dd class="form-inline"><input type="text" name="' + name + '[field][' + rel + ']" class="form-control" value="" size="10" /> <input type="text" name="' + name + '[value][' + rel + ']" class="form-control" value="" size="40" /> <span class="btn btn-sm btn-danger btn-remove"><i class="fa fa-times"></i></span> <span class="btn btn-sm btn-primary btn-dragsort"><i class="fa fa-arrows"></i></span></dd>')
                                .insertBefore($(this).parent());
                    });
                    
                    $(".fieldlist", form).on("click", "dd .btn-remove", function () {
                        $(this).parent().remove();
                    });
                    
                    require(['dragsort'], function () {//拖拽排序
                        //绑定拖动排序
                        $("dl.fieldlist", form).dragsort({
                            itemSelector: 'dd',
                            dragSelector: ".btn-dragsort",
                            dragEnd: function () {

                            },
                            placeHolderTemplate: "<dd></dd>"
                        });
                    });
                }
            },
//            bindevent: function (form) {
//
//            }
        },//events成员↑
        
        api: {//api成员 
            
            // ★★在表单验证成功后被调用！ 用ajax提交到服务器！
            submit: function (form, success_callback, error_callback, submit_callback) {// 表单的submit(提交)成员方法！！ 四个参数，表单对象，成功回调函数，错误回调函数，提交时候自动做的函数 ↓
                if (form.size() === 0)//没有提交的内容
                {
                    return Toastr.error(__("フォーム初期化まだ完成していません、サブミット失敗"));
                }
                
                if (typeof submit_callback === 'function') {//submit如果是一个自定义函数，那么执行他 (如果错返回）
                    if (false === submit_callback.call(form)) {
                        return false;
                    }
                }
                
                var type = form.attr("method").toUpperCase();// form 被jquery包装过，用 .attr(调用属性method （ method属性规定如何发送表单数据）
                type = type && (type === 'GET' || type === 'POST') ? type : 'GET';//缺省get
                url = form.attr("action");//form 的attr 属性 ，提交目标
                url = url ? url : location.href;//没写就是地址栏的内容 （只提交）
                
                var params = {};//修复当存在多选项元素（比如说checkbox）时提交的BUG
                var multipleList = $("[name$='[]']");
                if (multipleList.size() > 0) {
                    var postFields = form.serializeArray().map(function (obj) {
                        return $(obj).prop("name");
                    });
                    $.each(multipleList, function (i, j) {
                        if (postFields.indexOf($(this).prop("name")) < 0) {
                            params[$(this).prop("name")] = '';
                        }
                    });
                }
               
                Fast.api.ajax(  //★★★★★★★★★★★★调用Fast模块的Ajax请求方法,来提交！！★★★★★★★★★★★★★★★↓
                {  //参数1---AJAX options  ↓   
                    type: type,
                    url: url,
                    data: form.serialize() //序列化表单内容 类似于 a=1&b=2&c=3&d=4&e=5
                            + (params ? '&' + $.param(params) : ''),//参数（多选项元素）// jQuery的  param( obj ) 方法，创建一个数组或对象序列化的的字符串，适用于一个URL 地址查询字符串或Ajax请求 
                    
                    dataType: 'json',
                    complete: function (xhr) {
                        var token = xhr.getResponseHeader('__token__');
                        if (token) {
                            $("input[name='__token__']", form)//从 form 里头找出表单元素 __token__
                                    .val(token);
                        }
                    }
                },//参数1---AJAX options　↑
                
                function (ajaxResponseData, ajaxResponseObj) {//参数2 ---ajax的服务端验证成功了的话要做的事情（回调函数）↓
                    $('.form-group', form).removeClass('has-feedback has-success has-error');//做一些关于本form的class的处理
                    if (ajaxResponseData && typeof ajaxResponseData === 'object') {
                        if (typeof ajaxResponseData.token !== 'undefined') {//刷新客户端token
                            $("input[name='__token__']", form).val(ajaxResponseData.token);
                        }
                        if (typeof ajaxResponseData.callback !== 'undefined' && typeof ajaxResponseData.callback === 'function') {//调用客户端事件
                            ajaxResponseData.callback.call(form, ajaxResponseData);
                        }
                    }
                    if (typeof success_callback === 'function') {
                        if (false === success_callback.call(form, ajaxResponseData, ajaxResponseObj)) {//调用 上一层·（调用Form.api.submit的地方）传过来的 success回调函数
                            return false;
                        }
                    }
                }////参数2 ---ajax的服务端验证成功了的话要做的事情↑
                
                , function (ajaxResponseData, ajaxResponseObj) {//参数3-------- ajax的服务端验证错误了,或者ajax错误的话要做的事情（回调函数）↓
                    if (ajaxResponseData && typeof ajaxResponseData === 'object' && typeof ajaxResponseData.token !== 'undefined') {
                        $("input[name='__token__']", form).val(ajaxResponseData.token);
                    }
                    if (typeof error_callback === 'function') {
                        if (false === error_callback.call(form, ajaxResponseData, ajaxResponseObj)) {//回调到调用者（api.Submit的地方,一般是validator）定义的erron函数
                            return false;
                        }
                    }
                }//参数3--------ajax的服务端验证错误了的话要做的事情↑
               ); //★★★★★★★★★★★★调用Fast模块的Ajax请求方法,来提交！！★★★★★★★★★★★★★★★↑
       
                return false;
            },// end of api
           
            // ★★在表单验证成功后被调用，　表单的submit(提交)成员方法！！ 四个参数，表单对象，成功回调函数，错误回调函数，提交时候自动做的函数 ↑
            
            bindevent: function (form, success, error, submit) {//为表单绑定事件
                form = typeof form === 'object' ? form : $(form);//保证form是一个对象而不是DOM id
                var events = Form.events;//Form对象(不是上面的那个小写的form！而是require-form.js的全局化的实例)的events成员，内部有validator等成员函数
                events.validator(form, success, error, submit);//这个定义Form的客户端校验器 (nicevalidator)！而校验如果没问题，会ajax提交！
                
                events.selectpicker(form);//绑定select元素事件， bootstrap selectpicker是bootstrap里比较简单的一个下拉框的组件, 'bootstrap-select': '../libs/bootstrap-select/dist/js/bootstrap-select.min',//下拉框插件
                events.selectpage(form);//绑定selectpage元素事件
                events.datetimepicker(form);// 绑定日期时间元素事件
                events.plupload(form);//绑定plupload上传元素事件
                events.faselect(form);//绑定fachoose选择附件事件
                events.fieldlist(form);
            },//为表单绑定事件↑
            
            custom: {}
        },//api成员↑
    };
    return Form;//返回对象，别的地方如果加载了这个js，就可以用 返回值 来使用这个js库
});