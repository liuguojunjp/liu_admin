/**
 * Bootstrap Table Japanese translation
 * Author: Azamshul Azizy <azamshul@gmail.com>
 */
(function ($) {
    'use strict';

    $.fn.bootstrapTable.locales['ja-JP'] = {
        formatLoadingMessage: function () {
            return __('読み込み中です。少々お待ちください。');
        },
        formatRecordsPerPage: function (pageNumber) {
            return __('ページ当たり最大') + pageNumber + __('件');
        },
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return __('全') + totalRows + __('件から、')+ pageFrom + __('から') + pageTo + __('件目まで表示しています');
        },
        formatSearch: function () {
            return __('検索');
        },
        formatNoMatches: function () {
            return __('該当するレコードが見つかりません');
        },
        formatPaginationSwitch: function () {
            return __('ページ数を表示・非表示');
        },
        formatRefresh: function () {
            return __('更新');
        },
        formatToggle: function () {
            return __('トグル');
        },
        formatColumns: function () {
            return __('列');
        },
//        formatAllRows: function () {//問題が出る！
//            return __('すべて');
//        },
        formatExport: function () {
            return 'エクスポート';
        },
        formatClearFilters: function () {
            return '清空过滤';
        }
    };

    $.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['ja-JP']);

})(jQuery);