<!DOCTYPE html>
<html>
<head>
    <title>Editable</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../layui-layer-fast/css/bootstrap.min.css">
    <link rel="stylesheet" href="../layui-layer-fast/libs/bootstrap-table/src/bootstrap-table.css">
    <link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">
    <script src="../layui-layer-fast/libs/jquery/dist/jquery.min.js"></script>
    <script src="../layui-layer-fast/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../layui-layer-fast/libs/bootstrap-table/src/bootstrap-table.js"></script>
    <script src="../layui-layer-fast/libs/bootstrap-table/src/extensions/editable/bootstrap-table-editable.js"></script>
    <script src="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>   
    <script src="main.js"></script>
</head>
<body>
    <div class="container">
        <h1>Editable</h1>
        <table 
               data-pagination="true"
               data-toggle="table"
               data-url="data1.json">
            <thead>
            <tr>
                <th data-field="id">ID</th>
                <th data-field="name" data-editable="true">Item Name</th>
                <th data-field="price" data-editable="true">Item Price</th>
            </tr>
            </thead>
        </table>
        
        <div>
        <span>Status:</span>
        <a href="#" id="status"></a>
      </div>
    </div>
</body>
</html>