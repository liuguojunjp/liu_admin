<!DOCTYPE html>
<html>
<head>
    <title>Editable</title>
    <meta charset="utf-8">
     <script
    type="text/javascript"
    src="//code.jquery.com/jquery-3.1.1.js"
    
  ></script>
        
        
<script type="text/javascript">


    window.onload=function(){
      

$('#f-date').children('a').editable({
    type:'combodate',
    format:'YYYY/MM/DD',
    viewformat:"YYYY/MM/DD",
    template:"YYYY/MM/DD",
   
    pk:"1",
    placement: 'bottom',
}).on('save', function(e, params) {
    
    $('body').append('Saved value as UNIX Timestam: ' + params.newValue);
    $('body').append('<br>');
    $('body').append('Saved value Text: ' + $('#f-date').find('a').text());
    
});

    }

</script>

</head>
<body>

      <script src="https://vitalets.github.io/x-editable/assets/momentjs/moment.min.js"></script> 
        
        <!-- select2 --> 
       
        
        <!-- bootstrap 3 -->
        <link href="https://vitalets.github.io/x-editable/assets/bootstrap300/css/bootstrap.css" rel="stylesheet">
        <script src="https://vitalets.github.io/x-editable/assets/bootstrap300/js/bootstrap.js"></script>

        <!-- bootstrap-datetimepicker -->
            
        
        <!-- x-editable (bootstrap 3) -->
        <link href="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
        <script src="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>
    
    <li id="f-date"><span>Date: </span><a href="#" >2018/12/11</a></li>
    

</body>
</html>